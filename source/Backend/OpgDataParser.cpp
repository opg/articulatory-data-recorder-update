#pragma once
#include "OpgDataParser.h"

// @brief Parse function. This defines how the raw byte array "dataToParse" is interpreted
//		by the ByteDataCollector.
// @param const std::vector<std::byte>& dataToParse: raw byte array read out, omitting the
//		two start bytes.

Frame OpgDataParser::parse(const std::vector<std::byte>& dataToParse)
{
	// dataToParse is a captured byte array that needs to be interpreted correctly.
	// The opg sensor uses a 16 bit current value followed by 15 16 bit sensor values.
	static const int kNumSensors = 16;

	Frame frame;
	frame.scalarSensor.resize(numScalarSensors); // ByteDataCollector determines the numScalarSensors.

	unsigned int byteIndex = 0;
	// Read out the current frame index.
	frame.index = std::to_integer<unsigned int>(dataToParse[byteIndex++] << 24)
				+ std::to_integer<unsigned int>(dataToParse[byteIndex++] << 16)
				+ std::to_integer<unsigned int>(dataToParse[byteIndex++] << 8)
				+ std::to_integer<unsigned int>(dataToParse[byteIndex++]);

	// Read out the emitter current (0).
	short value_msb = (std::to_integer<short>(dataToParse[byteIndex++]) << 8);
	short value_lsb = std::to_integer<short>(dataToParse[byteIndex++]);

	short currentValue = value_msb + value_lsb;

	frame.scalarSensor[0] = static_cast<double>(currentValue);

	// Read out the sensor values (1:15)).
	for (int sensorIndex = 1; sensorIndex < kNumSensors; sensorIndex++)
	{
		// Convert MSB + LSB to single integer.
		int value_msb = (std::to_integer<signed short>(dataToParse[byteIndex++]) << 8);
		int value_lsb = std::to_integer<signed short>(dataToParse[byteIndex++]);

		signed short sensorValue = value_msb + value_lsb;

		frame.scalarSensor[sensorIndex] = static_cast<double>(sensorValue);
	}

	
	// Convert the current to mA.
	frame.scalarSensor[0] = (frame.scalarSensor[0] / (ADC1_MAX_VALUE_LSB * CURRENT_SENSE_RESISTOR_OHM * CURRENT_SENSE_GAIN)) * ADC1_REF_VOLTAGE_V * 1000; 

	/*
	// TODO: CURRENTLY COUNTS IS BETTER. 
	// Convert the sensor values to Volts.
	for (int sensorIndex = 1; sensorIndex < kNumSensors; sensorIndex++)
	{
		frame.scalarSensor[sensorIndex] = (frame.scalarSensor[sensorIndex] / ADC_MODULE_MAX_VALUE_LSB) * ADC_MODULE_RANGE_POSITIVE_V;
	}
	*/

	return frame;
}
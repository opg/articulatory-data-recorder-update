#pragma once
#include "File.h"

#include <string>

// All data is stored in the singleton Data class!
#include "Data.h"


class AdrFile :
    public File
{
public:
	AdrFile() = default;
	AdrFile(double start, double end);

protected:
	void readFromStream(std::istream& is) override;
	void writeToStream(std::ostream& os) override;

private:
	std::string createHeader();
	bool saveSubset_{ false };
	double startTime_{ 0.0 };
	double endTime_{ 0.0 };	
};


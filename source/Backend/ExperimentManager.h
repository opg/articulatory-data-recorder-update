#pragma once
#include <map>
#include <stdexcept>

#include "Randomizer.h"
#include "Prompt.h"

/// @brief This class holds functionality to define a randomization strategy.
class RandomizationStrategy
{
public:
	/// @brief This class holds the types of all currently implemented randomization strategies.
	enum class Type
	{
		CYCLIC_NON_RANDOM, WITH_REPLACEMENT, PERMUTE_ALL, PERMUTE_BALANCED, PERMUTE_BALANCED_NO_DOUBLETS
	};

	struct Description
	{
		std::string type;
		std::string brief;
		std::string verbose;

		Description() = default;
		/// @brief Constructs a randomization strategy with given values.
		/// @param type randomization strategy type
		/// @param brief brief description of the strategy
		/// @param verbose long description of the strategy
		Description(std::string type, std::string brief, std::string verbose) : type(type), brief(brief), verbose(verbose) {}
	};
	
	Type type{ Type::PERMUTE_BALANCED_NO_DOUBLETS };

	/// @brief Transforms a strategy to a string which can be printed to console.
	/// @return transformed strategy as string
	std::string ToString()
	{
		auto strategy = descriptions.find(type);
		if(strategy != descriptions.end())
		{
			return strategy->second.type;
		}
		throw std::invalid_argument("Could not find string representation for the passed type!");
	}

	/// @brief Transforms a string to a strategy which can be used inside of a randomizer.
	/// @param stringType type of strategy as string
	/// @return randomization strategy type
	static Type ToType(std::string stringType)
	{
		for (const auto& [type, description] : descriptions)
		{
			if (description.type == stringType)
			{
				return type;
			}
		}
		throw std::invalid_argument("Unknown randomization strategy" + stringType + "!");
	}

	/// @brief Constructs a randomization strategy object.
	/// @param randType randomization strategy type
	RandomizationStrategy(Type randType) : type(randType)
	{
	}

	RandomizationStrategy() = default;
	
public:
	inline static std::map<Type, Description> descriptions =
	{
		{Type::CYCLIC_NON_RANDOM, {"CYCLIC_NON_RANDOM", "Repeat for n = repetitions times", "The prompts will be given in the order in which they were specified in the inventory." }},
		{Type::WITH_REPLACEMENT, {"WITH_REPLACEMENT", "Randomize with replacement", "The prompts will be given randomly chosen from the inventory without memory." }},
		{Type::PERMUTE_ALL, {"PERMUTE_ALL", "Randomize without replacement", "The prompts will be given randomly chosen from the inventory with each inventory item appearing n = repetitions times."}},
		{Type::PERMUTE_BALANCED, {"PERMUTE_BALANCED", "Uniformly distributed items", "The prompts will be given randomly chosen from the inventory with each inventory item appearing n = repetitions times and only once per repetition cycle." }},
		{Type::PERMUTE_BALANCED_NO_DOUBLETS, {"PERMUTE_BALANCED_NO_DOUBLETS", "Uniformly distributed items with no doublets", "The prompts will be given randomly chosen from the inventory with each inventory item appearing n = repetitions times and only once per repetition cycle. No inventory item will appear at the beginning of a block if the previous block was concluded by the same item."}}
	};
};

/// @brief This struct holds settings which are part of the definition of an experiment.
struct Settings
{
	int repetitions{ 1 };
	RandomizationStrategy randStrategy;
	Settings() = default;
	/// @brief Constructs the settings object as part of the definition of an experiment.
	/// @param reps number of repetitions of each element
	/// @param strategy randomization strategy
	Settings(const int reps, const RandomizationStrategy strategy) : repetitions(reps), randStrategy(strategy) {}
};

/// @brief This struct holds the definition of an experiment object.
///
/// The type of an experiment is defined to be the same as the type of the underlying prompts. An experiment consists of an inventory, an intro and outro message and settings.
/// @tparam PromptContentType type of the underlying prompts
template<class PromptContentType>
struct Experiment
{
	std::vector<PromptContentType> inventory;
	std::string introMessage{ std::string("Please load/configure your experiment!") };
	std::string outroMessage{ std::string() };
	Settings settings;
	Experiment() = default;
	/// @brief Constructs an experiment with given parameters.
	/// @param inv inventory containing every element to be shown during the experiment
	/// @param sett settings, defining the number of repetitions and the randomization strategy
	/// @param welcomeString message to be shown at the beginning of the experiment
	/// @param endingString message to be shown at the end of the experiment
	Experiment(const std::vector<PromptContentType> inv, const Settings sett, std::string welcomeString = std::string(),
	           std::string endingString = std::string()) : inventory (inv), introMessage (welcomeString), outroMessage (endingString),
	           settings (sett) {}
};

/// @brief This class holds the definition of an experiment manager object.
///
/// The experiment manager object will hold an experiment and manages the procedure during this experiment. It is to be understood as interface object between an experiment (backend) and the visualization of the prompts (frontend).
/// @tparam PromptContentType type of the underlying experiment/prompts
template<class PromptContentType>
class ExperimentManager
{
public:
	ExperimentManager() = default;
	~ExperimentManager();

	/// @brief Constructs an experiment manager object with given inventory, number of repetitions and randomization strategy.
	/// @param inventory inventory of experiment
	/// @param repetitions number of repetitions of every item in the inventory
	/// @param randStrategy randomization strategy to be used to randomize the items in the inventory
	ExperimentManager(const std::vector<PromptContentType>& inventory, const int repetitions, const RandomizationStrategy randStrategy);

	/// @brief Constructs an experiment manager object with given inventory and settings (containing information on the number of repetitions and the randomization strategy)
	/// @param inventory inventory of experiment
	/// @param settings settings as part of the definition of the experiment
	ExperimentManager(const std::vector<PromptContentType>& inventory, const Settings& settings);

	/// @brief Constructs an experiment manager object with given (already defined) experiment.
	/// @param experiment experiment to be managed by the experiment manager object
	ExperimentManager(const Experiment<PromptContentType>& experiment);

	/// @brief Returns the current experiment manager object. "currentPrompt" will have been shifted to the next prompt.
	/// @return experiment manager object with shifted active prompt
	ExperimentManager<PromptContentType>& next();

	/// @brief Returns the current experiment manager object. "currentPrompt" will have been shifted to the previous prompt.
	/// @return experiment manager object with shifted active prompt
	ExperimentManager<PromptContentType>& previous();

	/// @brief Resets the experiment manager object (clearing the prompts queue).
	void reset();

	// Getter

	/// @brief Returns the currently used settings of the experiment manager object.
	/// @return experiment manager object settings
	const Settings& getSettings();

	/// @brief Returns the currently used inventory of the experiment inside of the experiment manager object.
	/// @return inventory of the experiment inside of the experiment manager object
	const std::vector<PromptContentType>& getInventory();

	/// @brief Returns the prompts queue (containing the randomized inventory of the experiment) of the experiment manager object.
	/// @return prompts queue
	const std::vector<Prompt<PromptContentType>>& getQueue();

	/// @brief Returns the experiment inside of the experiment manager object.
	/// @return experiment
	const Experiment<PromptContentType>& getExperiment();

	// Setter

	/// @brief Sets a new experiment to be managed by the experiment manager object (inventory already randomized).
	/// @param newExperiment new experiment
	void setExperiment(const Experiment<PromptContentType>& newExperiment);

	/// @brief Sets a new inventory to the managed experiment. Inventory will be randomized afterwards.
	/// @param newInventory new inventory
	void setInventory(const std::vector<PromptContentType>& newInventory);

	/// @brief Sets new settings to the experiment manager object.
	/// @param newSettings new settings
	void setSettings(const Settings& newSettings);

	/// @brief Iterator pointing on the currently shown prompt.
	typename std::vector<Prompt<PromptContentType>>::iterator currentPrompt;
	bool experimentHasStarted{ false };
	bool experimentHasFinished{ false };

private:
	Randomizer* randomizer_{ nullptr };
	Experiment<PromptContentType> experiment_;
	std::vector<Prompt<PromptContentType>> promptsQueue_;
	/// @brief Generates the prompts queue in dependency on the experiment manager settings and the contained experiment.
	void generateQueue();
};


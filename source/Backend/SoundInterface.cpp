#include "SoundInterface.h"
#include <memory>
#include <stdexcept>

#if defined(USE_PORTAUDIO)
#include "portaudio.h"
#endif

namespace
{

	class PortAudio : public SoundInterface
	{

	public:

		struct StreamParameters : PaStreamParameters
		{
			StreamParameters() : PaStreamParameters()
			{
				Pa_Initialize();
				device = Pa_GetDefaultOutputDevice();
				channelCount = 2;
				sampleFormat = paFloat32;
				if (device == paNoDevice)
				{
					suggestedLatency = 0;
				}
				else
				{
					suggestedLatency = Pa_GetDeviceInfo(device)->defaultHighOutputLatency;
				}
				hostApiSpecificStreamInfo = nullptr;
			}

			StreamParameters(PaDeviceIndex device, int channelCount, PaSampleFormat sampleFormat,
			                 PaTime suggestedLatency, void* hostApiSpecificStreamInfo)
				: PaStreamParameters()
			{
				Pa_Initialize();
				this->device = device;
				this->channelCount = channelCount;
				this->sampleFormat = sampleFormat;
				this->suggestedLatency = suggestedLatency;
				this->hostApiSpecificStreamInfo = hostApiSpecificStreamInfo;
			}

			StreamParameters(const StreamParameters& rhs)
			{
				Pa_Initialize();
				device = rhs.device;
				channelCount = rhs.channelCount;
				sampleFormat = rhs.sampleFormat;
				suggestedLatency = rhs.suggestedLatency;
				hostApiSpecificStreamInfo = rhs.hostApiSpecificStreamInfo;
			}

			~StreamParameters() { Pa_Terminate(); }
		};

		using paData = struct
		{
			std::vector<double>* buffer;
			unsigned* samplingRate_Hz;
			unsigned startPos;
			unsigned endPos;
			unsigned readPos;
			bool isAttached;
		};

	public:
		PortAudio();
		~PortAudio() override { Pa_Terminate(); }
		void reset() override;

		// Device management
		ApiInfo getHostApiInfo(int index) override;
		std::vector<ApiInfo> getHostApis() override;
		DeviceInfo getDeviceInfo(int index) override;
		std::vector<DeviceInfo> getDevices() override;
		ApiInfo getCurrentApi() override;
		ApiInfo getDefaultApi() override;
		DeviceInfo getCurrentInputDevice() override;
		DeviceInfo getCurrentOutputDevice() override;
		DeviceInfo getDefaultInputDevice() override;		
		DeviceInfo getDefaultOutputDevice() override;

		void setInputDevice(const DeviceInfo& device) override;
		void setInputDevice(int index) override;
		void setOutputDevice(const DeviceInfo& device) override;
		void setOutputDevice(int index) override;

		bool isSamplingRateSupported(DeviceInfo device, unsigned samplingRate_Hz) override;

		PaError checkStreamParameters(double samplingRate_Hz, const StreamParameters* inputParams = nullptr, 
			const StreamParameters* outputParams = nullptr);

		// Player
		double getPlaybackSamplingRate() override;
		void setPlaybackSamplingRate(unsigned samplingRate_Hz) override;
		Status getPlaybackStatus() override;
		Status playAsynchronously() override;
		Status playAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz) override;
		Status playAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz, double start_s,
		                          double end_s) override;
		Status playAsynchronously(double start_s, double end_s) override;
		bool playBlocking() override;
		bool playBlocking(std::vector<double>* buffer, unsigned* samplingRate_Hz) override;
		bool playBlocking(double start_s, double end_s) override;
		bool pausePlayback() override;
		bool stopPlayback() override;
		void resetPlayer() override;
		void setPlaybackBuffer(std::vector<double>* newBuffer, unsigned* samplingRate_Hz) override;
		[[nodiscard]] paData* getPlaybackData() { return &playbackData_; }
		void setPlaybackStartPos(unsigned newStartPos) override;
		void setPlaybackEndPos(unsigned newEndPos) override;

		// Recorder
		double getRecordingSamplingRate() override;
		void setRecordingSamplingRate(unsigned samplingRate_Hz) override;
		Status recordAsynchronously() override;
		Status recordAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz) override;
		Status recordBlocking(double time_s) override;
		Status recordBlocking(std::vector<double>* buffer, unsigned* samplingRate_Hz, double time_s) override;
		bool pauseRecording() override;
		Status stopRecording() override;
		bool resetRecording() override;
		Status getRecordingStatus() override;
		void setRecordingBuffer(std::vector<double>* newBuffer, unsigned* samplingRate_Hz) override;
		bool attachRecordingBuffer(bool attach) override;
		bool detachRecordingBuffer(bool detach) override;

		[[nodiscard]] paData getRecordingData() const { return recordingData_; }

	private:
		// These are the Portaudio callback function. They must have exactly this signature!
		static int outputCallback(const void* inputBuffer, void* outputBuffer,
			unsigned long framesPerBuffer,
			const PaStreamCallbackTimeInfo* timeInfo,
			PaStreamCallbackFlags statusFlags,
			void* userData);

		static int inputCallback(const void* inputBuffer, void* outputBuffer,
		                         unsigned long framesPerBuffer,
		                         const PaStreamCallbackTimeInfo* timeInfo,
		                         PaStreamCallbackFlags statusFlags,
		                         void* userData);

	private:
		paData playbackData_;
		paData recordingData_;
		PaStream* outputStream_;
		PaStream* inputStream_;
		StreamParameters outputStreamParameters_;
		StreamParameters inputStreamParameters_{
			getDefaultInputDevice().index, 1, paFloat32,
			getDeviceInfo(getDefaultInputDevice().index).defaultLowInputLatency, nullptr
	};
	};


	PortAudio::PortAudio() :
	playbackData_({nullptr, 0, 0, 0, 0}),
		recordingData_({nullptr, 0, 0, 0, 0})
	{
		if (Pa_Initialize() != paNoError)
		{
			throw std::runtime_error("[PortAudio::PortAudio()] Error initializing PortAudio!");
		}
		setInputDevice(getDefaultInputDevice());
		setOutputDevice(getDefaultOutputDevice());
	}

	void PortAudio::reset()
	{
		// We need to terminate any and all PortAudio instances for new devices to get enumerated
		while (Pa_Terminate() != paNotInitialized)
		{
		}
		if (Pa_Initialize() != paNoError) throw std::runtime_error("[PortAudio::reset()] Error terminating PortAudio!");
	}

	SoundInterface::ApiInfo PortAudio::getHostApiInfo(int index)
	{
		if (index >= Pa_GetHostApiCount() || index < 0)
		{
			return noApi;
		}

		auto* info = Pa_GetHostApiInfo(index);
		return {
			index, info->name, info->deviceCount,
			getDeviceInfo(info->defaultInputDevice),
			getDeviceInfo(info->defaultOutputDevice)
		};
	}

	std::vector<SoundInterface::ApiInfo> PortAudio::getHostApis()
	{
		std::vector<ApiInfo> apiInfo;
		for (int i = 0; i < Pa_GetHostApiCount(); ++i)
		{
			apiInfo.emplace_back(getHostApiInfo(i));
		}
		return apiInfo;
	}

	SoundInterface::DeviceInfo PortAudio::getDeviceInfo(int index)
	{
		const auto numDevices = Pa_GetDeviceCount();
		if (index >= numDevices || index < 0)
		{
			return noDevice;
		}
		auto info = Pa_GetDeviceInfo(index);
		return {
			index, info->hostApi, info->name, info->maxInputChannels, info->maxOutputChannels,
			info->defaultLowInputLatency, info->defaultLowOutputLatency,
			info->defaultHighInputLatency, info->defaultHighOutputLatency, info->defaultSampleRate
		};
	}

	std::vector<SoundInterface::DeviceInfo> PortAudio::getDevices()
	{
		std::vector<DeviceInfo> deviceInfo;
		for (int i = 0; i < Pa_GetDeviceCount(); ++i)
		{
			deviceInfo.emplace_back(getDeviceInfo(i));
		}
		return deviceInfo;
	}

	SoundInterface::ApiInfo PortAudio::getCurrentApi()
	{
		auto currentInput = getCurrentInputDevice();
		auto currentOutput = getCurrentOutputDevice();
		auto inApi = getHostApiInfo(currentInput.hostApiIndex);
		auto outApi = getHostApiInfo(currentOutput.hostApiIndex);

		if (currentInput == noDevice)
		{
			return outApi;
		}
		if (currentOutput == noDevice)
		{
			return inApi;
		}
		if (outApi != inApi)
		{
			throw std::runtime_error("[PortAudio::getCurrentApi()] Input and output devices use different APIs!");
		}
		return outApi;  // We will only get here if outApi == inApi and neither of them is noApi
	}

	SoundInterface::ApiInfo PortAudio::getDefaultApi()
	{
		return getHostApiInfo(Pa_GetDefaultHostApi());
	}

	SoundInterface::DeviceInfo PortAudio::getCurrentInputDevice()
	{
		return getDeviceInfo(inputStreamParameters_.device);
	}

	SoundInterface::DeviceInfo PortAudio::getCurrentOutputDevice()
	{
		return getDeviceInfo(outputStreamParameters_.device);
	}

	SoundInterface::DeviceInfo PortAudio::getDefaultInputDevice()
	{
		return getDeviceInfo(Pa_GetDefaultInputDevice());
	}

	SoundInterface::DeviceInfo PortAudio::getDefaultOutputDevice()
	{
		return getDeviceInfo(Pa_GetDefaultOutputDevice());
	}

	void PortAudio::setInputDevice(const SoundInterface::DeviceInfo& device)
	{
		setInputDevice(device.index);
	}

	void PortAudio::setInputDevice(int index)
	{
		inputStreamParameters_.device = index;
	}

	void PortAudio::setOutputDevice(const SoundInterface::DeviceInfo& device)
	{
		setOutputDevice(device.index);
	}

	void PortAudio::setOutputDevice(int index)
	{
		outputStreamParameters_.device = index;
	}

	bool PortAudio::isSamplingRateSupported(DeviceInfo device, unsigned samplingRate_Hz)
	{
		StreamParameters params;
		params.channelCount = 1;
		params.device = device.index;
		params.sampleFormat = inputStreamParameters_.sampleFormat;
		params.suggestedLatency = device.defaultHighOutputLatency;
		bool inputSupported = true;
		if (device.maxInputChannels > 0)
		{
			inputSupported = checkStreamParameters(samplingRate_Hz, &params, nullptr) != paInvalidSampleRate;
		}
		bool outputSupported = true;
		if (device.maxOutputChannels > 0)
		{
			outputSupported = checkStreamParameters(samplingRate_Hz, nullptr, &params) != paInvalidSampleRate;
		}

		return inputSupported && outputSupported;
	}

	PaError PortAudio::checkStreamParameters(double samplingRate_Hz, const StreamParameters* inputParams,
	                                      const StreamParameters* outputParams)
	{
		return Pa_IsFormatSupported(inputParams, outputParams, samplingRate_Hz);
	}

	double PortAudio::getPlaybackSamplingRate()
	{
		return *playbackData_.samplingRate_Hz;
	}

	void PortAudio::setPlaybackSamplingRate(unsigned samplingRate_Hz)
	{
		*playbackData_.samplingRate_Hz = samplingRate_Hz;
	}

	SoundInterface::Status PortAudio::getPlaybackStatus()
	{
		if (Pa_IsStreamActive(outputStream_) == 1)
		{
			return Status::ACTIVE;
		}
		if (playbackData_.readPos > playbackData_.startPos)
		{
			return Status::PAUSED;
		}
		return Status::STOPPED;
	}

	SoundInterface::Status PortAudio::playAsynchronously()
	{
		double end_s = static_cast<double>(playbackData_.buffer->size()) / getPlaybackSamplingRate();
		return playAsynchronously(0.0, end_s);
	}

	SoundInterface::Status PortAudio::playAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz)
	{
		setPlaybackBuffer(buffer, samplingRate_Hz);
		return playAsynchronously();
	}

	SoundInterface::Status PortAudio::playAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz,
	                                                     double start_s, double end_s)
	{
		setPlaybackBuffer(buffer, samplingRate_Hz);
		return playAsynchronously(start_s, end_s);
	}

	SoundInterface::Status PortAudio::playAsynchronously(double start_s, double end_s)
	{
		playbackData_.startPos = std::max(0, static_cast<int>(getPlaybackSamplingRate() * start_s));
		playbackData_.endPos = std::min(static_cast<int>(getPlaybackSamplingRate() * end_s),
			static_cast<int>(playbackData_.buffer->size()));

		Pa_CloseStream(outputStream_);

		const auto errorCode = checkStreamParameters(getPlaybackSamplingRate(), nullptr, &outputStreamParameters_);
		if (errorCode == paInvalidDevice)
		{
			throw std::runtime_error("[PortAudio::playAsynchronously()] Output device not valid!");
		}

		if (errorCode != paFormatIsSupported)
		{
			throw std::runtime_error("[PortAudio::playAsynchronously()] Format not supported by output device!");
		}

		Pa_OpenStream(&outputStream_,
			nullptr,
			&outputStreamParameters_,
			*playbackData_.samplingRate_Hz,
			paFramesPerBufferUnspecified,
			paNoFlag,
			outputCallback,
			this);
		Pa_StartStream(outputStream_);

		return getPlaybackStatus();
	}

	bool PortAudio::playBlocking()
	{
		double end_s = static_cast<double>(playbackData_.buffer->size()) / getPlaybackSamplingRate();
		return playBlocking(0.0, end_s);
	}

	bool PortAudio::playBlocking(std::vector<double>* buffer, unsigned* samplingRate_Hz)
	{
		setPlaybackBuffer(buffer, samplingRate_Hz);
		return playBlocking();
	}

	bool PortAudio::playBlocking(double start_s, double end_s)
	{
		auto startPos = std::max(0, static_cast<int>(getPlaybackSamplingRate() * start_s));
		auto endPos = std::min(static_cast<int>(getPlaybackSamplingRate() * end_s),
			static_cast<int>(playbackData_.buffer->size()));

		Pa_CloseStream(outputStream_);

		const auto errorCode = checkStreamParameters(getPlaybackSamplingRate(), nullptr, &outputStreamParameters_);
		if (errorCode == paInvalidDevice)
		{
			throw std::runtime_error("[PortAudio::playBlocking()] Output device not valid!");
		}

		if (checkStreamParameters(getPlaybackSamplingRate(), nullptr, &outputStreamParameters_) != paFormatIsSupported)
		{
			throw std::runtime_error("[PortAudio::playBlocking()] Format not supported by output device!");
		}

		auto err = Pa_OpenStream(&outputStream_,
			nullptr,
			&outputStreamParameters_,
			getPlaybackSamplingRate(),
			paFramesPerBufferUnspecified,
			paNoFlag,
			nullptr,
			nullptr);
		std::vector<float> buffer;
		for (int pos = startPos; pos < endPos; ++pos)
		{
			buffer.push_back(static_cast<float>(playbackData_.buffer->at(pos)));
		}
		Pa_StartStream(outputStream_);
		auto num = Pa_GetStreamWriteAvailable(outputStream_);
		auto ret = Pa_WriteStream(outputStream_,
		                          &buffer[0], static_cast<unsigned>(buffer.size()));
		
		Pa_StopStream(outputStream_);
		Pa_CloseStream(outputStream_);
		return true;
	}

	bool PortAudio::pausePlayback()
	{
		Pa_StopStream(outputStream_);
		return Pa_IsStreamActive(outputStream_);
	}

	bool PortAudio::stopPlayback()
	{
		Pa_StopStream(outputStream_);
		resetPlayer();
		return Pa_IsStreamActive(outputStream_);
	}

	void PortAudio::resetPlayer()
	{
		playbackData_.readPos = 0;		
	}

	void PortAudio::setPlaybackBuffer(std::vector<double>* newBuffer, unsigned* samplingRate_Hz)
	{
		playbackData_.buffer = newBuffer;
		playbackData_.samplingRate_Hz = samplingRate_Hz;
		playbackData_.startPos = 0;
		playbackData_.endPos = static_cast<unsigned>(newBuffer->size());
		playbackData_.readPos = 0;
	}

	void PortAudio::setPlaybackStartPos(unsigned newStartPos)
	{
		playbackData_.startPos = newStartPos;
	}

	void PortAudio::setPlaybackEndPos(unsigned newEndPos)
	{
		playbackData_.endPos = newEndPos;
	}

	double PortAudio::getRecordingSamplingRate()
	{
		return *recordingData_.samplingRate_Hz;
	}

	void PortAudio::setRecordingSamplingRate(unsigned samplingRate_Hz)
	{
		*recordingData_.samplingRate_Hz = samplingRate_Hz;
	}

	SoundInterface::Status PortAudio::recordAsynchronously()
	{
		// Close any open stream, just to be safe.
		Pa_CloseStream(inputStream_);

		const auto errorCode = checkStreamParameters(getRecordingSamplingRate(), &inputStreamParameters_, nullptr);

		if (errorCode == paInvalidDevice)
		{
			throw std::runtime_error("[PortAudio::recordAsynchronously()] Input device not valid!");
		}

		if (checkStreamParameters(getRecordingSamplingRate(), &inputStreamParameters_, nullptr) != paFormatIsSupported)
		{
			throw std::runtime_error("[PortAudio::recordAsynchronously()] Format not supported by input device!");
		}

		Pa_OpenStream(&inputStream_,
		              &inputStreamParameters_,
		              nullptr,
		              getRecordingSamplingRate(),
		              paFramesPerBufferUnspecified, paNoFlag,
		              &inputCallback, &recordingData_);

		Pa_StartStream(inputStream_);

		return getRecordingStatus();
	}

	SoundInterface::Status PortAudio::recordAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz)
	{
		setRecordingBuffer(buffer, samplingRate_Hz);

		return playAsynchronously();
	}

	SoundInterface::Status PortAudio::recordBlocking(double time_s)
	{
		const auto errorCode = checkStreamParameters(getRecordingSamplingRate(), &inputStreamParameters_, nullptr);

		if (errorCode == paInvalidDevice)
		{
			throw std::runtime_error("[PortAudio::recordBlocking()] Input device not valid!");
		}

		if (checkStreamParameters(getRecordingSamplingRate(), &inputStreamParameters_) != paFormatIsSupported)
		{
			throw std::runtime_error("[PortAudio::recordBlocking()] Format not supported by input device!");
		}

		Pa_CloseStream(inputStream_);

		auto err = Pa_OpenStream(&inputStream_,
		                         &inputStreamParameters_,
		                         nullptr,
		                         getRecordingSamplingRate(),
		                         paFramesPerBufferUnspecified,
		                         paNoFlag,
		                         nullptr,
		                         nullptr);

		const auto numSamples = static_cast<unsigned>(time_s * getRecordingSamplingRate());
		std::vector<float> tempBuffer(numSamples);
		err = Pa_StartStream(inputStream_);
		auto ret = Pa_ReadStream(inputStream_,
		                         &tempBuffer[0], static_cast<unsigned>(tempBuffer.size()));

		recordingData_.buffer->clear();
		for (const float sample : tempBuffer)
		{
			recordingData_.buffer->push_back(static_cast<double>(sample));
		}

		Pa_StopStream(outputStream_);
		Pa_CloseStream(outputStream_);

		return getRecordingStatus();
	}

	SoundInterface::Status PortAudio::recordBlocking(std::vector<double>* buffer, unsigned* samplingRate_Hz, double time_s)
	{
		setRecordingBuffer(buffer, samplingRate_Hz);
		return recordBlocking(time_s);
	}

	bool PortAudio::pauseRecording()
	{
		return false;
	}

	SoundInterface::Status PortAudio::stopRecording()
	{
		Pa_StopStream(inputStream_);
		return getRecordingStatus();
	}

	bool PortAudio::resetRecording()
	{
		stopRecording();
		recordingData_.buffer->clear();
		return false;
	}

	SoundInterface::Status PortAudio::getRecordingStatus()
	{
		if (Pa_IsStreamActive(inputStream_) == 1)
		{
			return Status::ACTIVE;
		}
		if (recordingData_.readPos > recordingData_.startPos)
		{
			return Status::PAUSED;
		}
		return Status::STOPPED;
	}

	void PortAudio::setRecordingBuffer(std::vector<double>* newBuffer, unsigned* samplingRate_Hz)
	{
		recordingData_.buffer = newBuffer;
		recordingData_.samplingRate_Hz = samplingRate_Hz;
		recordingData_.startPos = 0;
		recordingData_.endPos = static_cast<unsigned>(newBuffer->size());
		recordingData_.readPos = 0;
	}

	bool PortAudio::attachRecordingBuffer(bool attach)
	{
		recordingData_.isAttached = attach;
		return true;
	}

	bool PortAudio::detachRecordingBuffer(bool detach)
	{
		recordingData_.isAttached = !detach;
		return true;
	}

	int PortAudio::outputCallback(const void* inputBuffer, void* outputBuffer, unsigned long framesPerBuffer,
	                              const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
	                              void* userData)
	{
		const auto audioPlayer = static_cast<PortAudio*>(userData);
		const auto data = audioPlayer->getPlaybackData();

		auto out = static_cast<float*>(outputBuffer);

		(void)inputBuffer; /* Prevent unused variable warning. */

		for (unsigned i = 0; i < framesPerBuffer; i++)
		{
			if (data->startPos + data->readPos >= data->endPos)
			{
				if (data->startPos != data->endPos && audioPlayer->isRepeating())
				{
					data->readPos = 0;
					return paContinue;
				}
				data->readPos = 0;
				return paComplete;
			}
			// Copy the same sample to each of the two output channels
			*out = static_cast<float>((*data->buffer)[data->startPos + data->readPos]);
			out++;
			*out = static_cast<float>((*data->buffer)[data->startPos + data->readPos]);
			out++;
			data->readPos++;
		}
		return paContinue;
	}

	int PortAudio::inputCallback(const void* inputBuffer, void* outputBuffer, unsigned long framesPerBuffer,
	                             const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
	                             void* userData)
	{
		(void)outputBuffer; /* Prevent unused variable warning. */

		auto in = static_cast<const float*>(inputBuffer);
		const auto data = static_cast<paData*>(userData);

		if (!data->isAttached) return paContinue;

		for (unsigned int i = 0; i < framesPerBuffer; i++)
		{
			data->buffer->push_back(static_cast<double>(*in++));
		}
		return paContinue;
	}
}


SoundInterface& SoundInterface::getInstance()
{
#if defined(USE_PORTAUDIO)
		static PortAudio instance;
#else
#error No sound interface available!
#endif
		return instance;
}
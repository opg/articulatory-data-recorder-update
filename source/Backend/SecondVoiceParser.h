#pragma once
#include "DataParser.h"
class SecondVoiceParser :
    public DataParser
{
public:
	SecondVoiceParser(const std::vector<Sensor>& sensors, std::vector<int> contactMapping)
		: DataParser(sensors), contactIndexMap(contactMapping)
	{

	}

	Frame parse(const std::vector<std::byte>& dataToParse) override;

private:
	std::vector<int> contactIndexMap;
	
};


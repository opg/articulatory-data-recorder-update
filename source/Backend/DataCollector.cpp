#include "DataCollector.h"
#include <cstring>
#include <string>

#include <chrono>
#include <cstring>
#include <string>
#include <thread>

#include <chrono>
#include <cstring>
#include <string>
#include <thread>

wxDEFINE_EVENT(DataCollectedEvent, wxCommandEvent);

DataCollector::DataCollector(FrameBuffer& buffer, const std::vector<Sensor>& sensors, DataParser* dataParser, const SerialPort::PortSettings& serialPortSettings)
	: DataCollector(nullptr, buffer, sensors, dataParser, serialPortSettings)
{

}

DataCollector::DataCollector(wxEvtHandler* owner, FrameBuffer& buffer, const std::vector<Sensor>& sensors,
	DataParser* dataParser, const SerialPort::PortSettings& serialPortSettings) : wxTimer(owner), parser(dataParser), sensors_(sensors), serialPort_{ serialPortSettings }, buffer_(buffer)
{
	buffer.clear();
	for (const auto& sensor : sensors)
	{
		if (sensor.inputDimensions.size() == 1)
		{
			ScalarSignal s(sensor);
			buffer.addScalarSignal(s);
		}
		else
		{
			MatrixSignal s(sensor);
			buffer.addMatrixSignal(s);
		}
	}

	if (serialPort_.open())
	{
		//wxLogMessage("Serial port COM2 successfully opened!");
	}
	else
	{
		wxString message = wxString("Could not open serial port ") + wxString(serialPort_.getSettings().portName) + wxString(" !");
		wxLogError(message);
	}
}

bool DataCollector::run(const bool& start)
{
	if (start && !this->IsRunning())
	{
		if (!serialPort_.isOpen())
		{
			return false;
		}
		serialPort_.flushBuffer();
		if (hasTwoWayCommunication_)
		{
			sendCommand(startCommand_);
		}
		return this->Start(refreshPeriod_ms_, false);
	}

	if(!start && this->IsRunning())
	{
		if (hasTwoWayCommunication_)
		{
			sendCommand(stopCommand_);
		}
		this->Stop();
		// Collect data one last time
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
		this->Notify();
		return true;
	}

	return this->IsRunning();
}

void DataCollector::reset()
{
	this->run(false);
	buffer_.reset();
}

bool DataCollector::setSerialPortSettings(const SerialPort::PortSettings& settings)
{
	if (serialPort_.isOpen())
	{
		serialPort_.close();
	}
	serialPort_ = SerialPort(settings);
	return serialPort_.open();
}

const SerialPort::PortSettings& DataCollector::getSerialPortSettings() const
{
	return serialPort_.getSettings();
}

bool DataCollector::sendCommand(const std::string& command)
{
	if (!hasTwoWayCommunication_)
	{
		return false;
	}

	const char* c_command = command.c_str();
	const size_t bytesToWrite = strlen(c_command);
	const int bytesWritten = serialPort_.writeData(c_command, static_cast<int>(bytesToWrite));
	if (static_cast<size_t>(bytesWritten) != bytesToWrite)
	{
		return false;
	}

	// Everything went well
	return true;
}

void DataCollector::Notify()
{
	try
	{
		this->collectData();
		postDataCollectedEvent();
	}		
	catch (const std::overflow_error& e)
	{
		this->run(false);
		wxLogWarning(wxString(e.what()) + " Recording stopped.");
	}
}

void DataCollector::postDataCollectedEvent()
{
	const wxCommandEvent event(DataCollectedEvent);
	wxPostEvent(this->GetOwner(), event);
}


#pragma once
#include "File.h"
#include "Palate.h"

#include <string>

class PalateFile : public File
{
public:
	PalateFile() = default;
	PalateFile(Palate palate) : palate_(palate){}

	void print();
	Palate getPalate() const;
	
protected:
	void readFromStream(std::istream& is) override;
	void writeToStream(std::ostream& os) override;

private:
	void parseFile(const std::string& fileAsString);
	static std::vector<std::pair<double, double>> string2pairs(const std::string& stringWithPairs);
	static std::string pairs2string(const std::vector<std::pair<double, double>>& pairs);
	static std::vector<double> string2values(const std::string& stringWithValues);
	static std::string values2string(const std::vector<double>& values);
	
	Palate palate_;
};


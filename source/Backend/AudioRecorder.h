#pragma once

#include <stdexcept>

#include "ScalarSignal.h"
#include "portaudio.h"

class AudioRecorder
{
public:
	struct StreamParameters : PaStreamParameters
	{
		StreamParameters() : PaStreamParameters()
		{
			Pa_Initialize();
			device = Pa_GetDefaultInputDevice();
			channelCount = 1;
			sampleFormat = paInt16;
			if (device == paNoDevice)
			{
				suggestedLatency = 0;
			}
			else
			{
				suggestedLatency = Pa_GetDeviceInfo(device)->defaultLowInputLatency;
			}			
			hostApiSpecificStreamInfo = nullptr;
		}
		StreamParameters(PaDeviceIndex device, int channelCount, PaSampleFormat sampleFormat, PaTime suggestedLatency, void* hostApiSpecificStreamInfo)
			: PaStreamParameters()
		{
			Pa_Initialize();
			this->device = device;
			this->channelCount = channelCount;
			this->sampleFormat = sampleFormat;
			this->suggestedLatency = suggestedLatency;
			this->hostApiSpecificStreamInfo = hostApiSpecificStreamInfo;
		}
		StreamParameters(const StreamParameters& rhs)
		{
			Pa_Initialize();
			device = rhs.device;
			channelCount = rhs.channelCount;
			sampleFormat = rhs.sampleFormat;
			suggestedLatency = rhs.suggestedLatency;
			hostApiSpecificStreamInfo = rhs.hostApiSpecificStreamInfo;
		}
		~StreamParameters() { Pa_Terminate(); }
	};


	AudioRecorder(ScalarSignal* signalBuffer);
	~AudioRecorder();
	bool attachBuffer(bool attach = true);
	bool detachBuffer(bool detach = true);
	bool run(const bool& start = true);
	bool run_detached(const bool& start = true);
	bool start() { return this->run(true); }
	bool stop() { return this->run(false); }
	void reset();

public:
	typedef struct
	{
		ScalarSignal* audioBuffer;
		bool isAttached;  // The buffer can be detached, i.e., the audio stream is running but the gathered samples are discarded.
	}
	paData;

	// Audio data object for Portaudio stream
	paData audioData;


	std::map<int, std::string> getInputDevices() const;
	int getCurrentInputDevice() const;
	void setInputDevice(int index);

	bool isRunning() const;

	bool restartStream();

private:
	// This is the Portaudio callback function. It must have exactly this signature!
	static int recordAudio(const void* inputBuffer, void* outputBuffer,
	                       unsigned long framesPerBuffer,
	                       const PaStreamCallbackTimeInfo* timeInfo,
	                       PaStreamCallbackFlags statusFlags,
	                       void* userData);

private:
	PaStream* audioStream;
	bool isRunning_{ false };
	StreamParameters streamParameters_;
};


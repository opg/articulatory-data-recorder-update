#include "FrameBuffer.h"
#include <stdexcept>

FrameBuffer::FrameBuffer(const int maxNumFrames) : maxNumFrames_(maxNumFrames), numScalarSignals_(0),
                                                   numMatrixSignals_(0)
{
}

FrameBuffer::FrameBuffer(const int numScalarSignals, const int numMatrixSignals, const int maxNumFrames)
	: maxNumFrames_(maxNumFrames), numScalarSignals_(numScalarSignals), numMatrixSignals_(numMatrixSignals)
{
	this->reset();
}

Frame FrameBuffer::getFrame(size_t index)
{
	Frame frame;
	frame.index = frameIndex_[index];
	for (const auto& signal : scalarSignals)
	{
		frame.scalarSensor.push_back(signal[index]);
	}
	for (const auto& signal : matrixSignals)
	{
		frame.matrixSensor.push_back(signal[index]);
		frame.dimensions.push_back(signal.dimensions);
	}
	return frame;
}

Frame FrameBuffer::getCurrentFrame()
{
	return getFrame(currentFrameIndex_);
}

int FrameBuffer::getCurrentFrameIndex() const
{
	return currentFrameIndex_;
}

int FrameBuffer::getLastFrameIndex() const
{
	return lastFrameIndex_;
}

int FrameBuffer::getSamplingRate() const
{
	return samplingRate_;
}

ScalarSignal& FrameBuffer::getScalarSignal(int index)
{
	return this->scalarSignals[index];
}

std::vector<ScalarSignal>& FrameBuffer::getScalarSignals()
{
	return this->scalarSignals;
}

MatrixSignal& FrameBuffer::getMatrixSignal(int index)
{
	return this->matrixSignals[index];
}

const std::vector<MatrixSignal>& FrameBuffer::getMatrixSignals() const
{
	return this->matrixSignals;
}

std::vector<std::vector<int>> FrameBuffer::getMatrixDimensions() const
{
	std::vector<std::vector<int>> dimensions;
	for(const auto& signal : matrixSignals)
	{
		dimensions.push_back(signal.dimensions);
	}

	return dimensions;
}

int FrameBuffer::maxNumFrames() const
{
	return maxNumFrames_;
}

int FrameBuffer::numScalarSignals() const
{
	return numScalarSignals_;
}

int FrameBuffer::numMatrixSignals() const
{
	return numMatrixSignals_;
}

void FrameBuffer::setMaxNumFrames(int maxNumFrames)
{
	maxNumFrames_ = maxNumFrames;
	this->resize();
	this->reserve();
}

void FrameBuffer::setNumScalarSignals(int numScalarSignals)
{
	numScalarSignals_ = numScalarSignals;
	this->resize();
	this->reserve();
}

void FrameBuffer::addMatrixSignal(const MatrixSignal& newMatrixSignal)
{
	if(samplingRate_ != 0)
	{
		if (samplingRate_ != newMatrixSignal.getSamplingRate_Hz())
		{
			throw std::logic_error("Added signal " + newMatrixSignal.name + " has the wrong sampling rate!");
		}
	}
	else
	{
		samplingRate_ = newMatrixSignal.getSamplingRate_Hz();
	}
	matrixSignals.push_back(newMatrixSignal);
	numMatrixSignals_ = static_cast<int>(matrixSignals.size());
}

void FrameBuffer::addScalarSignal(const ScalarSignal& newScalarSignal)
{
	if (samplingRate_ != 0)
	{
		if (samplingRate_ != newScalarSignal.getSamplingRate_Hz())
		{
			throw std::logic_error("Added signal " + newScalarSignal.name + " has the wrong sampling rate!");
		}
	}
	else
	{
		samplingRate_ = newScalarSignal.getSamplingRate_Hz();
	}
	scalarSignals.push_back(newScalarSignal);
	numScalarSignals_ = static_cast<int>(scalarSignals.size());
}

void FrameBuffer::setNumMatrixSignals(int numMatrixSignals)
{
	numMatrixSignals_ = numMatrixSignals;
	this->resize();
	this->reserve();
}

void FrameBuffer::reserve()
{
	for (auto& signal : scalarSignals)
	{
		signal.reserve(maxNumFrames_);
	}

	for (auto& signal : matrixSignals)
	{
		signal.reserve(maxNumFrames_);
	}
}

void FrameBuffer::clear()
{
	scalarSignals.clear();
	numScalarSignals_ = 0;
	matrixSignals.clear();
	numMatrixSignals_ = 0;
	lastFrameIndex_ = -1;
	currentFrameIndex_ = -1;
	samplingRate_ = 0;
}

void FrameBuffer::resize()
{
	scalarSignals.resize(numScalarSignals_);
	matrixSignals.resize(numMatrixSignals_);
}

void FrameBuffer::reset()
{
	for (auto& signal : scalarSignals)
	{
		signal.clear();
	}
	for (auto& signal : matrixSignals)
	{
		signal.clear();
	}
	lastFrameIndex_ = -1;
	currentFrameIndex_ = -1;
	this->reserve();
}

size_t FrameBuffer::capacity() const
{
	if (!scalarSignals.empty())
	{
		return scalarSignals.front().capacity();
	}
	if (!matrixSignals.empty())
	{
		return matrixSignals.front().capacity();
	}
	// If both are empty
	return 0;
}

size_t FrameBuffer::size() const
{
	if (!scalarSignals.empty())
	{
		return scalarSignals.front().size();
	}
	if (!matrixSignals.empty())
	{
		return matrixSignals.front().size();
	}
	// If both are empty
	return 0;
}

void FrameBuffer::addFrame(Frame newFrame)
{
	frameIndex_.push_back(newFrame.index);

	if (scalarSignals.size() != newFrame.scalarSensor.size() || matrixSignals.size() != newFrame.matrixSensor.size())
	{
		throw std::invalid_argument("Frame format wrong! Incorrect hardware backend selected?");
	}

	for (size_t i = 0; i < newFrame.scalarSensor.size(); ++i)
	{
		scalarSignals[i].push_back(newFrame.scalarSensor[i]);
	}

	for (size_t i = 0; i < newFrame.matrixSensor.size(); ++i)
	{
		matrixSignals[i].push_back(newFrame.matrixSensor[i]);
	}

	lastFrameIndex_++;
}

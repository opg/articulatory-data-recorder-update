#pragma once
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "Calibrator.h"


// A struct representing a sensor
struct Sensor
{
	Sensor() = default;
	Sensor(const std::string& name, const std::string& unitX, const std::string& unitY,
		const std::vector<int>& inputDimensions, const int samplingRate, const std::pair<double, double>& valueRange, 
		Calibrator<double, double>* calibrator = nullptr, const std::string& unitCalibratedY = "",
		const std::pair<double,double>& calibratedRange= std::make_pair(0.0, 1.0))
		: name(name),
		unit_x(unitX),
		unit_y(unitY),
		inputDimensions(inputDimensions),
		samplingRate(samplingRate),
		valueRange(valueRange),
		calibrator(calibrator),
		unit_calibrated_y(unitCalibratedY),
		calibratedRange(calibratedRange)
	{
	}


	std::string name{ "s" };
	std::string unit_x{ "" };  // Quantity of the dimension of sampling (most likely time)
	std::string unit_y{ "" };  // Quantity that is measured
	std::shared_ptr<Calibrator<double, double>> calibrator{ nullptr };  // Calibrator to convert unit_y (optional, may be nullptr) to unit_calibrated_y
	std::string unit_calibrated_y{ "" };  // Quantity after calibration
	
	
	std::vector<int> inputDimensions{ 1 };  // e.g., 1 for a standard distance sensor or 2 for a contact matrix
	int samplingRate{ 100 };  // sampling rate must be the same for all sensors of a hardware backend
	std::pair<double, double> valueRange{ 0.0, 4095.0 };  // raw value range; e.g., (0, 4095) for a 12-bit converted sensor value
	std::pair<double, double> calibratedRange{ 0.0, 1.0 };  // value range after calibration (if applicable)
	std::map<int, std::vector<int>> matrixIndexMap;  // Optional map to map index of matrix sensor data to a Cartesian index; only necessary if data is not already neatly linearly indexed!
	
};


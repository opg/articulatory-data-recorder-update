#pragma once
#include "ByteDataCollector.h"
#include "EosParser.h"
#include "PiecewiseLinearCalibrator.h"

class EosBackend :
    public ByteDataCollector
{
	using EosCalibrator = PiecewiseLinearCalibrator<double, double>;
	
public:
	EosBackend(wxEvtHandler* owner, FrameBuffer& buffer)
	: ByteDataCollector(owner, buffer, sensors, new EosParser(sensors),
		std::make_pair(std::byte{ 0xA5 }, std::byte{ 0x5A }),
		48, portSettings)
	{
	}

	static const int samplingRate_Hz{ 100 };
	inline static const std::vector<Sensor> sensors
	{
		Sensor("Lip 0", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Lip 1", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("D0", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0),
		new EosCalibrator({{0.0, 0.0}, {4095.0, 30.0}}), "mm", std::make_pair(0.0, 30.0)),
		Sensor("D1", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0),
		new EosCalibrator({{0.0, 0.0}, {4095.0, 30.0}}), "mm", std::make_pair(0.0, 30.0)),
		Sensor("D2", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0),
		new EosCalibrator({{0.0, 0.0}, {4095.0, 30.0}}), "mm", std::make_pair(0.0, 30.0)),
		Sensor("D3", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0),
		new EosCalibrator({{0.0, 0.0}, {4095.0, 30.0}}), "mm", std::make_pair(0.0, 30.0)),
		Sensor("D4", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0),
		new EosCalibrator({{0.0, 0.0}, {4095.0, 30.0}}), "mm", std::make_pair(0.0, 30.0)),
		Sensor("Contacts", "s", "", std::vector<int>{8, 8}, samplingRate_Hz, std::make_pair(0.0, 1.0))
	};
	inline static const SerialPort::PortSettings portSettings{ "COM3", 115200, SerialPort::NO_PARITY, SerialPort::ONE_STOP_BIT, false, 0, 0 };

};


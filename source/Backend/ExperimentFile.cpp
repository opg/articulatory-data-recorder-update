#include "ExperimentFile.h"

#include <sstream>


ExperimentFile::ExperimentFile(Experiment<std::string> experiment) : experiment_(experiment)
{
	
}

Experiment<std::string> ExperimentFile::getExperiment() const
{
	return experiment_;
}

void ExperimentFile::readFromStream(std::istream& is)
{
	std::string line;
	size_t numItems;
	size_t lineIdx = 0;
	while (std::getline(is, line))
	{
		// First line is a header
		if(lineIdx == 0)
		{
			std::string header;
			if (line != "ADR Experiment File")
			{
				throw std::ios_base::failure("File does not seem to be an ADR Experiment File!");
			}
			lineIdx++;
			continue;
		}

		// Second line are the experiment settings
		if(lineIdx == 1)
		{
			std::istringstream iss(line);
			int repetitions;
			std::string randStrategy;
			iss >> repetitions >> randStrategy >> numItems;
			experiment_.settings = { repetitions, RandomizationStrategy::ToType(randStrategy) };
			lineIdx++;
			continue;
		}

		// Third line is the intro message
		if(lineIdx == 2)
		{
			experiment_.introMessage = line;
			lineIdx++;
			continue;
		}

		// Next numLines lines are the inventory items
		if(lineIdx > 2 && lineIdx <= numItems  + 2)
		{
			experiment_.inventory.push_back(line);
			lineIdx++;
			continue;
		}

		// Final line is the outro message
		experiment_.outroMessage = line;
		lineIdx++;
	}
}

void ExperimentFile::writeToStream(std::ostream& os)
{
	os << "ADR Experiment File" << std::endl;
	os << experiment_.settings.repetitions << "\t"
	<< experiment_.settings.randStrategy.ToString() << "\t"
	<< experiment_.inventory.size() << std::endl;

	os << experiment_.introMessage << std::endl;
	for (const auto& item : experiment_.inventory)
	{
		os << item << std::endl;
	}
	os << experiment_.outroMessage << std::endl;		
}


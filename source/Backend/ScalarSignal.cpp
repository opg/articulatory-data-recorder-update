#include "ScalarSignal.h"

void ScalarSignal::useCalibrator(bool doUse)
{
	useCalibrator_ = doUse;
	if (isCalibrated())
	{
		current_unit_y = unit_calibrated_y;
		current_range = calibrated_range;
	}
	else
	{
		current_unit_y = unit_y;
		current_range = range;
	}
}

bool ScalarSignal::isCalibrated() const
{
	return (useCalibrator_ && calibrator_ != nullptr);
}

Calibrator<double, double>* ScalarSignal::getCalibrator()
{
	return calibrator_.get();
}

Signal<double>::value_type ScalarSignal::getValue(size_type pos) const
{
	if (isCalibrated())
	{
		return calibrator_->getCalibratedValue(this->at(pos));
	}
	return this->at(pos);
}

//
//double ScalarSignal::operator[](size_t i) const
//{
//	if (isCalibrated())
//	{
//		return calibrator_->getCalibratedValue(data[i]);
//	}
//	else
//	{
//		return data[i];
//	}
//}

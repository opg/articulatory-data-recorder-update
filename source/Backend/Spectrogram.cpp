#include "Spectrogram.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <execution>

Spectrogram::Spectrogram(int frameLength_ms, double overlap_pct, int samplingRate, double relativeCutoff, dsp::window::type windowType, double preEmphasisCoeff) : Spectrogram(Settings(frameLength_ms, overlap_pct, samplingRate, relativeCutoff, windowType, preEmphasisCoeff))
{

}

Spectrogram::Spectrogram(const Settings settings) : settings_(settings)
{
	setSettings(settings_);
}

Spectrogram& Spectrogram::transform(const std::vector<double>& signal)
{
	// Algorithm to obtain the spectrogram is:
	// 0. Pre-emphasis
	// 1. Create a number of overlapping frames from the signal
	// 2. Window each frame
	// 3. Calculate the log-squared-spectrum of each frame
	// -> Final result is a matrix of real values, each column representing one frame's log-squared magnitude spectrum

	spectrogram_.clear();

	const auto preemphasized_signal = dsp::filter::filter({ 1, -1 * getPreEmphasisCoeff() }, { 1.0 }, signal);

	// Divide into frames 
	auto frames = dsp::signalToFrames(preemphasized_signal, settings_.frameLength_, settings_.overlap_);
	spectrogram_.resize(frames.size());

	// Window the frames
	std::transform(std::execution::par_unseq,frames.begin(), frames.end(), frames.begin(),
		[this](auto f)
		{
			std::transform(f.begin(), f.end(), 
				window_.begin(), f.begin(), std::multiplies<>());
			return f;
		});

	// Calculate log magnitude spectrum
	std::transform(std::execution::par_unseq,
		frames.begin(), frames.end(),
		spectrogram_.begin(),
		[=](auto frame)
		{
			return dsp::fft::logSquaredMagnitudeSpectrum(frame, settings_.nFft_, settings_.relativeCutoff_);
		});

	return *this;
}

int Spectrogram::getNfft() const
{
	return settings_.nFft_;
}


int Spectrogram::getSamplingRate_Hz() const
{
	return settings_.samplingRate_Hz_;
}

int Spectrogram::getFrameLength(bool inSamples) const
{
	if (settings_.samplingRate_Hz_ == -1 || inSamples)
	{
		return settings_.frameLength_;
	}
	else
	{
		return static_cast<int>(1000.0 / static_cast<double>(settings_.samplingRate_Hz_) * settings_.frameLength_);
	}
}

double Spectrogram::getOverlap(bool inSamples) const
{
	if (settings_.samplingRate_Hz_ == -1 || inSamples)
	{
		return settings_.overlap_;
	}
	else
	{
		return static_cast<double>(settings_.overlap_) / settings_.frameLength_;
	}

}

std::vector<dsp::window::type> Spectrogram::getSupportedWindowTypes()
{
	return {
		dsp::window::type::boxcar,
		dsp::window::type::hamming,
		dsp::window::type::hann
	};
}

dsp::window::type Spectrogram::getWindowType() const
{
	return settings_.windowType_;
}

double Spectrogram::getMaxLevel() const
{
	std::vector<double> maxElements;
	for (const auto& row : spectrogram_)
	{
		maxElements.push_back(*std::max_element(row.begin(), row.end()));
	}

	return *std::max_element(maxElements.begin(), maxElements.end());
}

Spectrogram::Settings Spectrogram::getSettings() const
{
	return settings_;
}

double Spectrogram::getCutoff() const
{
	return settings_.relativeCutoff_;
}

double Spectrogram::getPreEmphasisCoeff() const
{
	return settings_.preEmphasisCoeff_;
}

std::vector<std::vector<double>> Spectrogram::get() const
{
	return spectrogram_;
}

void Spectrogram::setNfft(int newNfft)
{
	settings_.nFft_ = newNfft;
}

void Spectrogram::setOverlap(int newOverlap)
{
	settings_.overlap_ = newOverlap;
	settings_.frameStride_ = settings_.frameLength_ - settings_.overlap_;
}

void Spectrogram::setSamplingRate_Hz(int samplingRate_Hz)
{
	// changing sampling rate also has an effect on the frame length and the overlap [samples]
	if (samplingRate_Hz != -1 && getSamplingRate_Hz() != -1)
	{
		double samplingRateRatio = samplingRate_Hz / static_cast<double>(getSamplingRate_Hz());
		settings_.samplingRate_Hz_ = samplingRate_Hz;
		setFrameLength(static_cast<int>(abs(samplingRateRatio) * getFrameLength(true)));
		setOverlap(static_cast<int>(abs(samplingRateRatio) * getOverlap(true)));
	}
	else {
		settings_.samplingRate_Hz_ = samplingRate_Hz;
	}
}

void Spectrogram::setFrameLength(int frameLength)
{
	settings_.frameLength_ = frameLength;
	// Make sure the length of the FFT is not too short
	if (settings_.nFft_ < static_cast<int>(pow(2, dsp::nextpow2(settings_.frameLength_))))
	{
		setNfft(static_cast<int>(pow(2, dsp::nextpow2(settings_.frameLength_))));
	}
	setWindowType(this->getWindowType());
}

void Spectrogram::setCutoff(double newCutoff)
{
	settings_.relativeCutoff_ = newCutoff;
}

void Spectrogram::setWindowType(dsp::window::type newWindowType)
{
	settings_.windowType_ = newWindowType;
	window_ = dsp::window::get_window<double>(settings_.windowType_, settings_.frameLength_);
}

void Spectrogram::setPreEmphasisCoeff(double newCoeff)
{
	settings_.preEmphasisCoeff_ = newCoeff;
}

void Spectrogram::setSettings(Settings newSettings)
{
	setFrameLength(newSettings.frameLength_);
	setOverlap(newSettings.overlap_);
	setSamplingRate_Hz(newSettings.samplingRate_Hz_);
	setNfft(newSettings.nFft_);
	setWindowType(newSettings.windowType_);
	setCutoff(newSettings.relativeCutoff_);
	setPreEmphasisCoeff(newSettings.preEmphasisCoeff_);
}

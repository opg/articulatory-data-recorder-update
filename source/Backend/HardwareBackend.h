#pragma once

#include <map>
#include <string>

#include "EosBackend.h"
#include "Eos4Backend.h"
#include "OsloBackend.h"
#include "SecondVoiceBackend.h"
#include "TongueMouseBackend.h"
// Test comment for branch test.

namespace HardwareBackend
{
	enum class Type
	{
		EOS,
		EOS_4,
		SECOND_VOICE,
		OSLO,
		TONGUE_MOUSE,
		OPG
	};
	
	inline std::map<Type, std::string> type2string =
	{
		{Type::EOS, "EOS"},
		{Type::EOS_4, "EOS 4.0"},
		{Type::SECOND_VOICE, "Second Voice"},
		{Type::OSLO, "OSLO"},
		{Type::TONGUE_MOUSE, "TongueMouse"},
		{Type::OPG, "OPG 2.0"}
	};
	inline std::map<std::string, Type> string2type =
	{
		{"EOS", Type::EOS},
		{"EOS 4.0", Type::EOS_4},
		{"Second Voice", Type::SECOND_VOICE},
		{"OSLO", Type::OSLO},
		{"TongueMouse", Type::TONGUE_MOUSE},
		{"OPG 2.0", Type::OPG}
	};
}
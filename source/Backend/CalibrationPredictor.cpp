#include "CalibrationPredictor.h"

#include <stdexcept>

CalibrationPredictor::CalibrationPredictor(const std::vector<std::vector<double>>& predictorCoefficients) : predictorCoefficients_(predictorCoefficients)
{
	if (predictorCoefficients_.size() != calibrationDistances_mm_.size())
	{
		throw std::logic_error("Number of predictor sets does not match number of calibration distances!");
	}
}

CalibrationPredictor::CalibrationPredictor(const std::vector<std::vector<double>>& predictorCoefficients,
	const std::vector<double>& distances_mm) : predictorCoefficients_(predictorCoefficients), calibrationDistances_mm_(distances_mm)
{
	if (predictorCoefficients_.size() != calibrationDistances_mm_.size())
	{
		throw std::logic_error("Number of predictor sets does not match number of calibration distances!");
	}
}

std::vector<Calibrator<double, double>::calibrationPoint>  CalibrationPredictor::predictCalibrationValues(double zeroDistance_adc)
{
	std::vector<Calibrator<double, double>::calibrationPoint>  calibrationPoints;
	for (int i = 0; i < calibrationDistances_mm_.size(); ++i)
	{
		calibrationPoints.emplace_back(predictAdcValue(predictorCoefficients_[i], zeroDistance_adc), calibrationDistances_mm_[i]);
	}

	return calibrationPoints;
}

void CalibrationPredictor::setPredictorCoefficients(const std::vector<std::vector<double>>& predictorCoefficients)
{
	predictorCoefficients_ = predictorCoefficients;
}

void CalibrationPredictor::setCalibrationDistances(const std::vector<double>& distances_mm)
{
	calibrationDistances_mm_ = distances_mm;
}

double CalibrationPredictor::predictAdcValue(const std::vector<double>& a, double x0)
{
	return a[0] + a[1] * x0 + a[2] * x0 * x0;
}

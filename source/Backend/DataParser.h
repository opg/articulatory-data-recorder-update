#pragma once
#include <cstddef>
#include <vector>

#include "Frame.h"
#include "Sensor.h"

// Abstract base class for a specialized data parser (e.g., EosDataParser, SecondVoiceDataParser etc.)
class DataParser
{
public:
	DataParser(const std::vector<Sensor>& sensors) : sensors(sensors)
	{
		for (const auto& sensor : sensors)
		{
			if (sensor.inputDimensions.size() > 1)
			{
				numMatrixSensors++;
			}
			else
			{
				numScalarSensors++;
			}
		}
	}

	virtual Frame parse(const std::vector<std::byte>& dataToParse) = 0;
	std::vector<Sensor> sensors;
	int numScalarSensors{ 0 };
	int numMatrixSensors{ 0 };
};


#include "AudioRecorder.h"
#include "Dsp.h"

AudioRecorder::AudioRecorder(ScalarSignal* signalBuffer)
{
	audioData.audioBuffer = signalBuffer;
	audioData.isAttached = true;
	// Initialize portaudio
	if (paNoError != Pa_Initialize()) { throw std::runtime_error("Error initializing PortAudio!"); }

}

AudioRecorder::~AudioRecorder()
{
	Pa_Terminate();
}

bool AudioRecorder::attachBuffer(bool attach)
{
	audioData.isAttached = attach;
	return audioData.isAttached;
}

bool AudioRecorder::detachBuffer(bool detach)
{
	return !attachBuffer(!detach);
}

bool AudioRecorder::run(const bool& start)
{
	if (start)
	{
		// Close any open stream, just to be safe.
		Pa_CloseStream(&audioStream);
		Pa_OpenStream(&audioStream, 
			&streamParameters_, 
			nullptr, 
			audioData.audioBuffer->getSamplingRate_Hz(),
			paFramesPerBufferUnspecified, paNoFlag, 
			&recordAudio, &audioData);

		Pa_StartStream(audioStream);
		isRunning_ = true;
	}
	else
	{
		Pa_StopStream(audioStream);
		isRunning_ = false;
	}

	return isRunning_;
}

bool AudioRecorder::run_detached(const bool& start)
{
	detachBuffer();
	return this->run(start);
}

void AudioRecorder::reset()
{
	isRunning_ = false;
	audioData.audioBuffer->clear();
}


std::map<int, std::string> AudioRecorder::getInputDevices() const
{
	std::map<int, std::string> devices;
	auto hostApi = Pa_GetDefaultHostApi();
	devices.insert({ -1, "Default Input Device" });
	for (int i = 0; i < Pa_GetDeviceCount(); ++i)
	{
		const PaDeviceInfo* deviceInfo = Pa_GetDeviceInfo(i);
		if (deviceInfo->hostApi == hostApi && deviceInfo->maxInputChannels > 0)
		{
			devices.insert({ i, std::string(deviceInfo->name) });
		}
	}

	return devices;
}

int AudioRecorder::getCurrentInputDevice() const
{
	return streamParameters_.device;
}

void AudioRecorder::setInputDevice(int index)
{
	bool restartStream = this->isRunning() ? true : false;
	this->run(false);

	if (index == -1)
	{
		index = Pa_GetDefaultInputDevice();
		if (index == paNoDevice) { throw std::runtime_error("No default input device available!"); }
	}
	auto newParameters = streamParameters_;
	newParameters.device = index;
	PaError error = Pa_IsFormatSupported(&newParameters, nullptr, audioData.audioBuffer->getSamplingRate_Hz());
	if (error != paNoError)
	{
		throw std::runtime_error("Device does not support the current stream parameters!");
	}
	streamParameters_ = newParameters;
	
	this->run(restartStream);
}

bool AudioRecorder::isRunning() const
{
	return isRunning_;
}

bool AudioRecorder::restartStream()
{
	this->run(false);
	return this->run(true);
}

int AudioRecorder::recordAudio(const void* inputBuffer, void* outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void* userData)
{	
	(void)outputBuffer; /* Prevent unused variable warning. */
	signed short* in = (signed short*) inputBuffer;
	paData* data = (paData*) userData;

	for (unsigned int i = 0; i < framesPerBuffer; i++)
	{
		if (data->isAttached)
		{
			data->audioBuffer->push_back(static_cast<double>(*in++) / INT16_MAX);
		}		
	}
	return 0;
}

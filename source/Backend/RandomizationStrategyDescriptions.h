#pragma once

class RandomizationStrategyDescriptions
{
public:
	RandomizationStrategyDescriptions() {};
	std::string cyclicNonRandomDescription{ "The prompts will be given in the order in which they were specified in the inventory." };
	std::string withoutReplacementDescription{ "The prompts will be given randomly chosen from the inventory without memory." };
	std::string permuteAllDescription{ "The prompts will be given randomly chosen from the inventory with each inventory item appearing n = repetitions times." };
	std::string permuteBalancedDescription{ "The prompts will be given randomly chosen from the inventory with each inventory item appearing n = repetitions times and only once per repetition cycle." };
	std::string permuteBalancedNoDoubletsDescription{ "The prompts will be given randomly chosen from the inventory with each inventory item appearing n = repetitions times and only once per repetition cycle. No inventory item will appear at the beginning of a block if the previous block was concluded by the same item." };
};


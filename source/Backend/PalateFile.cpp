#include "PalateFile.h"

#include <iostream>
#include <iterator>
#include <sstream>

#include "tinyxml/tinyxml2.h"

void PalateFile::print()
{
	std::cout << "*** Palate file ***" << std::endl;
	std::cout << "Name: " << palate_.name << std::endl;
	std::cout << "Shape 2d: " << std::endl;
	for (const auto& point : palate_.shape_2d)
	{
		std::cout << "x: " << point.first << " y: " << point.second << std::endl;
	}
	std::cout << "Number of contact sensors: " << palate_.numContactSensors << std::endl;
	std::cout << "Number of distance sensors: " << palate_.numDistanceSensors << std::endl;
	for (const auto& s : palate_.distanceSensors)
	{
		std::cout << "Distance sensor " << s.index << std::endl;
		std::cout << "x_cm: " << s.x_cm
		<< " y_cm: " << s.y_cm
		<< " angle_deg: " << s.angle_deg
		<< std::endl;
		std::cout << "Calibration: " << std::endl;
		for (const auto& p : s.calibration_dc_mm_adc)
		{
			std::cout << "mm: " << p.first << " adc: " << p.second << std::endl;
		}
		std::cout << "Triple PT coefficients: ";
		for (const auto& c : s.coefficients_triplePT)
		{
			std::cout << c << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "****************" << std::endl;
}

Palate PalateFile::getPalate() const
{
	return palate_;
}

void PalateFile::readFromStream(std::istream& is)
{
	std::stringstream rawText;
	rawText << is.rdbuf();

	parseFile(rawText.str());
}

void PalateFile::writeToStream(std::ostream& os)
{
	// Create the XML doc and then write to stream
	tinyxml2::XMLDocument doc;
	auto root = doc.NewElement("palate");
	root->SetAttribute("name", palate_.name.c_str());
	doc.InsertFirstChild(root);
	
	auto shape2d = doc.NewElement("shape_2d");
	shape2d->SetAttribute("points_x_y_cm", pairs2string(palate_.shape_2d).c_str());
	root->InsertEndChild(shape2d);

	auto contact_sensors = doc.NewElement("contact_sensors");
	contact_sensors->SetAttribute("quantity", palate_.numContactSensors);
	root->InsertEndChild(contact_sensors);

	auto light_sensors = doc.NewElement("light_sensors");
	light_sensors->SetAttribute("quantity", palate_.numDistanceSensors);
	root->InsertEndChild(light_sensors);

	unsigned int idx = 0;
	for (const auto& s : palate_.distanceSensors)
	{
		auto light_sensor = doc.NewElement("light_sensor");
		light_sensor->SetAttribute("index", idx++);
		light_sensor->SetAttribute("x_cm", std::to_string(s.x_cm).c_str());
		light_sensor->SetAttribute("y_cm", std::to_string(s.y_cm).c_str());
		light_sensor->SetAttribute("angle_deg", std::to_string(s.angle_deg).c_str());
		light_sensor->SetAttribute("calibration_dc_mm_adc", pairs2string(s.calibration_dc_mm_adc).c_str());
		light_sensor->SetAttribute("coefficients_triplePT", values2string(s.coefficients_triplePT).c_str());
		light_sensors->InsertEndChild(light_sensor);
	}
	
	
	tinyxml2::XMLPrinter printer;
	doc.Print(&printer);

	os << printer.CStr();	
}

void PalateFile::parseFile(const std::string& fileAsString)
{
	tinyxml2::XMLDocument doc;
	doc.Parse(fileAsString.c_str());

	// Get name
	palate_.name = doc.RootElement()->Attribute("name");
	
	// Get shape 2d points
	const auto shapeString = doc.RootElement()->FirstChildElement("shape_2d")->Attribute("points_x_y_cm");
	palate_.shape_2d = string2pairs(shapeString);
	
	// Get contact sensor information
	palate_.numContactSensors = std::stoi(doc.RootElement()->FirstChildElement("contact_sensors")->Attribute("quantity"));

	// Get distance sensor information
	palate_.numDistanceSensors = std::stoi(doc.RootElement()->FirstChildElement("light_sensors")->Attribute("quantity"));

	auto lightSensorXml = doc.RootElement()->FirstChildElement("light_sensors")->FirstChildElement("light_sensor");
	while(true)
	{
		Palate::LightSensor s;
		s.index = std::stoi(lightSensorXml->Attribute("index"));
		s.x_cm = std::stod(lightSensorXml->Attribute("x_cm"));
		s.y_cm = std::stod(lightSensorXml->Attribute("y_cm"));
		s.angle_deg = std::stod(lightSensorXml->Attribute("angle_deg"));
		s.calibration_dc_mm_adc = string2pairs(lightSensorXml->Attribute("calibration_dc_mm_adc"));
		s.coefficients_triplePT = string2values(lightSensorXml->Attribute("coefficients_triplePT"));

		palate_.distanceSensors.push_back(s);
		
		lightSensorXml = lightSensorXml->NextSiblingElement("light_sensor");
		if (!lightSensorXml)
		{
			break;
		}
	}
}

std::vector<std::pair<double, double>> PalateFile::string2pairs(const std::string& stringWithPairs)
{
	std::istringstream iss(stringWithPairs);
	std::istream_iterator<std::string> start(iss), end;

	std::vector<std::pair<double, double>> parsedPairs;
	std::pair<double, double> p;
	
	for (auto it = start; it != end; ++it)
	{
		p.first = std::stod(*it);
		++it;
		p.second = std::stod(*it);
		parsedPairs.push_back(p);
	}

	return parsedPairs;
}

std::string PalateFile::pairs2string(const std::vector<std::pair<double, double>>& pairs)
{
	std::string s;
	for (const auto& p : pairs)
	{
		s += std::to_string(p.first) + " " + std::to_string(p.second) + " ";
	}
	s.pop_back();  // Get rid of extra whitespace at the end of the string
	return s;
}

std::vector<double> PalateFile::string2values(const std::string& stringWithValues)
{
	std::istringstream iss(stringWithValues);
	std::istream_iterator<std::string> start(iss), end;
	std::vector<double> v;
	for (auto it = start; it!= end; ++it)
	{
		v.push_back(std::stod(*it));
	}
	return v;
}

std::string PalateFile::values2string(const std::vector<double>& values)
{
	std::string s;

	for (const auto& v : values)
	{
		s += std::to_string(v) + " ";
	}
	s.pop_back();  // Get rid of extra whitespace at the end of the string
	return s;
}



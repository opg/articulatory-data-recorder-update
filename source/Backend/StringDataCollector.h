#pragma once
#include <vector>
#include "DataCollector.h"
#include "StringDataParser.h"

class StringDataCollector : public DataCollector
{
public:

	StringDataCollector(wxEvtHandler* owner, FrameBuffer& buffer, const std::vector<Sensor>& sensors, StringDataParser* dataParser, const SerialPort::PortSettings& portSettings)
		: DataCollector(owner, buffer, sensors, dataParser, portSettings)
	{
	}

protected:
	bool collectData() override;
};


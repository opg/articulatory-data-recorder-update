#pragma once
#include <vector>
#include <string>

#include "StringDataCollector.h"
#include "OsloDataParser.h"


class OsloBackend :
    public StringDataCollector
{
public:
	OsloBackend(wxEvtHandler* owner, FrameBuffer& buffer)
	: StringDataCollector(owner, buffer, sensors, new OsloDataParser(sensors), portSettings)
	{
	}

	static const int samplingRate_Hz{ 100 };
	inline static const std::vector<Sensor> sensors
	{
		Sensor("S0", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S1", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S2", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S3", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S4", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S5", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S6", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S7", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("S8", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0))
	};
	inline static const SerialPort::PortSettings portSettings{ "COM3", 115200, SerialPort::NO_PARITY, SerialPort::ONE_STOP_BIT, false, 0, 0 };
};


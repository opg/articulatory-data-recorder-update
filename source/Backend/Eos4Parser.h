#pragma once
#include "DataParser.h"
class Eos4Parser :
    public DataParser
{
public:
	Eos4Parser(const std::vector<Sensor>& sensors, std::vector<int> contactMapping)
		: DataParser(sensors), contactIndexMap(contactMapping)
	{

	}

	Frame parse(const std::vector<std::byte>& dataToParse) override;

private:
	std::vector<int> contactIndexMap;
	
};


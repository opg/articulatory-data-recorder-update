#pragma once
#include <vector>


// A Frame contains one set of sensor data from a single time step.
class Frame
{
public:
	std::vector<double> scalarSensor;  // contains a number of scalar sensors, e.g., each element a sample from a distance sensor
	std::vector<std::vector<double>> matrixSensor;  // outer container contains a number of matrix sensors, e.g., each element a contact sensor matrix (linearly-indexed)
	std::vector<std::vector<int>> dimensions;  // Contains the dimensions of the matrix sensors to turn linear index to multiple index
	unsigned int mode{ 0 };
	unsigned int index{ 0 };
};
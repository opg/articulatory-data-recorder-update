#include "OsloDataParser.h"

#include <iostream>

OsloDataParser::OsloDataParser(const std::vector<Sensor>& sensors): StringDataParser(sensors, "%4d,%4d,%4d,%4d,%4d,%4d,%4d,%4d,%4d,\n\r")
{
}

Frame OsloDataParser::parse(const std::vector<std::byte>& dataToParse)
{
	Frame frame;
	auto data = std::vector<int>(sensors.size(), 0);
	const auto* characters = reinterpret_cast<const char*>(dataToParse.data());
	const auto tokensFound = sscanf(characters, formatString_.c_str(),
									&data[0], &data[1], &data[2],
	                                &data[3], &data[4], &data[5],
	                                &data[6], &data[7], &data[8]);
	for (const auto& d : data)
	{
		frame.scalarSensor.push_back(static_cast<double>(d));
	}
	if (tokensFound != sensors.size())
	{
		std::cout << "OsloDataParser::parse(): Incomplete data string!" << std::endl;
		return Frame();
	}
	return frame;
}

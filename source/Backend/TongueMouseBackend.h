#pragma once
#include "StringDataCollector.h"
#include "TongueMouseDataParser.h"

#include <vector>

class TongueMouseBackend : public StringDataCollector
{
public:

	TongueMouseBackend(wxEvtHandler* owner, FrameBuffer& buffer)
		: StringDataCollector(owner, buffer, sensors, new TongueMouseDataParser(sensors), portSettings)
	{
		hasAudio = false;
	}

	static const int samplingRate_Hz{ 500 };
	inline const static std::vector<Sensor> sensors
	{
		Sensor("Up", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Right", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Down", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Left", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Click", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Left click", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Right click", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0))
	};
	inline const static SerialPort::PortSettings portSettings{ "COM3", 115200, SerialPort::NO_PARITY, SerialPort::ONE_STOP_BIT, false, 0, 0 };
};
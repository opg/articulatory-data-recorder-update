#pragma once
#include <string>
#include <fstream>

#include "File.h"

class FileManager
{
public:
	static bool readFile(const std::string& filename, File& file, bool binaryMode = false)
	{
		std::ifstream inFile;
		if(binaryMode)
		{
			inFile.open(filename, std::ios::binary);
		}
		else
		{
			inFile.open(filename);
		}

		if(inFile.is_open())
		{
			inFile >> file;
			inFile.close();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	static bool writeFile(File& file, const std::string& filename, bool binaryMode = false)
	{
		std::ofstream outFile;
		if (binaryMode)
		{
			outFile.open(filename, std::ios::binary);
		}
		else
		{
			outFile.open(filename);  // Opens in text mode by default
		}
		
		if(outFile.is_open())
		{
			outFile << file;
			outFile.close();
			return true;
		}
		else
		{
			return false;
		}	
	}
};





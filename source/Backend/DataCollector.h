#pragma once

#include <vector>
#include <wx/wx.h>

#include "DataParser.h"
#include "FrameBuffer.h"
#include "SerialPort.h"
#include "Synchronizer.h"

// This event is fired every time new data was collected. It can be caught by a parent (GUI window) to update the data display appropriately.
wxDECLARE_EVENT(DataCollectedEvent, wxCommandEvent);

// Abstract base class for all specialized DataCollectors (e.g., EosDataCollector, SecondVoiceDataCollector etc)
// A data collector listens to a serial port for incoming data, parses it, and stores it in a buffer.
// The class is derived from wxTimer so it can check for new data periodically at a given interval.
class DataCollector : public wxTimer
{
public:
	/// @brief The method used to synchronize audio (if supported) and sensor data
	enum class SyncMethod
	{
		NO_SYNC,
		BEEP_SYNC,
		BEEP_BEEP_SYNC
	};

public:
	/// @brief  Creates a DataCollector with no parent.
	/// @param buffer 
	/// @param sensors 
	/// @param dataParser 
	/// @param serialPortSettings 
	explicit DataCollector(FrameBuffer& buffer, const std::vector<Sensor>& sensors, 
		DataParser* dataParser, const SerialPort::PortSettings& serialPortSettings);

	/// @brief Creates a DataCollector and connects it to an event handler that may process a DataCollectedEvent.
	/// @param owner 
	/// @param buffer 
	/// @param sensors 
	/// @param dataParser 
	/// @param serialPortSettings 
	explicit DataCollector(wxEvtHandler* owner, FrameBuffer& buffer, const std::vector<Sensor>& sensors,
		DataParser* dataParser, const SerialPort::PortSettings& serialPortSettings);
	/// @brief Destructor.
	~DataCollector() override { reset(); }
	bool run(const bool& start = true);
	void reset();
	bool setSerialPortSettings(const SerialPort::PortSettings& settings);
	const SerialPort::PortSettings& getSerialPortSettings() const;

	/// @brief Send a command to the hardware backend
	/// @param command A string containing the command (ideally a SCPI string)
	/// @return True if the command was successfully sent, false if something went wrong (or the data collector does not support two-way communication).
	bool sendCommand(const std::string& command);
	
public:
	// Parser object to parse the extracted byte-wise frame
	std::unique_ptr<DataParser> parser;
	const std::vector<Sensor>& sensors_;

	// Set this flag in the derived backend to "false" if audio shall not be recorded at the same time
	bool hasAudio{ true };

	// Set this flag to true if the derived backend supports two-way communication
	bool hasTwoWayCommunication_{ false };

	// Set the supported synchronization method in the derived class here
	SyncMethod syncMethod_{ SyncMethod::NO_SYNC };
	// Parameters needed by the synchronization method
	std::shared_ptr<SyncParameters> syncParams_{ nullptr };

protected:
	// The command that is sent to the hardware to start measurements (if two-way communication is supported)
	std::string startCommand_{ "SEQ:START\r\n" };
	// The command that is sent to the hardware to stop measurents (if two-way communication is supported)
	std::string stopCommand_{ "SEQ:STOP\r\n" };

	// How often is the serial port checked for new data?
	unsigned int refreshPeriod_ms_{20};
	// Serial port to collect data from
	SerialPort serialPort_;

	// Buffer to store the parsed frames (stores only a reference because some other class will own the buffer)
	FrameBuffer& buffer_;
	// Internal buffer that holds the unparsed, byte-wise data as they come in from the serial port
	static const int kMaxBufferBytes = 64000;
	unsigned char bytesBuffer_[kMaxBufferBytes]{0};
	std::vector<std::byte> byteFrame_;
	
	void Notify() override;
	virtual bool collectData() = 0;

private:
	void postDataCollectedEvent();
	
};
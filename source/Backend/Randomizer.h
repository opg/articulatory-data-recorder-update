#pragma once
#include <vector>
#include <random>

/// @brief Abstract base class for all specialized randomizer classes.
///
/// This class holds functionality to randomize index values.
class Randomizer
{
public:
	virtual ~Randomizer() = default;
	Randomizer() = default;
	
	/// @brief Constructs a randomizer with given values.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	/// @param numRepetitions number of repetitions of each index
	/// @param randomSeed random seed to be used
	Randomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed = std::default_random_engine::default_seed);

	/// @brief Returns randomized indices.
	/// @return randomized indices
	[[nodiscard]] std::vector<size_t> getRandomizedIndices() const;
	
	/// @brief Sets the index range.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	void setIndexRange(size_t minIndex, size_t maxIndex);

	/// @brief Sets the number of repetitions of each index
	/// @param numRepetitions number of repetitions
	void setNumRepetitions(int numRepetitions);

protected:
	int numRepetitions_{ 0 };
	std::default_random_engine rng_;
	std::uniform_int_distribution<size_t> uniformDistribution_;
	std::vector<size_t> randomizedIndices_;

	/// @brief Returns a random index.
	/// @return random index
	size_t getRandomIndex();

	/// @brief Pure virtual function to randomize indices.
	///
	/// Has to be overriden in all derived randomizer classes!
	virtual void randomizeIndices() = 0;	
};

/// @brief This class holds a randomizer which leads to all indices from minimum to maximum being repeated for numRepetitions times (non-randomized).
class CyclicNonRandomRandomizer : public Randomizer
{
public:
	/// @brief Constructs the cyclic non-random randomizer object.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	/// @param numRepetitions number of repetitions
	/// @param randomSeed random seed to be used
	CyclicNonRandomRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed = std::default_random_engine::default_seed);

	/// @brief Randomizes indices. Overrides randomizeIndices() from base class.
	void randomizeIndices() override;
};


/// @brief This class holds a randomizer which leads to randomized indices between minimum and maximum of size numRepetitions * (max-min) with replacement.
class RandomizerWithReplacement : public Randomizer
{
public:
	/// @brief Constructs the cyclic non-random randomizer object.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	/// @param numRepetitions number of repetitions
	/// @param randomSeed random seed to be used
	RandomizerWithReplacement(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed = std::default_random_engine::default_seed);

	/// @brief Randomizes indices. Overrides randomizeIndices() from base class.
	void randomizeIndices() override;
};

/// @brief This class holds a randomizer which leads to randomized indices between minimum and maximum of size numRepetitions * (max-min) without replacement.
class PermuteAllRandomizer : public Randomizer
{
public:
	/// @brief Constructs the cyclic non-random randomizer object.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	/// @param numRepetitions number of repetitions
	/// @param randomSeed random seed to be used
	PermuteAllRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed = std::default_random_engine::default_seed);

	/// @brief Randomizes indices. Overrides randomizeIndices() from base class.
	void randomizeIndices() override;
};

/// @brief This class holds a randomizer which leads to randomized indices between minimum and maximum of size numRepetitions * (max-min) without replacement and stratification across sets of repetitions.
class PermuteBalancedRandomizer : public Randomizer
{
public:
	/// @brief Constructs the cyclic non-random randomizer object.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	/// @param numRepetitions number of repetitions
	/// @param randomSeed random seed to be used
	PermuteBalancedRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed = std::default_random_engine::default_seed);

	/// @brief Randomizes indices. Overrides randomizeIndices() from base class.
	void randomizeIndices() override;
};

// randomized indices between minimum and maximum without replacement of size numRepetitions * (max - min) and stratification across sets of repetitions and no doublets at set boundaries

/// @brief This class holds a randomizer which leads to randomized indices between minimum and maximum of size numRepetitions * (max-min) without replacement and stratification across sets of repetitions, not holding
/// any doublets at set boundaries.
class PermuteBalancedNoDoubletsRandomizer : public Randomizer
{
public:
	/// @brief Constructs the cyclic non-random randomizer object.
	/// @param minIndex lower bound of indices
	/// @param maxIndex upper bound of indices
	/// @param numRepetitions number of repetitions
	/// @param randomSeed random seed to be used
	PermuteBalancedNoDoubletsRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed = std::default_random_engine::default_seed);

	/// @brief Randomizes indices. Overrides randomizeIndices() from base class.
	void randomizeIndices() override;
};

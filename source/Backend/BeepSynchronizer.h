﻿#pragma once

#include <dsp.h>

/// @brief Parameters required for beep synchronization
struct BeepSyncParameters : SyncParameters
{
	BeepSyncParameters() = default;
	BeepSyncParameters(double beepFrequency_Hz, double beepLength_s) : beepFrequency_Hz_(beepFrequency_Hz), beepLength_s_(beepLength_s) {}
	virtual ~BeepSyncParameters() override = default;

	/// @brief Frequency of the beep in Hertz
	double beepFrequency_Hz_{ 1000 };
	/// @brief Length of the beep in seconds
	double beepLength_s_{ 0.01 };
	/// @brief Length of the signal to search for beep (from beginning of signal)
	double window_s_{ 0.4 };
};

/// @brief Implements the Beep Synchronization method
/// @tparam T Data type of the signal to synchronize
template <typename T>
class BeepSynchronizer : public Synchronizer<T>
{
public:
	/// @brief Uses the same constructors as the base class
	using Synchronizer<T>::Synchronizer;

	/// @brief Return the frequency of the beep in Hertz
	double getBeepFrequency_Hz() { return dynamic_cast<BeepSyncParameters*>(this->params_.get())->beepFrequency_Hz_; }
	/// @brief Return the length of the beep in seconds
	double getBeepLength_s(){ return dynamic_cast<BeepSyncParameters*>(this->params_.get())->beepLength_s_; }
	/// @brief Calculate and return the index of the sample that will be the new first sample after synchronizing 
	int getSyncSample() override;
	/// @brief Perform the synchronization. If getSyncSample() was not called before calling this, it is called internally.
	void sync() override;

private:
	/// @brief Index of the sample that will be the new first sample after synchronization
	int syncSample_{ -1 };
};

template <typename T>
int BeepSynchronizer<T>::getSyncSample()
{
	const auto* params = dynamic_cast<BeepSyncParameters*>(this->params_.get());
	auto beep = dsp::signals::sin<T>(params->beepFrequency_Hz_, params->beepLength_s_, this->signal_.getSamplingRate_Hz());
	const auto n_xcorr = this->signal_.getSamplingRate_Hz() * params->window_s_;
	const auto xcorr = dsp::correlate<T>({ this->signal_.begin(), this->signal_.begin() + n_xcorr }, beep.getSamples(), dsp::correlation_mode::valid);
	auto maxCorr = std::max_element(xcorr.begin(), xcorr.end());
	syncSample_ = std::distance(xcorr.begin(), maxCorr);
		
	return syncSample_;
}

template <typename T>
void BeepSynchronizer<T>::sync()
{
	if(syncSample_ == -1)
	{
		getSyncSample();
	}

	this->signal_.erase(this->signal_.begin(), this->signal_.begin() + syncSample_);
}

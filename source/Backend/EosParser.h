#pragma once
#include "DataParser.h"

class EosParser :
    public DataParser
{
public:
	EosParser(const std::vector<Sensor>& sensors)
		: DataParser(sensors)
	{
		
	}

	Frame parse(const std::vector<std::byte>& dataToParse) override;
};


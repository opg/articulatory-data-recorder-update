#pragma once
#include <string>
#include <cstddef>
#include <vector>
#include "DataParser.h"

class StringDataParser :
    public DataParser
{
public:
	StringDataParser(const std::vector<Sensor>& sensors, const std::string& formatString)
		: DataParser(sensors), formatString_(formatString)
	{
	}
	std::string formatString_;
	Frame parse(const std::vector<std::byte>& dataToParse) override = 0;
};


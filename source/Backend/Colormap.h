#pragma once
#include <map>

#include <wx/colour.h>


class ColorMap
{
	// https://www.kennethmoreland.com/color-advice/
public:
	ColorMap(bool inverted = false);
	ColorMap(const std::map<double, wxColor> newTable);
	virtual ~ColorMap() = 0;  // Necessary to make class abstract. No need to override in derived class because of default implementation
	wxColor getColor(double value) const;
	[[nodiscard]] int getNumberOfColors() const;

	enum class ColorMapType {
		SMOOTH_COOL_WARM,
		GRAY,
		BLACK_BODY,
	};

	inline static std::map<ColorMapType, std::string> colorMapType2string =
	{
		{ColorMapType::SMOOTH_COOL_WARM, "Smooth"},
		{ColorMapType::GRAY, "Gray"},
		{ColorMapType::BLACK_BODY, "Black"},
	};

	inline static std::map<std::string, ColorMapType> string2colorMapType =
	{
		{"Smooth", ColorMapType::SMOOTH_COOL_WARM},
		{"Gray", ColorMapType::GRAY},
		{"Black", ColorMapType::BLACK_BODY},
	};

	ColorMapType getColorMapType() const;

protected:
	void setColorTable(const std::map<double, wxColor> newTable);
	void setColorMapType(ColorMapType newColorMapType);

private:
	int numColors_{ 256 };  // Using 256 colors (which is the same as Python's matplotlib and others)
	std::map<double, wxColor> colorTable_;
	bool inverted_{ false };  // Invert the color mapping
	ColorMapType colorMapType_{ ColorMapType::SMOOTH_COOL_WARM };
};


class GrayColorMap : public ColorMap
{
public:
	GrayColorMap(bool inverted = false);
};

class SmoothCoolWarmColorMap : public ColorMap
{
public:
	SmoothCoolWarmColorMap(bool inverted = true);
};

class BlackBodyColorMap : public ColorMap
{
public:
	BlackBodyColorMap(bool inverted = false);
};





#pragma once
#include <windows.h>

// ****************************************************************************
// This class represents a serial COM port
// ****************************************************************************

class SerialPort
{
  // **************************************************************************
  // Public data.
  // **************************************************************************

public:
  enum Parity
  {
    NO_PARITY,
    ODD_PARITY,
    EVEN_PARITY
  };

  enum StopBits
  {
    ONE_STOP_BIT,
    TWO_STOP_BITS
  };
	/// \brief This struct represents the settings of the EOS COM port.
  struct PortSettings 
  {
    char portName[256]{"COM1"};
    int baudRate{ 115200 };
    Parity parity{ NO_PARITY };
    StopBits stopBits{ ONE_STOP_BIT };
    bool hardwareFlowControl{ false };
    unsigned long timeout_s{ 0 };
    unsigned long timeout_ms{ 0 };
};

  PortSettings settings{ "COM2",115200, NO_PARITY, ONE_STOP_BIT,
      false, 0, 0 };
 

  // **************************************************************************
  // **************************************************************************

public:
  SerialPort();
  SerialPort(PortSettings settings);
  SerialPort(const char* portName, int baudRate, Parity parity = NO_PARITY,
      StopBits stopBits = ONE_STOP_BIT, bool hardwareFlowControl = false);
  ~SerialPort();

  bool open();
  void close();
  bool isOpen();
  const PortSettings& getSettings() const;

  int bytesAvailable();
  bool flushBuffer() const;
  int readData(char *data, int numBytes);
  int writeData(const char *data, int numBytes);

  // **************************************************************************
  // Private data.
  // **************************************************************************

private:
    
  // Windows specific variables
  HANDLE handle{ INVALID_HANDLE_VALUE };
  COMMCONFIG commConfig;
  COMMTIMEOUTS commTimeouts;

};

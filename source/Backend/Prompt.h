#pragma once
#include <vector>

/// @brief This templated class defines a prompt.
///
/// This class works as base for any experiment. Prompts will
/// be defined as a tuple of a prompt id and a prompt content. This
/// content can be anything (string, audio file, image, etc.).
/// @tparam PromptType type of the defined prompt
template <class PromptType> class Prompt
{
public:
	/// @brief Constructs a prompt object.
	/// @param id id of the constructed prompt object
	/// @param content content of the constructed prompt object
	Prompt(int id, PromptType content);

	/// @brief Returns the id.
	/// @return id
	[[nodiscard]] int getId() const;

	/// @brief Returns the content.
	/// @return content
	PromptType getContent() const;	

	/// @brief Sets a new id to the prompt object.
	/// @param newId new id
	void setId(int newId);

	/// @brief Sets a new content to the prompt object.
	/// @param newContent new content
	void setContent(PromptType newContent);

	/// @brief Operator to compare the content of different prompts.
	/// @param promptToCompareWith prompt to which current prompt should be compared
	/// @return content is the same
	bool operator==(const Prompt& promptToCompareWith) const { return content_ == promptToCompareWith.content_; }

private:
	int id_{ -1 };
	PromptType content_;
};

template <class PromptType>
Prompt<PromptType>::Prompt(int id, PromptType content) : id_(id), content_(content)
{
}

template <class PromptType>
int Prompt<PromptType>::getId() const
{
	return id_;
}

template <class PromptType>
PromptType Prompt<PromptType>::getContent() const
{
	return content_;
}

template <class PromptType>
void Prompt<PromptType>::setId(int newId)
{
	id_ = newId;
}

template <class PromptType>
void Prompt<PromptType>::setContent(PromptType newContent)
{
	content_ = newContent;
}


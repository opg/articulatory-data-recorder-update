#pragma once

#include <complex>
#include <vector>
#include <map>

#include "Dsp.h"

class Spectrogram
{
	// Usage example:
	// auto s = Spectrogram(5, 0.5, 44100);
	// s.transform(audioSignal).get()

public:
	Spectrogram() = default;
	Spectrogram(int frameLength_ms, double overlap_ms, int samplingRate = -1, double relativeCutoff = 0.5, dsp::window::type windowType = dsp::window::type::hamming, double preEmphasisCoeff = 0.95);

	struct Settings
	{
		double preEmphasisCoeff_{ 0.95 };  // Filter coefficient alpha: y[n] = y[n] - alpha * y[n-1]
		int frameLength_{ 128 };  // In samples
		int overlap_{ 64 };  // In samples
		int frameStride_{ 64 };	// In samples
		int samplingRate_Hz_{ -1 };  // If sampling rate is -1, use normalized frequencies (0, 2pi)
		double relativeCutoff_{ 0.5 };  // How much of each spectrum to keep (relative to the sampling frequency)
		dsp::window::type windowType_{ dsp::window::type::hamming };
		int nFft_{ static_cast<int>(pow(2, dsp::nextpow2(frameLength_))) };
		Settings() = default;
		Settings(int frameLength_ms, double overlap_pct, const int samplingRate = -1, double relativeCutoff = 0.5, dsp::window::type windowType = dsp::window::type::hamming, double preEmphasisCoeff = 0.95) : frameLength_(frameLength_ms), overlap_(static_cast<int>(overlap_pct* frameLength_)), frameStride_(frameLength_ - overlap_), samplingRate_Hz_(samplingRate), relativeCutoff_(relativeCutoff), windowType_(windowType), preEmphasisCoeff_(preEmphasisCoeff)
		{
			frameLength_ = static_cast<int>(static_cast<double>(frameLength_ms) / 1000.0 * static_cast<double>(samplingRate_Hz_));
			overlap_ = static_cast<int>(overlap_pct * static_cast<double>(frameLength_));
			frameStride_ = frameLength_ - overlap_;
			nFft_ = static_cast<int>(pow(2, dsp::nextpow2(frameLength_)));
		}
	};

	Spectrogram(const Settings settings);
	Spectrogram& transform(const std::vector<double>& signal);

	[[nodiscard]] int getNfft() const;
	[[nodiscard]] int getSamplingRate_Hz() const;
	[[nodiscard]] int getFrameLength(bool inSamples = false) const;
	[[nodiscard]] double getOverlap(bool inSamples = false) const;
	[[nodiscard]] static std::vector<dsp::window::type> getSupportedWindowTypes();
	[[nodiscard]] dsp::window::type getWindowType() const;
	[[nodiscard]] double getMaxLevel() const;
	[[nodiscard]] Settings getSettings() const;
	[[nodiscard]] double getCutoff() const;
	[[nodiscard]] double getPreEmphasisCoeff() const;

	[[nodiscard]] std::vector<std::vector<double>> get() const;

	void setNfft(int newNfft);
	void setOverlap(int newOverlap);
	void setSamplingRate_Hz(int samplingRate_Hz);
	void setFrameLength(int frameLength);
	void setCutoff(double newCutoff);
	void setWindowType(dsp::window::type newWindowType);
	void setPreEmphasisCoeff(double newCoeff);
	void setSettings(Settings newSettings);


private:
	std::vector<std::vector<double>> spectrogram_;
	std::vector<double> window_;
	Settings settings_;
};


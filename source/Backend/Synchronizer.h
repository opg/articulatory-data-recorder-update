#pragma once

/// @brief Available methods for synchronization.
enum class SyncMethod
{
	NO_SYNC,
	BEEP_SYNC
};

/// @brief Base struct for parameters used for the synchronization. 
struct SyncParameters
{
	SyncParameters() = default;
	virtual ~SyncParameters() = default;
};

/// @brief A base class for all implementations of synchronizer methods.
/// @tparam T The data type of the signal to synchronize.
template <class T>
class Synchronizer
{
public:
	/// @brief A default constructor is not available because the reference to the signal to be synchronized has to be initialized at object construction time
	Synchronizer() = delete;
	/// @brief Constructor for a base synchronizer.
	/// @param signal A reference to the signal to be synchronized.
	/// @param params A pointer to an object derived from SyncParameters containing the parameters required for syncing.
	Synchronizer(Signal<T>& signal, std::shared_ptr<SyncParameters> params = nullptr) : signal_(signal), params_(params){}
	virtual ~Synchronizer() = default;

	/// @brief Calculates and returns the sample index of the sample that will be the new first sample after synchronization
	virtual int getSyncSample() { return 0; }
	/// @brief Perform the synchronization. If getSyncSample() was not called before this, it is called internally if necessary.
	virtual void sync(){}


protected:
	/// @brief A reference to the signal to synchronize
	Signal<T>& signal_;
	/// @brief A pointer to an object derived from SyncParameters.
	/// In classes derived from Synchronizer, this pointer needs to be dynamically cast to the respective struct derived from SyncParameters.
	std::shared_ptr<SyncParameters> params_;
};
#pragma once

#include <string>
#include <vector>

struct Palate
{
	struct LightSensor
	{
		int index;
		double x_cm;
		double y_cm;
		double angle_deg;
		std::vector<std::pair<double, double>> calibration_dc_mm_adc;
		std::vector<double> coefficients_triplePT;	
	};
	
	Palate() = default;

	std::string name;
	std::vector<std::pair<double, double>> shape_2d;
	unsigned int numContactSensors;
	unsigned int numDistanceSensors;
	std::vector<LightSensor> distanceSensors;
};


#include "ExperimentManager.h"
#include <string>

template <class PromptContentType>
ExperimentManager<PromptContentType>::~ExperimentManager()
{
	delete randomizer_;
}

template <class PromptContentType>
ExperimentManager<PromptContentType>::ExperimentManager(const std::vector<PromptContentType>& inventory,
	const int repetitions, const RandomizationStrategy randStrategy) : ExperimentManager(inventory, Settings(repetitions, randStrategy))
{	
}

template <class PromptContentType>
ExperimentManager<PromptContentType>::ExperimentManager(const std::vector<PromptContentType>& inventory, const Settings& settings)
	: ExperimentManager(Experiment<PromptContentType>(inventory, settings))
{
}

template <class PromptContentType>
ExperimentManager<PromptContentType>::ExperimentManager(const Experiment<PromptContentType>& experiment) 
{
	setExperiment(experiment);
}

template <class PromptContentType>
ExperimentManager<PromptContentType>& ExperimentManager<PromptContentType>::next()
{
	if(!experimentHasStarted)
	{
		experimentHasStarted = true;
	}
	else if(currentPrompt < promptsQueue_.end() - 1)
	{
		++currentPrompt;
	}
	else
	{
		experimentHasFinished = true;
	}
	return *this;
}

template <class PromptContentType>
ExperimentManager<PromptContentType>& ExperimentManager<PromptContentType>::previous()
{
	if (experimentHasFinished)
	{
		experimentHasFinished = false;
	}
	else if(currentPrompt > promptsQueue_.begin())
	{
		--currentPrompt;
	}
	else
	{
		experimentHasStarted = false;
	}
	return *this;
}

template <class PromptContentType>
void ExperimentManager<PromptContentType>::generateQueue()
{
	this->reset();

	const auto randomizedIndices = randomizer_->getRandomizedIndices();
	for (const auto& index : randomizedIndices)
	{
		promptsQueue_.emplace_back(static_cast<int>(index), experiment_.inventory[index]);
	}

	currentPrompt = promptsQueue_.begin();
}

template <class PromptContentType>
void ExperimentManager<PromptContentType>::reset()
{
	promptsQueue_.clear();
	experimentHasStarted = false;
	experimentHasFinished = false;
}

template <class PromptContentType>
const Settings& ExperimentManager<PromptContentType>::getSettings()
{
	return experiment_.settings;
}

template <class PromptContentType>
const std::vector<PromptContentType>& ExperimentManager<PromptContentType>::getInventory()
{
	return experiment_.inventory;
}

template <class PromptContentType>
const std::vector<Prompt<PromptContentType>>& ExperimentManager<PromptContentType>::getQueue()
{
	return promptsQueue_;
}

template<class PromptContentType>
const Experiment<PromptContentType>& ExperimentManager<PromptContentType>::getExperiment()
{
	return experiment_;
}

template<class PromptContentType>
void ExperimentManager<PromptContentType>::setExperiment(const Experiment<PromptContentType>& newExperiment)
{
	experiment_ = newExperiment;

	switch (experiment_.settings.randStrategy.type)
	{
	case RandomizationStrategy::Type::CYCLIC_NON_RANDOM:
		randomizer_ = new CyclicNonRandomRandomizer(0, experiment_.inventory.size() - 1, experiment_.settings.repetitions);
		break;
	case RandomizationStrategy::Type::WITH_REPLACEMENT:
		randomizer_ = new RandomizerWithReplacement(0, experiment_.inventory.size() - 1, experiment_.settings.repetitions);
		break;
	case RandomizationStrategy::Type::PERMUTE_ALL:
		randomizer_ = new PermuteAllRandomizer(0, experiment_.inventory.size() - 1, experiment_.settings.repetitions);
		break;
	case RandomizationStrategy::Type::PERMUTE_BALANCED:
		randomizer_ = new PermuteBalancedRandomizer(0, experiment_.inventory.size() - 1, experiment_.settings.repetitions);
		break;
	case RandomizationStrategy::Type::PERMUTE_BALANCED_NO_DOUBLETS:
		randomizer_ = new PermuteBalancedNoDoubletsRandomizer(0, experiment_.inventory.size() - 1, experiment_.settings.repetitions);
		break;
	default:
		break;
	}
	generateQueue();
}

template <class PromptContentType>
void ExperimentManager<PromptContentType>::setInventory(const std::vector<PromptContentType>& newInventory)
{
	Experiment<PromptContentType> newExperiment = getExperiment();
	newExperiment.inventory = newInventory;
	setExperiment(newExperiment);
}

template <class PromptContentType>
void ExperimentManager<PromptContentType>::setSettings(const Settings& newSettings)
{
	Experiment<PromptContentType> newExperiment = getExperiment();
	newExperiment.settings = newSettings;
	setExperiment(newExperiment);
}

// Necessary to avoid linker error when only including the ExperimentManager.h
template class ExperimentManager<std::string>;
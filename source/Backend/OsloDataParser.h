#pragma once
#include <cstddef>
#include <vector>

#include "Frame.h"
#include "StringDataParser.h"

class OsloDataParser :
    public StringDataParser
{

public:
	OsloDataParser(const std::vector<Sensor>& sensors);
	Frame parse(const std::vector<std::byte>& dataToParse) override;
};


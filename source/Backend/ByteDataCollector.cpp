#include "ByteDataCollector.h"

bool ByteDataCollector::collectData()
{

	enum State { WAIT_START_BYTE_0, WAIT_START_BYTE_1, WAIT_DATA_BYTE };
	static State state = WAIT_START_BYTE_0;

	const int numNewBytes = serialPort_.readData(reinterpret_cast<char*>(bytesBuffer_), serialPort_.bytesAvailable());

	if (numNewBytes >= kMaxBufferBytes)
	{
		// Buffer overflow
		wxLogWarning("Buffer overflow!");
		state = WAIT_START_BYTE_0;
		return false;
	}

	for (int byteIdx = 0; byteIdx < numNewBytes; ++byteIdx)
	{
		std::byte byte = static_cast<std::byte>(bytesBuffer_[byteIdx]);
		switch (state)
		{
		case WAIT_START_BYTE_0:
			if (byte == headerBytes_.first)
			{
				state = WAIT_START_BYTE_1;
			}
			break;

			// ************************************************************

		case WAIT_START_BYTE_1:
			if (byte == headerBytes_.second)
			{
				state = WAIT_DATA_BYTE;
				byteFrame_.clear();
			}
			else
				if (byte != headerBytes_.second)
				{
					state = WAIT_START_BYTE_0;
				}
			break;

			// ************************************************************

		case WAIT_DATA_BYTE:
			byteFrame_.push_back(byte);
			if (byteFrame_.size() >= kFrameLength)
			{
				int checkSum = 0;

				for (int i = 0; i < kFrameLength - 1; i++)  // Do not include the checksum byte in checksum!
				{
					checkSum += std::to_integer<int>(byteFrame_[i]);
				}

				if (static_cast<std::byte>(checkSum) == byteFrame_.back())
				{
					buffer_.addFrame(parser->parse(byteFrame_));
					if (buffer_.size() > buffer_.maxNumFrames() - 1)
					{
						throw std::overflow_error("Frame buffer full!");
					}
				}
				else
				{
					wxLogWarning("Mismatch of checksum for sensor data frame!\n");
				}
				state = WAIT_START_BYTE_0;
			}
			break;
		}
	}

	return true;
}

#pragma once
#include <vector>
#include "Calibrator.h"

class CalibrationPredictor
{
public:
	CalibrationPredictor() = default;
	CalibrationPredictor(const std::vector<std::vector<double>>& predictorCoefficients);
	CalibrationPredictor(const std::vector<std::vector<double>>& predictorCoefficients, const std::vector<double>& distances_mm);
	
	std::vector<Calibrator<double, double>::calibrationPoint> predictCalibrationValues(double zeroDistance_adc);

	void setPredictorCoefficients(const std::vector<std::vector<double>>& predictorCoefficients);
	void setCalibrationDistances(const std::vector<double>& distances_mm);

private:
	static double predictAdcValue(const std::vector<double>& a, double x0);
	
	std::vector<std::vector<double>> predictorCoefficients_{ 3, {1, 0, 0} };
	std::vector<double> calibrationDistances_mm_{ 0, 5, 10, 15, 20, 25, 30 };
};


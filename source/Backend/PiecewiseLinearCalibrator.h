#pragma once
#include <algorithm>
#include <vector>
#include <stdexcept>

#include "Calibrator.h"

// A piece-wise linear calibrator determines the calibrated value by linearly interpolating
// between known points in the calibration characteristic
template<class rawType, class calibratedType>
class PiecewiseLinearCalibrator : public Calibrator<rawType, calibratedType>
{
public:
	PiecewiseLinearCalibrator(const std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint>& knownPoints);
	calibratedType getCalibratedValue(rawType x) override;


	[[nodiscard]] const std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint>& getKnownPoints() const;

	void setCalibrationPoints(const std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint>& newKnownPoints) override;
	
private:
	calibratedType getInterpolatedValue(const rawType& x, const typename Calibrator<rawType, calibratedType>::calibrationPoint& lowerBound, const typename Calibrator<rawType, calibratedType>::calibrationPoint& upperBound);
	void sortKnownPoints();
	std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint> knownPoints_;

};

//*****************************************************************************
//*****************************************************************************

template <class rawType, class calibratedType>
PiecewiseLinearCalibrator<rawType, calibratedType>::PiecewiseLinearCalibrator(
	const std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint>& knownPoints) : knownPoints_(knownPoints)
{
	// The known points must be sorted in ascending order by their raw values
	sortKnownPoints();
}


template <class rawType, class calibratedType>
calibratedType PiecewiseLinearCalibrator<rawType, calibratedType>::getCalibratedValue(rawType x)
{
	if (knownPoints_.empty())
	{
		return 0.0;
	}

	if (knownPoints_.size() == 1)
	{
		return knownPoints_[0].calibratedValue;
	}

	// Find the first known point whose raw value is *not less* than x
	auto nextClosest = std::lower_bound(knownPoints_.begin(), knownPoints_.end(), x,
		[](const auto& a, const auto& b) { return a.rawValue < b; });

	if (nextClosest == knownPoints_.end())  // i.e., all known raw values are smaller than x
	{
		return knownPoints_.back().calibratedValue;
	}

	if (nextClosest == knownPoints_.begin())  // i.e., x is smaller than all known raw values
	{
		return knownPoints_.front().calibratedValue;
	}

	// Otherwise x is in an interval between two known points, so interpolate to get the corresponding calibrated value
	return getInterpolatedValue(x, *(nextClosest - 1), *nextClosest);
}

template <class rawType, class calibratedType>
const std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint>&
PiecewiseLinearCalibrator<rawType, calibratedType>::getKnownPoints() const
{
	return knownPoints_;
}

template <class rawType, class calibratedType>
void PiecewiseLinearCalibrator<rawType, calibratedType>::setCalibrationPoints(const std::vector<typename Calibrator<rawType, calibratedType>::calibrationPoint>& newKnownPoints)
{
	knownPoints_ = newKnownPoints;
	sortKnownPoints();
}

template <class rawType, class calibratedType>
calibratedType PiecewiseLinearCalibrator<rawType, calibratedType>::getInterpolatedValue(const rawType& x,
	const typename Calibrator<rawType, calibratedType>::calibrationPoint& lowerBound, const typename Calibrator<rawType, calibratedType>::calibrationPoint& upperBound)
{
	const auto deltaRaw = upperBound.rawValue - lowerBound.rawValue;
	if (deltaRaw < 0)
	{
		throw std::logic_error("Passed upper bound is smaller than lower bound! Did you mix up the order of arguments?");
	}
	const auto deltaCalibrated = upperBound.calibratedValue - lowerBound.calibratedValue;
	auto t = static_cast<calibratedType>(x - lowerBound.rawValue) / deltaRaw;
	auto y = lowerBound.calibratedValue + t * deltaCalibrated;

	return y;
}

template <class rawType, class calibratedType>
void PiecewiseLinearCalibrator<rawType, calibratedType>::sortKnownPoints()
{
	std::sort(knownPoints_.begin(), knownPoints_.end(), [](const auto& a, const auto& b) { return a.rawValue < b.rawValue; });
}


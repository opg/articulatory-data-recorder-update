#pragma once
#include "StringDataParser.h"

#include <iostream>


class TongueMouseDataParser :
    public StringDataParser
{
public:
	TongueMouseDataParser(const std::vector<Sensor>& sensors)
		: StringDataParser(sensors, "%d\t%d\t%d\t%d\t%d\t%d\t%d\r\n")
	{
	}


	Frame parse(const std::vector<std::byte>& dataToParse) override;
};

inline Frame TongueMouseDataParser::parse(const std::vector<std::byte>& dataToParse)
{
	Frame frame;
	auto data = std::vector<int>(sensors.size(), 0);
	const auto* characters = reinterpret_cast<const char*>(dataToParse.data());
	const auto tokensFound = sscanf(characters, formatString_.c_str(),
		&data[0], &data[1], &data[2],
		&data[3], &data[4], &data[5],
		&data[6]);
	for (const auto& d : data)
	{
		frame.scalarSensor.push_back(static_cast<double>(d));
	}
	if (tokensFound != sensors.size())
	{
		std::cout << "TongueMouseDataParser::parse(): Incomplete data string!" << std::endl;
		return Frame();
	}
	return frame;
}


#pragma once
#include "DataCollector.h"


class ByteDataCollector :
	public DataCollector
{
public:
	ByteDataCollector(wxEvtHandler* owner, FrameBuffer& buffer, const std::vector<Sensor>& sensors, DataParser* dataParser, 
		std::pair<std::byte, std::byte> headerBytes, int frameLength, const SerialPort::PortSettings& serialPortSettings)
		: DataCollector(owner, buffer, sensors, dataParser, serialPortSettings), headerBytes_(headerBytes), kFrameLength(frameLength)
	{
	}

protected:
	bool collectData() override;

private:
	std::pair<std::byte, std::byte> headerBytes_;
	const int kFrameLength;  // Frame length in Byte without header bytes but including the checksum byte
};


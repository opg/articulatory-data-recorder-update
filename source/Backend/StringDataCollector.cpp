#include "StringDataCollector.h"
#include <cstddef>

bool StringDataCollector::collectData()
{
	volatile const int numNewBytes = serialPort_.readData(reinterpret_cast<char*>(bytesBuffer_), serialPort_.bytesAvailable());
	if (numNewBytes >= kMaxBufferBytes)
	{
		// Buffer overflow
		return false;
	}
	
	for (int byteIdx = 0; byteIdx < numNewBytes; ++byteIdx)
	{
		unsigned char ch = bytesBuffer_[byteIdx];
		byteFrame_.push_back(static_cast<std::byte>(ch));
		if (ch == '\r')
		{
			Frame frame = parser->parse(byteFrame_);
			byteFrame_.clear();
			if (!frame.scalarSensor.empty())
			{
				buffer_.addFrame(frame);
			}
		}
		if (buffer_.size() > buffer_.maxNumFrames() - 1)
		{
			throw std::overflow_error("Frame buffer full!");
		}
	}
	return true;
}

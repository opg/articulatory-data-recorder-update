#pragma once

// Abstract base class of signal calibrators
template<class rawType, class calibratedType>
class Calibrator
{
public:
	struct calibrationPoint
	{
		rawType rawValue;
		calibratedType calibratedValue;
		calibrationPoint(rawType r, calibratedType c) { rawValue = r; calibratedValue = c; }
	};
	
	Calibrator() = default;
	virtual ~Calibrator() = default;
	virtual calibratedType getCalibratedValue(rawType x) = 0;
	virtual void setCalibrationPoints(const std::vector<calibrationPoint>& newKnownPoints) = 0;
};





#pragma once
#include <vector>

#include "DataParser.h"
#include "Frame.h" 
#include "Sensor.h"

class OpgDataParser :
	public DataParser
{
public:
	OpgDataParser(const std::vector<Sensor>& sensors) :
		DataParser(sensors)
	{

	};

	// Public variables.
	const double ADC1_MAX_VALUE_LSB = 4095.0;
	const double CURRENT_SENSE_RESISTOR_OHM = 1.0;
	const double CURRENT_SENSE_GAIN = 100.0; // INA183-A2
	const double ADC1_REF_VOLTAGE_V = 3.3; // STM32F103 ADC1 internal reference voltage.
	const double ADC_MODULE_MAX_VALUE_LSB = 16383.0;
	const double ADC_MODULE_RANGE_POSITIVE_V = 12.288; // 3*4.096 V.

	// Public functions.
	Frame parse(const std::vector<std::byte>& dataToParse) override;

};


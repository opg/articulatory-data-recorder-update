#pragma once
#include <string>
#include <vector>

#include <Dsp.h>

#include "Sensor.h"

template<class T>
class Signal : public dsp::Signal<T>
{
public:
	Signal() = default;
	virtual ~Signal() = default;
	Signal(const std::string& name, const std::string& unitX, const std::string& unitY, int samplingRate,
		const std::pair<double, double>& range, const std::vector<int>& dimensions, int reservedSize = 360000)
		:
		dsp::Signal<T>(samplingRate),
		name(name),
		unit_x(unitX),
		unit_y(unitY),
		range(range),
		dimensions(dimensions),
		reservedSize_(reservedSize)
	{
		this->reserve(reservedSize_);
	}
	Signal(const Sensor& sensor)
		:
		dsp::Signal<T>(sensor.samplingRate),
		name(sensor.name),
		unit_x(sensor.unit_x),
		unit_y(sensor.unit_y),
		range(sensor.valueRange),
		dimensions(sensor.inputDimensions)
	{
		this->reserve(reservedSize_);
	}
	std::string name{ "s" };
	std::string unit_x{ "" };  // Quantity of the dimension of sampling (most likely time)
	std::string unit_y{ "" };  // Quantity that is measured
	std::pair<double, double> range{ 0, 1 };  // Upper and lower limit of the signal
	std::vector<int> dimensions;  // Dimensions of the signal (only relevant for higher-dimensional Signals like MatrixSignal)

private:
	int reservedSize_{ 360000 };  // Reserve size for the signal so its address does not change before that!
};

using MatrixSignal = Signal<std::vector<double>>;
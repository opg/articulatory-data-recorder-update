#include "AdrFile.h"

#include <algorithm>
#include <sstream>

AdrFile::AdrFile(double start, double end) : saveSubset_(true), startTime_(start), endTime_(end)
{	
}

void AdrFile::readFromStream(std::istream& is)
{
	//TODO
}

void AdrFile::writeToStream(std::ostream& os)
{
	// Meta information
	os << "ArticulatoryDataRecorder" << std::endl;
	os << "Hardware backend: " << std::endl;
	os << HardwareBackend::type2string[Data::getInstance().currentBackend] << std::endl;
	os << "Number of scalar signals:" << std::endl;
	os << Data::getInstance().frameBuffer.numScalarSignals() << std::endl;
	os << "Number of matrix signals:" << std::endl;
	os << Data::getInstance().frameBuffer.numMatrixSignals() << std::endl;
	os << "Dimensions of matrix sensors:" << std::endl;
	for (const auto& dims : Data::getInstance().frameBuffer.getMatrixDimensions())
	{
		for (const auto& dim : dims)
		{
			os << dim << " ";
		}
		os << std::endl;
	}
	os << "Sampling rate [Hz]:" << std::endl;
	const auto samplingRate = Data::getInstance().frameBuffer.getSamplingRate();
	os << samplingRate << std::endl;

	// Print table header
	os << createHeader() << std::endl;
	
	// Print frame buffer
	size_t firstFrame{ 0 };
	size_t lastFrame{ Data::getInstance().frameBuffer.size() - 1};
	if (saveSubset_)
	{
		firstFrame = startTime_ * samplingRate;
		lastFrame = std::min(static_cast<size_t>(endTime_ * samplingRate), Data::getInstance().frameBuffer.size() - 1);
	}
	for (size_t i = firstFrame; i <= lastFrame; ++i)
	{
		const auto& frame = Data::getInstance().frameBuffer.getFrame(i);

		os << frame.index << "\t"; 
		for (const auto& sensor : frame.scalarSensor)
		{
			os << sensor << "\t";
		}

		for (const auto& sensor : frame.matrixSensor)
		{
			for (const auto& value : sensor)
			{
				os << value << " ";
			}
			os << "\t";
		}
		os << std::endl;
	}	
}

std::string AdrFile::createHeader()
{
	std::ostringstream header;

	header << "Index" << "\t";

	for (const auto& signal : Data::getInstance().frameBuffer.getScalarSignals())
	{
		header << signal.name << "\t";
	}
	for (const auto& signal : Data::getInstance().frameBuffer.getMatrixSignals())
	{
		header << signal.name << "\t";
	}
	
	return header.str();
}

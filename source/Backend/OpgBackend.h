#pragma once
#include "ByteDataCollector.h"
#include "OpgDataParser.h"

#include <vector>

class OpgBackend :
	public ByteDataCollector
{
public:
	// Constructor.
	OpgBackend(wxEvtHandler* owner, FrameBuffer& buffer)
		: ByteDataCollector(owner, buffer, sensors, new OpgDataParser(sensors),
			std::make_pair(std::byte{ 0xAA }, std::byte{ 0x55 }), 37, portSettings) // 37 = number of transmitted bytes (incl. CS) minus first two start bytes
	{
		hasAudio = true;
	}

	static const int samplingRate_Hz{ 100 };
	inline static const std::vector<Sensor> sensors
	{
		Sensor("Current", "s", "mA", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 15.0)),
		Sensor("S11", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S12", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S13", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S14", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S21", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S22", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S23", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S24", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S31", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S32", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S33", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S34", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S41", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S42", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
		Sensor("S43", "s", "Counts", std::vector<int>{1}, samplingRate_Hz, std::make_pair(-500.0, 16383.0)),
	};

	inline static const SerialPort::PortSettings portSettings{ "COM8", 115200, SerialPort::NO_PARITY, SerialPort::ONE_STOP_BIT, false, 0, 0 };
};
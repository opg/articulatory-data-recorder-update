#include "EosParser.h"

Frame EosParser::parse(const std::vector<std::byte>& dataToParse)
{
	Frame frame;
	int bytePos = 0;
	frame.mode = std::to_integer<unsigned int>(dataToParse[bytePos++]);
	frame.index = std::to_integer<unsigned int>(dataToParse[bytePos++])
	+ (std::to_integer<unsigned int>(dataToParse[bytePos++]) << 8);

	frame.scalarSensor.resize(numScalarSensors);
	frame.matrixSensor.resize(numMatrixSensors);

	for (int i = 0; i < numScalarSensors; ++i)
	{
		unsigned int value = std::to_integer<unsigned int>(dataToParse[bytePos++])
		+ (std::to_integer<unsigned int>(dataToParse[bytePos++]) << 8);
		// TODO: The Triple-PT measurements are not used so far! Just skip the bytes holding that data for now
		if (i == 1) { bytePos += 2; }
		else if (i > 1){ bytePos += 4; }
		// TODO END
		frame.scalarSensor[i] = static_cast<double>(value) / 10.0;
	}
	for (int i = 0; i < numMatrixSensors; ++i)
	{
		// TODO: What if there is more than one matrix sensor?
		// In that case we cannot just assume that the rest of the frame contains data from a single matrix sensor
		// What we need is the number of elements that belong to each matrix sensor. Maybe even rows and columns?
		
		for (; bytePos < dataToParse.size() - 1; ++bytePos)
		{
			int value = std::to_integer<unsigned int>(dataToParse[bytePos]);
			for (int bitOffset = 0; bitOffset < 8; ++bitOffset)
			{
				frame.matrixSensor[i].push_back((value & 0x01 << bitOffset));
			}
		}
	}

	return frame;
}

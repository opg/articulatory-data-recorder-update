#include "Randomizer.h"
#include <algorithm>


Randomizer::Randomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed)
	: numRepetitions_(numRepetitions), rng_{ randomSeed }, uniformDistribution_{ minIndex, maxIndex }
{

}

std::vector<size_t> Randomizer::getRandomizedIndices() const
{
	return randomizedIndices_;
}

void Randomizer::setIndexRange(size_t minIndex, size_t maxIndex)
{
	uniformDistribution_ = std::uniform_int_distribution<size_t>(minIndex, maxIndex );
}

void Randomizer::setNumRepetitions(int numRepetitions)
{
	numRepetitions_ = numRepetitions;
}

size_t Randomizer::getRandomIndex()
{
	return uniformDistribution_(rng_);
}

CyclicNonRandomRandomizer::CyclicNonRandomRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed)
	: Randomizer(minIndex, maxIndex, numRepetitions, randomSeed)
{
	CyclicNonRandomRandomizer::randomizeIndices();
}

void CyclicNonRandomRandomizer::randomizeIndices()
{
	const auto samplesPerRepetition = uniformDistribution_.max() - uniformDistribution_.min() + 1;
	const auto totalNumSamples = samplesPerRepetition * numRepetitions_;

	for (int sampleIdx = 0; sampleIdx < totalNumSamples; ++sampleIdx)
	{
		randomizedIndices_.emplace_back(uniformDistribution_.min() + (sampleIdx % samplesPerRepetition));
	}
}

RandomizerWithReplacement::RandomizerWithReplacement(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed)
	: Randomizer(minIndex, maxIndex, numRepetitions, randomSeed)
{
	RandomizerWithReplacement::randomizeIndices();
}

void RandomizerWithReplacement::randomizeIndices()
{
	const auto samplesPerRepetition = uniformDistribution_.max() - uniformDistribution_.min() + 1;
	const auto totalNumSamples = samplesPerRepetition * numRepetitions_;

	for (int sampleIdx = 0; sampleIdx < totalNumSamples; ++sampleIdx)
	{
		randomizedIndices_.emplace_back(getRandomIndex());
	}
}

PermuteAllRandomizer::PermuteAllRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions, int randomSeed)
	: Randomizer(minIndex, maxIndex, numRepetitions, randomSeed)
{
	PermuteAllRandomizer::randomizeIndices();
}

void PermuteAllRandomizer::randomizeIndices()
{
	// Get a list of non-randomized indices, repeated numRepetitions_ times
	const CyclicNonRandomRandomizer csr(uniformDistribution_.min(), uniformDistribution_.max(), numRepetitions_);
	randomizedIndices_ = csr.getRandomizedIndices();
	// Shuffle the list
	std::shuffle(randomizedIndices_.begin(), randomizedIndices_.end(), rng_);
}

PermuteBalancedRandomizer::PermuteBalancedRandomizer(size_t minIndex, size_t maxIndex, int numRepetitions,
	int randomSeed)
	: Randomizer(minIndex, maxIndex, numRepetitions, randomSeed)
{
	PermuteBalancedRandomizer::randomizeIndices();
}

void PermuteBalancedRandomizer::randomizeIndices()
{
	std::vector<size_t> tmpIndices;

	for (auto tmpIndex = uniformDistribution_.min(); tmpIndex <= uniformDistribution_.max(); ++tmpIndex)
	{
		tmpIndices.push_back(tmpIndex);
	}

	for (auto repIdx = 0; repIdx < numRepetitions_; ++repIdx)
	{
		std::shuffle(tmpIndices.begin(), tmpIndices.end(), rng_);
		randomizedIndices_.insert(randomizedIndices_.end(), tmpIndices.begin(), tmpIndices.end());
	}
}

PermuteBalancedNoDoubletsRandomizer::PermuteBalancedNoDoubletsRandomizer(size_t minIndex, size_t maxIndex,
	int numRepetitions, int randomSeed)
	: Randomizer(minIndex, maxIndex, numRepetitions, randomSeed)
{
	PermuteBalancedNoDoubletsRandomizer::randomizeIndices();
}

void PermuteBalancedNoDoubletsRandomizer::randomizeIndices()
{
	std::vector<size_t> tmpIndices;

	for (auto tmpIndex = uniformDistribution_.min(); tmpIndex <= uniformDistribution_.max(); ++tmpIndex)
	{
		tmpIndices.push_back(tmpIndex);
	}

	for (auto repIdx = 0; repIdx < numRepetitions_; ++repIdx)
	{
		do
		{
			std::shuffle(tmpIndices.begin(), tmpIndices.end(), rng_);
		} while (repIdx > 0 && tmpIndices.front() == randomizedIndices_.back());

		randomizedIndices_.insert(randomizedIndices_.end(), tmpIndices.begin(), tmpIndices.end());
	}
}

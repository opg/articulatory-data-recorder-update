#pragma once
#include <vector>

#include "HardwareBackend.h"
#include "FrameBuffer.h"
#include "Palate.h"
#include "ExperimentManager.h"
/**
 * This singleton class is used to store all objects that need to be accessed by otherwise unrelated objects in the frontend.
 */
class Data 
{
public:
	// Use this method to obtain the only instance of Data in existence. Note: You cannot assign it for thread-safety reasons!
    static Data& getInstance()
    {
        static Data instance;
        return instance;
    }
    // Delete copy constructor to avoid copies being made 
    Data(Data const&) = delete;
    // Delete assignment operator: May only access object through Data::getInstance() (thread-safe)
    void operator=(Data const&) = delete;

    void setPalate(Palate newPalate);

	
public:
    static const int kMaxBufferFrames{ 360000 };  // 1 hour @ 100 Hz frame rate
    int audioSamplingRate{ 16000 };
    int maxAudioSamples{ audioSamplingRate * 3600 };  // 1 hour @ 16 kHz
    FrameBuffer frameBuffer{ kMaxBufferFrames };
    ScalarSignal audioBuffer{ "Audio", "s", "", audioSamplingRate, std::make_pair(-1, 1), std::vector<int>{1}, maxAudioSamples };
    HardwareBackend::Type currentBackend;
	
private:
	// Private constructor
    Data();

    Palate palate_;
};


#pragma once
#include <vector>

#include "Frame.h"
#include "ScalarSignal.h"


// A FrameBuffer stores the sensor data in a more convenient fashion as individual signals 
class FrameBuffer
{
	// TODO:
	// - Add grouping variable to facilitate plotting of several signals in the same picture
public:
	FrameBuffer() = default;
	FrameBuffer(const int maxNumFrames);
	FrameBuffer(const int numScalarSignals, const int numMatrixSignals, 
		const int maxNumFrames);
	void clear();
	void reset();
	[[nodiscard]] size_t capacity() const;
	[[nodiscard]] size_t size() const;
	void addFrame(Frame newFrame);
	Frame getFrame(size_t index);
	Frame getCurrentFrame();
	[[nodiscard]] int getCurrentFrameIndex() const;
	[[nodiscard]] int getLastFrameIndex() const;
	[[nodiscard]] int getSamplingRate() const;
	ScalarSignal& getScalarSignal(int index);
	std::vector<ScalarSignal>& getScalarSignals();
	MatrixSignal& getMatrixSignal(int index);
	[[nodiscard]] const std::vector<MatrixSignal>& getMatrixSignals() const;
	[[nodiscard]] std::vector<std::vector<int>> getMatrixDimensions() const;

	[[nodiscard]] int maxNumFrames() const;
	[[nodiscard]] int numScalarSignals() const;
	[[nodiscard]] int numMatrixSignals() const;
	void setNumMatrixSignals(int numMatrixSignals);
	void setNumScalarSignals(int numScalarSignals);
	void addMatrixSignal(const MatrixSignal& newMatrixSignal);
	void addScalarSignal(const ScalarSignal& newScalarSignal);
	void setMaxNumFrames(int maxNumFrames);


public:


private:
	int maxNumFrames_{ 0 };
	std::vector<int> frameIndex_;  // Holds the frame index as transmitted by the hardware device (may be non-contiguous due to data loss!)
	int currentFrameIndex_{ -1 }; 
	int lastFrameIndex_{ -1 };
	int samplingRate_{ 0 };
	int numScalarSignals_{ 0 };
	int numMatrixSignals_{ 0 };
	std::vector<ScalarSignal> scalarSignals;  // e.g., distance sensors data: size of outer vector -> number of sensors, size of inner vector -> time steps
	std::vector<MatrixSignal> matrixSignals; // e.g., contact sensor data: size of outer vector -> number of sensors, middle vector -> time steps, inner vector -> linearly indexed contact matrix

private:
	void resize();  // Resize the signal containers to their respective number of signals
	void reserve();  // Reserve space in each signal for the maximum number of frames


public:
	Frame operator[](int i)
	{
		return getFrame(i);
	}
};
#pragma once
#include <memory>

#include "Signal.h"


class ScalarSignal :
    public Signal<double>
{
public:
	ScalarSignal() = default;
	ScalarSignal(const std::string& name, const std::string& unitX, const std::string& unitY,
		int samplingRate, const std::pair<double, double>& range, const std::vector<int>& dimensions, int reservedSize, Calibrator<double, double>* calibrator = nullptr)
		: Signal<double>(name, unitX, unitY, samplingRate, range, dimensions, reservedSize), calibrator_(calibrator)
	{
	}

	ScalarSignal(const Sensor& sensor)
		: Signal<double>(sensor),
	calibrator_(sensor.calibrator), unit_calibrated_y(sensor.unit_calibrated_y), calibrated_range(sensor.calibratedRange)
	{
	}

	~ScalarSignal() = default;

	void useCalibrator(bool doUse = false);
	[[nodiscard]] bool isCalibrated() const;
	Calibrator<double, double>* getCalibrator();
	value_type getValue(size_type pos) const override;

	std::pair<double, double> current_range{ range };
	std::pair<double, double> calibrated_range;
	std::string unit_calibrated_y;
	std::string current_unit_y{ unit_y };  // Use the raw unit_y or the unit_calibrated_y
	
private:
	std::shared_ptr<Calibrator<double, double>> calibrator_{ nullptr };
	bool useCalibrator_{ false };
};


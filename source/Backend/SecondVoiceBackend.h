#pragma once
#include "ByteDataCollector.h"
#include "SecondVoiceParser.h"

#include <vector>

class SecondVoiceBackend :
	public ByteDataCollector
{
public:
	SecondVoiceBackend(wxEvtHandler* owner, FrameBuffer& buffer)
		: ByteDataCollector(owner, buffer, sensors, new SecondVoiceParser(sensors, contactMapping),
			std::make_pair(std::byte{ 0x5A }, std::byte{ 0xA5 }),
			34, portSettings)
	{
	}

	static const int samplingRate_Hz{ 100 };
	inline static const std::vector<Sensor> sensors
	{
		Sensor("Lip 0", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Lip 1", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Velum", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("D0", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("D1", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("D2", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("D3", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("D4", "s", "LSB", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 4095.0)),
		Sensor("Contacts", "s", "", std::vector<int>{7, 7}, samplingRate_Hz, std::make_pair(0.0, 1.0)),
		Sensor("F0", "s", "Hz", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 400.0)),
		Sensor("Voicing", "s", "", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 1.0)),
		Sensor("Breathiness", "s", "", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 1.0)),
		Sensor("Pressure", "s", "Pa", std::vector<int>{1}, samplingRate_Hz, std::make_pair(0.0, 2000.0))
	};
	inline static const SerialPort::PortSettings portSettings{ "COM3", 115200, SerialPort::NO_PARITY, SerialPort::ONE_STOP_BIT, false, 0, 0 };
	// Enter the contact mapping here: use the index in unsorted linearized contact data vector -> index in sorted linearized contact data vector
	// Linearized index: (rowIndex * numColumns) + columnIndex
	inline static const std::vector<int> contactMapping
	{
		(3 * 7) + 6,
		(3 * 7) + 5,
		(3 * 7) + 4,
		(2 * 7) + 5,
		(2 * 7) + 4,
		(1 * 7) + 5,
		(1 * 7) + 4,
		(0 * 7) + 4,
		(0 * 7) + 2,
		(1 * 7) + 2,
		(1 * 7) + 1,
		(2 * 7) + 2,
		(2 * 7) + 1,
		(3 * 7) + 2,
		(3 * 7) + 1,
		(3 * 7) + 0,
		(4 * 7) + 6,
		(4 * 7) + 5,
		(4 * 7) + 4,
		(5 * 7) + 5,
		(5 * 7) + 4,
		(6 * 7) + 6,
		(6 * 7) + 5,
		(6 * 7) + 4,
		(6 * 7) + 2,
		(6 * 7) + 1,
		(6 * 7) + 0,
		(5 * 7) + 2,
		(5 * 7) + 1,
		(4 * 7) + 2,
		(4 * 7) + 1,
		(4 * 7) + 0
	};
};
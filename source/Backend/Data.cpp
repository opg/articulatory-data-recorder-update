#include "Data.h"

#include <stdexcept>

#include "SoundInterface.h"

void Data::setPalate(Palate newPalate)
{
	palate_ = newPalate;

	if(frameBuffer.numScalarSignals() != palate_.numDistanceSensors)
	{
		throw std::logic_error("Number of distance sensors in palate files does not match current backend!");
	}

	for (unsigned int idx = 0; idx < palate_.numDistanceSensors; ++idx)
	{
		std::vector<Calibrator<double, double>::calibrationPoint> calibrationPoints;
		for (const auto& p : palate_.distanceSensors[idx].calibration_dc_mm_adc)
		{
			calibrationPoints.emplace_back(p.second, p.first);  // Calibrated value comes before raw value in palate file format!
	
		}
		auto* calibrator = frameBuffer.getScalarSignal(idx).getCalibrator();
		if (calibrator != nullptr)
		{
			calibrator->setCalibrationPoints(calibrationPoints);
		}
	}
}

Data::Data()
{
	SoundInterface::getInstance().setPlaybackBuffer(&audioBuffer.getSamples(), &audioBuffer.getSamplingRate_Hz());
	SoundInterface::getInstance().setRecordingBuffer(&audioBuffer.getSamples(), &audioBuffer.getSamplingRate_Hz());
}

#pragma once
#include <fstream>
#include <vector>
#include <string>

#pragma pack(1)

class File
{

protected:
	virtual void readFromStream(std::istream& is) = 0;
	virtual void writeToStream(std::ostream& os) = 0;


public:
	friend std::ostream& operator<<(std::ostream& os, File& obj)  // Operator cannot be made virtual, so make it instead depend on a virtual function
	{
		obj.writeToStream(os);
		return os;
	}

	friend std::istream& operator>>(std::istream& is, File& obj)  // Operator cannot be made virtual, so make it instead depend on a virtual function
	{
		obj.readFromStream(is);
		return is;
	}

};

#pragma pack()

#pragma once

#include <string>

#include "File.h"
#include "ExperimentManager.h"

#pragma pack(1)

class ExperimentFile :
    public File
{
public:
	ExperimentFile() = default;
	ExperimentFile(Experiment<std::string> experiment);

	[[nodiscard]] Experiment<std::string> getExperiment() const;
	
protected:
	void readFromStream(std::istream& is) override;
	void writeToStream(std::ostream& os) override;

private:
	Experiment<std::string> experiment_;
	
};

#pragma pack()
#pragma once
#include <string>
#include <vector>

/// @brief An abstract interface to the platform's sound devices.
class SoundInterface
{
public:
	/// @brief A struct describing a sound device.
	struct DeviceInfo
	{
		int index;
		int hostApiIndex;
		std::string name;
		int maxInputChannels;
		int maxOutputChannels;
		double defaultLowInputLatency;
		double defaultLowOutputLatency;
		double defaultHighInputLatency;
		double defaultHighOutputLatency;
		double defaultSampleRate;

		friend bool operator==(const DeviceInfo& lhs, const DeviceInfo& rhs)
		{
			return lhs.index == rhs.index
				&& lhs.hostApiIndex == rhs.hostApiIndex
				&& lhs.name == rhs.name
				&& lhs.maxInputChannels == rhs.maxInputChannels
				&& lhs.maxOutputChannels == rhs.maxOutputChannels
				&& lhs.defaultLowInputLatency == rhs.defaultLowInputLatency
				&& lhs.defaultLowOutputLatency == rhs.defaultLowOutputLatency
				&& lhs.defaultHighInputLatency == rhs.defaultHighInputLatency
				&& lhs.defaultHighOutputLatency == rhs.defaultHighOutputLatency
				&& lhs.defaultSampleRate == rhs.defaultSampleRate;
		}

		friend bool operator!=(const DeviceInfo& lhs, const DeviceInfo& rhs)
		{
			return !(lhs == rhs);
		}
	};

	inline static DeviceInfo noDevice{ -1, -1, "", 0, 0, 0, 0, 0, 0, 0 };

	/// @brief A struct describing an available host API
	struct ApiInfo
	{
		int index;
		std::string name;
		int numDevices;
		DeviceInfo defaultInputDevice;
		DeviceInfo defaultOutputDevice;

		friend bool operator==(const ApiInfo& lhs, const ApiInfo& rhs)
		{
			return lhs.index == rhs.index
				&& lhs.name == rhs.name
				&& lhs.numDevices == rhs.numDevices
				&& lhs.defaultInputDevice == rhs.defaultInputDevice
				&& lhs.defaultOutputDevice == rhs.defaultOutputDevice;
		}

		friend bool operator!=(const ApiInfo& lhs, const ApiInfo& rhs)
		{
			return !(lhs == rhs);
		}
	};

	inline static ApiInfo noApi{ -1, "", 0, noDevice, noDevice };

	/// @brief The state of an input or output sound device.
	enum class Status
	{
		STOPPED,
		ACTIVE,
		PAUSED
	};

public:
	virtual ~SoundInterface() = default;
	virtual void reset() = 0;

	// Device management 
	virtual ApiInfo getHostApiInfo(int index) = 0;
	virtual std::vector<ApiInfo> getHostApis() = 0;
	virtual ApiInfo getDefaultApi() = 0;
	virtual ApiInfo getCurrentApi() = 0;
	virtual DeviceInfo getDeviceInfo(int index) = 0;
	virtual std::vector<DeviceInfo> getDevices() = 0;
	virtual DeviceInfo getCurrentInputDevice() = 0;
	virtual DeviceInfo getCurrentOutputDevice() = 0;
	virtual DeviceInfo getDefaultInputDevice() { return DeviceInfo(); }
	virtual DeviceInfo getDefaultOutputDevice() { return DeviceInfo(); }

	virtual void setInputDevice(const DeviceInfo& device) = 0;
	virtual void setInputDevice(int index) = 0;
	virtual void setOutputDevice(const DeviceInfo& device) = 0;
	virtual void setOutputDevice(int index) = 0;

	virtual void setDevices(const DeviceInfo& input, const DeviceInfo& output) { setInputDevice(input); setOutputDevice(output); }

	virtual bool isSamplingRateSupported(DeviceInfo device, unsigned samplingRate_Hz) = 0;

	virtual bool isRunning() { return getPlaybackStatus() == Status::ACTIVE || getRecordingStatus() == Status::ACTIVE; }

	// Playback methods
	virtual double getPlaybackSamplingRate() = 0;
	virtual void setPlaybackSamplingRate(unsigned samplingRate_Hz) = 0;
	virtual Status playAsynchronously() = 0;
	virtual Status playAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz) = 0;
	virtual Status playAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz, double start_s,
	                                  double end_s) = 0;
	virtual Status playAsynchronously(double start_s, double end_s) = 0;
	virtual bool playBlocking() = 0;
	virtual bool playBlocking(std::vector<double>* buffer, unsigned* samplingRate_Hz) = 0;
	virtual bool playBlocking(double start_s, double end_s) = 0;

	virtual bool pausePlayback() = 0;
	virtual bool stopPlayback() = 0;
	virtual void resetPlayer() = 0;

	virtual Status getPlaybackStatus() = 0;
	virtual bool isRepeating() { return isRepeating_; }
	virtual void isRepeating(bool repeat) { isRepeating_ = repeat; }

	virtual void setPlaybackBuffer(std::vector<double>* newBuffer, unsigned* samplingRate_Hz) = 0;
	virtual void setPlaybackStartPos(unsigned newStartPos) = 0;
	virtual void setPlaybackEndPos(unsigned newEndPos) = 0;

	// Recording methods
	virtual double getRecordingSamplingRate() = 0;
	virtual void setRecordingSamplingRate(unsigned samplingRate_Hz) = 0;
	virtual Status recordAsynchronously() = 0;
	virtual Status recordAsynchronously(std::vector<double>* buffer, unsigned* samplingRate_Hz) = 0;
	virtual Status recordBlocking(double time_s) = 0;
	virtual Status recordBlocking(std::vector<double>* buffer, unsigned* samplingRate_Hz, double time_s) = 0;

	virtual bool pauseRecording() = 0;
	virtual Status stopRecording() = 0;
	virtual bool resetRecording() = 0;

	virtual Status getRecordingStatus() = 0;

	virtual void setRecordingBuffer(std::vector<double>* newBuffer, unsigned* samplingRate_Hz) = 0;
	virtual bool attachRecordingBuffer(bool attach = true) = 0;
	virtual bool detachRecordingBuffer(bool detach = true) = 0;

	static SoundInterface& getInstance();

	// Deleted methods and operators due to singleton pattern
	SoundInterface(const SoundInterface&) = delete;
	SoundInterface(SoundInterface&&) = delete;
	SoundInterface& operator=(const SoundInterface&) = delete;
	SoundInterface& operator=(const SoundInterface&&) = delete;

protected:
	// Protected constructor: Singleton pattern
	SoundInterface() = default;

private:
	bool isRepeating_{false};
};

 
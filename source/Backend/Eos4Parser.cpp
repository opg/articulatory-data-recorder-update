#include "Eos4Parser.h"

#include <stdexcept>

Frame Eos4Parser::parse(const std::vector<std::byte>& dataToParse)
{
	static const int kNumOpticalSensors = 7;
	static const int kLastContactBytePos = 24;
	
	Frame frame;
	int bytePos = 0;
	frame.index = std::to_integer<unsigned int>(dataToParse[bytePos++])
		+ (std::to_integer<unsigned int>(dataToParse[bytePos++]) << 8);

	frame.scalarSensor.resize(numScalarSensors);
	frame.matrixSensor.resize(numMatrixSensors);

	// In the EOS 4.0 data frame from the MCU, first come only 7 scalar sensors
	int i;   // Will be needed again later
	for (i = 0; i < kNumOpticalSensors; ++i)
	{
		unsigned int value = std::to_integer<unsigned int>(dataToParse[bytePos++])
			+ (std::to_integer<unsigned int>(dataToParse[bytePos++]) << 8);
		frame.scalarSensor[i] = static_cast<double>(value);
	}
	// Then comes the contact data 
	for (int j = 0; j < numMatrixSensors; ++j)
	{
		int contactIdx{ 0 };
		int matrixSize{ 1 };
		for (const auto& dim : this->sensors[i + j].inputDimensions)
		{
			matrixSize *= dim;
		}
		// Initialize linearly indexed matrix with -1 so values that are not overwritten will eventually be drawn as "no contact sensor"
		frame.matrixSensor[j].resize(matrixSize, -1);
		for (; bytePos < kLastContactBytePos; ++bytePos)
		{
			int value = std::to_integer<unsigned int>(dataToParse[bytePos]);
			for (int bitOffset = 0; bitOffset < 8; ++bitOffset)
			{
				// Sort the data so their position is the linearized index into the contact pattern matrix
				if (contactIdx < contactIndexMap.size())
				{
					frame.matrixSensor[j][contactIndexMap[contactIdx]] = (value >> bitOffset & 0x01);
				}
				else
				{
					break;
				}
				contactIdx++;
			}
		}
	}
	
	return frame;
}

#include "StringPromptsPage.h"

static const int IDB_PREVIOUS_PROMPT = 7001;
static const int IDB_NEXT_PROMPT = 7002;

wxDEFINE_EVENT(INTRO_REACHED, wxCommandEvent);
wxDEFINE_EVENT(OUTRO_REACHED, wxCommandEvent);
wxDEFINE_EVENT(PAGE_UPDATE, wxCommandEvent);

BEGIN_EVENT_TABLE(StringPromptsPage, wxPanel)
EVT_SIZE(StringPromptsPage::OnResize)
EVT_COMMAND(wxID_ANY, PAGE_UPDATE, OnUpdate)
EVT_COMMAND(kExperimentStart, INTRO_REACHED, OnIntroReached)
EVT_COMMAND(kExperimentEnd, OUTRO_REACHED, OnOutroReached)
EVT_MENU(IDB_PREVIOUS_PROMPT, OnPreviousPrompt)
EVT_MENU(IDB_NEXT_PROMPT, OnNextPrompt)
END_EVENT_TABLE()


StringPromptsPage::StringPromptsPage(wxWindow* parent, ExperimentManager<std::string>* ptrExperimentManager, bool fireEvents, wxWindowID id) : PromptsPage<std::string>(parent, ptrExperimentManager, id), fireEvents(fireEvents)
{
	initWidgets();
	
	// ****************************************************************************
	// Accelerator Table
	// ****************************************************************************
	wxAcceleratorEntry entries[2];
	entries[0].Set(wxACCEL_NORMAL, WXK_LEFT, IDB_PREVIOUS_PROMPT);
	entries[1].Set(wxACCEL_NORMAL, WXK_RIGHT, IDB_NEXT_PROMPT);
	const wxAcceleratorTable hotKeys(2, entries);
	this->wxWindowBase::SetAcceleratorTable(hotKeys);
}

void StringPromptsPage::update()
{
	if (ptrExperimentManager_ != nullptr)
	{
		if (!ptrExperimentManager_->getQueue().empty())
		{
			if (ptrExperimentManager_->experimentHasFinished)
			{
				showPrompt->SetLabel(ptrExperimentManager_->getExperiment().outroMessage);
				wxCommandEvent event(OUTRO_REACHED, kExperimentEnd);
				if (fireEvents && outroReachedEventFired == false)
				{
					outroReachedEventFired = true;
					ProcessEvent(event);
				}
			}
			else if (ptrExperimentManager_->experimentHasStarted)
			{
				outroReachedEventFired = false;
				introReachedEventFired = false;
				showPrompt->SetLabel(ptrExperimentManager_->currentPrompt->getContent());
			}
			else
			{
				showPrompt->SetLabel(ptrExperimentManager_->getExperiment().introMessage);
				wxCommandEvent event(INTRO_REACHED, kExperimentStart);			
				if (fireEvents && introReachedEventFired == false)
				{
					introReachedEventFired = true;
					ProcessEvent(event);

				}
			}
			updateLayout();
		}
		else
		{
			reset();
		}
	}
}

void StringPromptsPage::reset()
{
	showPrompt->SetLabel("Please load/configure your experiment!");
	outroReachedEventFired = false;
	introReachedEventFired = false;
	updateLayout();
}

void StringPromptsPage::OnIntroReached(wxCommandEvent& event)
{
	event.Skip();
}

void StringPromptsPage::OnOutroReached(wxCommandEvent& event)
{
	event.Skip();
}


void StringPromptsPage::initWidgets()
{
	showPrompt = new TextCtrlSelfExpanding(this, wxID_ANY, "Load/Configure your experiment!", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxNO_BORDER | wxTE_READONLY | wxTE_CENTER | wxTE_NO_VSCROLL);
	myFont = wxFont(18, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	showPrompt->SetFont(myFont);

	topLevelSizer = new wxBoxSizer(wxVERTICAL);
	topLevelSizer->Add(showPrompt, wxSizerFlags().Expand());
	this->SetSizerAndFit(topLevelSizer);
}

void StringPromptsPage::OnResize(wxSizeEvent& event)
{
	updateLayout();
}

void StringPromptsPage::OnPreviousPrompt(wxCommandEvent& event)
{
	if (!ptrExperimentManager_->getQueue().empty())
	{
		ptrExperimentManager_->previous();
		wxCommandEvent event(PAGE_UPDATE, wxID_ANY);
		ProcessEvent(event);
	}
}

void StringPromptsPage::OnNextPrompt(wxCommandEvent& event)
{
	if (!ptrExperimentManager_->getQueue().empty())
	{
		ptrExperimentManager_->next();
		wxCommandEvent event(PAGE_UPDATE, wxID_ANY);
		ProcessEvent(event);
	}
}

void StringPromptsPage::OnUpdate(wxCommandEvent& event)
{
	event.ResumePropagation(wxEVENT_PROPAGATE_MAX);
	event.Skip();
}

void StringPromptsPage::updateLayout()
{
	this->Refresh();
	this->Layout();
	if (showPrompt != nullptr)
	{
		showPrompt->Update();
	}
}


#pragma once
#include <map>
#include <wx/dialog.h>
#include <wx/clrpicker.h>

#include "GraphPicture.h"

// This event is fired every time signal settings have been changed. Parent of the dialog window can catch.
wxDECLARE_EVENT(SignalPicPropChangedEvent, wxCommandEvent);

class SignalPicturePropertiesDialog : public wxDialog
{
public:
	SignalPicturePropertiesDialog(wxWindow* parent, std::map<std::string, GraphPicture::Properties>& properties);
	virtual ~SignalPicturePropertiesDialog() = default;

private:
	void initWidgets();
	void OnCheckboxEvent(wxCommandEvent& event);
	void OnChangeTrackLinewidth(wxSpinDoubleEvent& event);
	void OnChangeColor(wxColourPickerEvent& event);
	void OnEnter(wxCommandEvent& event);

	void postSignalPropChangedEvent();

	std::map<std::string, GraphPicture::Properties>* ptrSignalProperties_;


public:

	// ****************************************************************************
	// Declare the event table right at the end
	// ****************************************************************************
	DECLARE_EVENT_TABLE()
};


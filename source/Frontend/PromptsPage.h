#ifndef __PROMPTS_PAGE_H__
#define __PROMPTS_PAGE_H__

#include <wx/wx.h>
#include "../Backend/ExperimentManager.h"

template <class PromptContentType>
class PromptsPage : public wxPanel
{
	// ****************************************************************
	// Public functions.
	// ****************************************************************
public:
	PromptsPage<PromptContentType>(wxWindow* parent, ExperimentManager<PromptContentType>* ptrExperimentManager, wxWindowID id = wxID_ANY);
		
	// ****************************************************************
	// Private functions.
	// ****************************************************************
private:
	
	// ****************************************************************
	// Public data.
	// ****************************************************************
public:
	wxBoxSizer* sizer;
	wxBoxSizer* topLevelSizer;


	// ****************************************************************
	// Private data.
	// ****************************************************************
protected:
	ExperimentManager<PromptContentType>* ptrExperimentManager_{ nullptr };

	DECLARE_EVENT_TABLE()

};


#endif
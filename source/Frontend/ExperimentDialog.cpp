#include "ExperimentDialog.h"
#include <wx/spinctrl.h>

static const int IDC_RANDOMIZATION_STRATEGY = 12000;
static const int IDS_REPETITION = 12001;
static const int IDT_STARTING_TEXT = 12002;
static const int IDT_ENDING_TEXT = 12003;


BEGIN_EVENT_TABLE(ExperimentDialog, wxDialog)
EVT_COMBOBOX(IDC_RANDOMIZATION_STRATEGY, OnRandomizationStrategySelection)
EVT_BUTTON(wxID_OK, OnOk)
END_EVENT_TABLE()

ExperimentDialog::ExperimentDialog(wxWindow* parent) : ExperimentDialog(parent, Experiment<std::string>(std::vector<std::string>(), Settings()))
{
	
}

ExperimentDialog::ExperimentDialog(wxWindow* parent, const Experiment<std::string>& experiment) : wxDialog(parent, wxID_ANY, "Experiment settings", wxDefaultPosition, wxDefaultSize), experiment_(experiment)
{
	//this->wxWindowBase::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));

	auto* topLevelSizer = new wxBoxSizer(wxVERTICAL);
	auto* introMessageSizer = new wxStaticBoxSizer(wxHORIZONTAL, this, "Intro message:");
	auto* inventoryAndSettingsSizer = new wxBoxSizer(wxHORIZONTAL);
	auto* leftSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Inventory:");
	auto* rightSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Settings:");
	auto* rightUpperSizer = new wxBoxSizer(wxHORIZONTAL);
	auto* rightLowerSizer = new wxBoxSizer(wxHORIZONTAL);
	auto* randStrategyDescriptionSizer = new wxStaticBoxSizer(wxHORIZONTAL, this, "Randomization strategy description:");
	auto* outroMessageSizer = new wxStaticBoxSizer(wxHORIZONTAL, this, "Outro message:");

	introMessageInputCtrl_ = new wxTextCtrl(this, IDT_STARTING_TEXT, experiment.introMessage);
	introMessageSizer->Add(introMessageInputCtrl_, wxSizerFlags(1).Border(wxALL).Expand());
	topLevelSizer->Add(introMessageSizer, wxSizerFlags().Expand().Border(wxALL));

	inventoryInput = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_DONTWRAP);
	for (const auto& str : experiment.inventory)
	{
		if (str != experiment.inventory.back()) {
			inventoryInput->AppendText(str + "\n");
		}
		else
		{
			inventoryInput->AppendText(str);
		}
	}
	leftSizer->Add(inventoryInput, wxSizerFlags(1).Border(wxALL).Expand());
	auto tmp = inventoryInput->GetFont().GetPixelSize().y;
	inventoryInput->SetMinClientSize(wxSize(-1, inventoryLinesToDisplay * inventoryInput->GetFont().GetPixelSize().y));

	inventoryAndSettingsSizer->Add(leftSizer, wxSizerFlags(1).Expand().Border(wxALL));

	auto* repetitionString = new wxStaticText(this, wxID_ANY, "Repetitions:", wxDefaultPosition, wxDefaultSize);
	repetitionsCtrl_ = new wxSpinCtrl(this, IDS_REPETITION, "", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT, 1, 100, experiment.settings.repetitions);
	rightUpperSizer->Add(repetitionString, wxSizerFlags().Border(wxALL).CenterVertical());
	rightUpperSizer->Add(repetitionsCtrl_, wxSizerFlags().Border(wxALL).CenterVertical());
	repetitionsCtrl_->SetMinSize(wxSize(minimumWidgetSize, -1));

	wxArrayString choices;
	for (const auto& [randStratType, randStratStrings] : RandomizationStrategy::descriptions)
	{
		choices.push_back(randStratStrings.brief);
		randDisplayMap_.emplace(randStratStrings.brief, randStratType);
	}

	auto* randomizationStratString = new wxStaticText(this, wxID_ANY, "Randomization strategy:", wxDefaultPosition, wxDefaultSize);
	randomizationStratBox_ = new wxComboBox(this, IDC_RANDOMIZATION_STRATEGY, RandomizationStrategy::descriptions[experiment.settings.randStrategy.type].brief, wxDefaultPosition, wxDefaultSize, choices, wxCB_DROPDOWN | wxCB_READONLY);
	rightLowerSizer->Add(randomizationStratString, wxSizerFlags().Border(wxALL).CenterVertical());
	rightLowerSizer->Add(randomizationStratBox_, wxSizerFlags().Border(wxALL).CenterVertical());
	randomizationStratBox_->SetMinSize(wxSize(minimumWidgetSize, -1));

	randomizationStratDescriptionText_ = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxBORDER_NONE);
	randomizationStratDescriptionText_->SetLabel(RandomizationStrategy::descriptions[experiment.settings.randStrategy.type].verbose);
	randomizationStratDescriptionText_->wxWindowBase::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK));
	randStrategyDescriptionSizer->Add(randomizationStratDescriptionText_, wxSizerFlags(1).Border(wxALL).Expand());

	rightSizer->Add(rightUpperSizer, wxSizerFlags().Border(wxALL).Align(wxALIGN_RIGHT));
	rightSizer->Add(rightLowerSizer, wxSizerFlags().Border(wxALL).Align(wxALIGN_RIGHT));
	rightSizer->Add(randStrategyDescriptionSizer, wxSizerFlags(1).Border(wxALL).Expand());


	inventoryAndSettingsSizer->Add(rightSizer, wxSizerFlags(1).Expand().Border(wxALL));
	topLevelSizer->Add(inventoryAndSettingsSizer, wxSizerFlags(1).Border(wxALL));

	outroMessageInputCtrl_ = new wxTextCtrl(this, IDT_ENDING_TEXT, experiment.outroMessage, wxDefaultPosition, wxDefaultSize);
	outroMessageSizer->Add(outroMessageInputCtrl_, wxSizerFlags(1).Border(wxALL).Expand());
	topLevelSizer->Add(outroMessageSizer, wxSizerFlags().Expand().Border(wxALL));

	// Create and add the standard button sizer with separator line
	topLevelSizer->Add(ExperimentDialog::CreateSeparatedButtonSizer(wxOK | wxCANCEL), wxSizerFlags().Expand().Border(wxALL));


	this->SetSizerAndFit(topLevelSizer);
}


void ExperimentDialog::OnRandomizationStrategySelection(wxCommandEvent& event)
{
	randomizationStratDescriptionText_->SetLabel(RandomizationStrategy::descriptions[randDisplayMap_[randomizationStratBox_->GetValue().ToStdString()]].verbose);
}

void ExperimentDialog::OnOk(wxCommandEvent& event)
{
	experiment_.introMessage = introMessageInputCtrl_->GetValue();
	experiment_.inventory = splitDelimitedString(inventoryInput->GetValue().ToStdString(), "\n");
	experiment_.settings.repetitions = repetitionsCtrl_->GetValue();
	experiment_.settings.randStrategy = RandomizationStrategy(randDisplayMap_[randomizationStratBox_->GetValue().ToStdString()]);
	experiment_.outroMessage = outroMessageInputCtrl_->GetValue();

	EndModal(wxID_OK);
}

Experiment<std::string>& ExperimentDialog::getExperiment()
{
	return this->experiment_;
}

std::vector<std::string> ExperimentDialog::splitDelimitedString(std::string delimitedString, std::string delimiter)
{
	size_t pos;
	std::vector<std::string> splitString;
	while ((pos = delimitedString.find(delimiter)) != std::string::npos) {
		std::string token = delimitedString.substr(0, pos);
		splitString.push_back(token);
		delimitedString.erase(0, pos + delimiter.length());
	}
	if (!delimitedString.empty())
	{
		splitString.push_back(delimitedString);
	}

	return splitString;
}


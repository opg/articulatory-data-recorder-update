#pragma once
#include "BasicPicture.h"
#include "Graph.h"

// declare needed custom events
wxDECLARE_EVENT(SelectionChangedEvent, wxCommandEvent);
wxDECLARE_EVENT(ScrollRequestEvent, wxCommandEvent);

/// @brief This class extends the base class BasicPicture.
///
/// This class extends the functionality of the base class BasicPicture.
/// A graph picture object will hold properties and a selection. There are
/// various functions which allow manipulation of these properties and the selection.
class GraphPicture : public BasicPicture
{
public:
	/// @brief This structs holds properties for a graph picture object.
	struct Properties
	{
		int pictureID{ -1 };
		wxColour backgroundColor{ *wxWHITE };
		wxColour lineColor{ *wxBLACK };
		int linewidth_px{ 1 };
		bool doNormalize{ false };
		bool isShown{ true };
	};

	/// @brief This class is a class for a selection. It allows the definition of
	/// a starting and an ending point, as well as making all parameters of the selection
	/// manipulatable.
	class Selection
	{
	public:
		/// @brief Defines the type of a marker.
		enum class Marker { Start, End, None };
		/// @brief Constructs a selection object with default parameters.
		Selection() = default;
		/// @brief Constructs a selection object with non-default values
		/// @param start Start of the selection in current abscissa units
		/// @param end End of the selection in current abscissa units
		Selection(double start, double end) : start_(start), hasStart_(true), end_(end), hasEnd_(true) {}

		/// @brief Invalidates the current selection.
		void clear() { hasStart_ = false; hasEnd_ = false; active_ = Marker::None; }
		
		/// @brief Returns the start of the selection object.
		/// @return starting value
		[[nodiscard]] double getStart() const;
		/// @brief Returns the center of the selection object.
		/// @return center value
		[[nodiscard]] double getCenter() const;		
		/// @brief Returns the end of the selection object.
		/// @return ending value
		[[nodiscard]] double getEnd() const;
        /// @brief Returns the length of the selection object.
		/// @return length of the selection
		[[nodiscard]] double getLength() const;



		/// @brief Returns the currently selected marker.
		/// @return active marker
		[[nodiscard]] Marker getActive() const;

		/// @brief Checks for a starting point of the selection object.
		/// @return Has a starting point
		[[nodiscard]] bool hasStart() const;

		/// @brief Checks for a ending point of the selection object.
		/// @return Has an ending point
		[[nodiscard]] bool hasEnd() const;

		/// @brief Validates the current selection.
		/// @return Is a valid selection
		[[nodiscard]] bool isValid() const { return (start_ < end_&& hasStart() && hasEnd()); }

		/// @brief Sets a new start of the selection object.
		/// @param newStartPos new start point
		/// @param startDragging Is currently dragged
		void setStart(const double newStartPos, bool startDragging = true);

		/// @brief Sets a new end of the selection object.
		/// @param newEndPos new end point
		void setEnd(const double newEndPos) { end_ = newEndPos; hasEnd_ = true; }

		/// @brief Sets the selected marker to be active.
		/// @param newActive new active marker
		void setActive(const Marker newActive) { active_ = newActive; }

		/// @brief Drags the active marker to a given position.
		/// @param newMarkerPos new marker position
		void dragActiveMarker(double newMarkerPos);

	private:
		double start_{ 0.0 };
		bool hasStart_{ false };
		double end_{ 0.0 };
		bool hasEnd_{ false };
		Marker active_{ Marker::None };

	};

	/// @brief Constructs a graph picture object.
	///
	/// Graph picture objects can be part of a stack. This helps when having
	/// multiple pictures to be shown simultaneously which share a common
	/// abscissa. Generating those objects as part of a stack will allow having a selection
	/// spanning over all graph picture objects being part of the stack.
	/// @param parent Parent of the graph picture object
	/// @param isPartOfStack is part of stack
	GraphPicture(wxWindow* parent, bool isPartOfStack = false);

	/// @brief Constructs a graph picture object with given visualization parameters.
	///
	/// Graph picture objects can be part of a stack. This helps when having
	/// multiple pictures to be shown simultaneously which share a common
	/// abscissa. Generating those objects as part of a stack will allow having a selection
	/// spanning over all graph picture objects being part of that stack.
	/// @param parent Parent of the graph picture object
	/// @param backgroundColor background color
	/// @param lineColor line color
	/// @param lineWidth_px line width in pixels
	/// @param isPartOfStack is part of stack
	GraphPicture(wxWindow* parent, wxColour backgroundColor, wxColour lineColor, int lineWidth_px = 3, bool isPartOfStack = false);

	/// @brief Constructs a graph picture object with given visualization parameters.
	///
	/// Graph picture objects can be part of a stack. This helps when having
	/// multiple pictures to be shown simultaneously which share a common
	/// abscissa. Generating those objects as part of a stack will allow having a selection
	/// spanning over all graph picture objects being part of that stack.
	/// @param parent Parent of the graph picture object
	/// @param abscissaProperties properties of the linear abscissa
	/// @param ordinateProperties properties of the linear ordinate
	/// @param backgroundColor background color
	/// @param lineColor line color
	/// @param lineWidth_px line width in pixels
	/// @param isPartOfStack is part of stack
	GraphPicture(wxWindow* parent, Graph::LinearDomain abscissaProperties, Graph::LinearDomain ordinateProperties,
		wxColour backgroundColor, wxColour lineColor, int lineWidth_px = 3, bool isPartOfStack = false);

	/// @brief Constructs a graph picture object with given visualization parameters.
	///
	/// Graph picture objects can be part of a stack. This helps when having
	/// multiple pictures to be shown simultaneously which share a common
	/// abscissa. Generating those objects as part of a stack will allow having a selection
	/// spanning over all graph picture objects being part of that stack.
	/// @param parent Parent of the graph picture object
	/// @param properties Properties of the graph picture
	/// @param isPartOfStack is part of stack
	GraphPicture(wxWindow* parent, Properties properties, bool isPartOfStack = false);

	/// @brief Sets the abscissa to be painted (or not).
	/// @param abscissaIsVisible abscissa will be painted
	void virtual paintAbscissa(bool abscissaIsVisible);

	/// @brief Returns if abscissa will be painted.
	/// @return abscissa is painted
	bool virtual isAbscissaPainted();

	double zoomIn();
	double zoomOut();
	std::pair<double, double> zoomToSelection();
	virtual double zoomToAll();
	
	// Getter

	/// @brief Returns the picture id of graph picture object.
	/// @return picture id
	[[nodiscard]] int getPictureID() const;

	/// @brief Returns the background color of graph picture object.
	/// @return background color
	[[nodiscard]] wxColour getBackgroundColor() const;

	/// @brief Returns the line color of graph picture object.
	/// @return line color
	[[nodiscard]] wxColour getLineColor() const;

	/// @brief Returns the line width of graph picture object.
	/// @return line width in pixels
	[[nodiscard]] int getLineWidth() const;

	/// @brief Returns if the graph picture object will show the data
	/// being normalized (or not)
	/// @return is data normalized
	[[nodiscard]] bool isNormalized() const;

	/// @brief Returns the properties of graph picture object.
	/// @return properties
	[[nodiscard]] Properties getProperties() const;

	/// @brief Returns if graph picture object has a selection
	/// @return has a selection
	[[nodiscard]] bool hasSelection() const;

	/// @brief Returns if graph picture object has a valid selection.
	/// @return has valid selection
	[[nodiscard]] bool hasValidSelection() const;

	/// @brief Returns the start of the selection of graph picture object.
	/// @return value of selection start
	[[nodiscard]] double getSelectionStart() const;

	/// @brief Returns the end of the selection of graph picture object.
	/// @return value of selection end
	[[nodiscard]] double getSelectionEnd() const;

	/// @brief Returns the highlighting color of the selection.
	/// @return highlighting color
	[[nodiscard]] wxColour getHighlightColor() const;

	/// @brief Returns the alpha value (opaqueness) of the selection.
	/// @return value of the opaqueness
	[[nodiscard]] double getHighlightAlpha() const;

	// Setter

	/// @brief Sets the graph picture object to show the data normalized
	/// @param doNormalize normalize data
	void isNormalized(bool doNormalize);

	/// @brief Sets the id of the graph picture object.
	/// @param newPictureID new picture id
	void setPictureID(const int newPictureID);

	/// @brief Sets the background color of the graph picture object.
	/// @param backgroundColor new background color
	void setBackgroundColor(const wxColour& backgroundColor);

	/// @brief Sets the line color of the graph picture object.
	/// @param lineColor new line color
	void setLineColor(const wxColour& lineColor);

	/// @brief Sets the line width of the graph picture object.
	/// @param lineWidth_px new line width
	void setLineWidth(const int lineWidth_px);

	/// @brief Sets the visibility of the graph picture object.
	/// @param isShown the picture shall be shown
	void setIsShown(const bool isShown);

	/// @brief Sets the properties of the graph picture object.
	/// @param newProperties new properties
	void setProperties(const Properties& newProperties);

	/// @brief Sets a selection to the graph picture object.
	/// @param newSelection object holding the new selection
	void setSelection(const Selection& newSelection);

	/// @brief Sets the start of the selection of the graph picture object.
	/// @param i index of the new start of the selection
	void setSelectionStart(int i);

	/// @brief Sets the start of the selection of the graph picture object.
	/// @param i index of the new end of the selection
	void setSelectionEnd(int i);

	/// @brief Drags the selection to a given position.
	/// @param i index of the new position of the selection
	void dragSelection(int i);

	/// @brief Invalidates the selection of the graph picture object.
	void clearSelection();

	/// @brief Sets the highlighting color for the selection.
	/// @param newColor new highlighting color
	void setHighlightColor(wxColour newColor);

	/// @brief Sets the alpha value (opaqueness) of the selection.
	/// @param newAlpha new alpha value (opaqueness)
	void setHighlightAlpha(double newAlpha);

	Graph graph;

protected:
	/// @brief Returns an opaque color in dependency on the given alpha value.
	/// @param alpha alpha value (opaqueness)
	/// @param background background color
	/// @param foreground foreground color
	/// @return opaque color
	static wxColour alpha2rgb(double alpha, wxColour background, wxColour foreground);
	
	/* There are a number of draw methods to allow layered drawing of the graph and the signal(s) */
	
	/// @brief Draws to the given device context.
	///
	/// Overrides the base function "draw()" from BasicPicture to manage drawing
	/// for the graph picture object.
	/// @param dc device context for painting (see wxWidgets documentation)
	void draw(wxDC& dc) override;

	/// @brief Draws the graph including the background of the graph.
	///
	/// Calls drawGraphBackground() and drawGraphAxes() internally.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawGraph(wxDC& dc);

	/// @brief Draws the background of the graph.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawGraphBackground(wxDC& dc);

	/// @brief Draws the axes of the graph.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawGraphAxes(wxDC& dc);

	/// @brief Draws the selection of the graph picture object.
	///
	/// Order of drawing the components of the selection must not be changed
	/// in order to secure a valid visualization. This function calls drawSelectionMark()
	/// and drawSelectionHighlight() internally.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawSelection(wxDC& dc);

	/// @brief Draws a selection marker at a given position.
	/// @param dc device context for painting (see wxWidgets documentation)
	/// @param xPos index of the position of the selection mark
	/// @param markerType Start, End or None
	void drawSelectionMark(wxDC& dc, int xPos, Selection::Marker markerType);

	/// @brief Draws all available selection markers.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawSelectionMarks(wxDC& dc);

	/// @brief Highlights the area between selection markers.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawSelectionHighlight(wxDC& dc);

	
	wxBrush backgroundBrush{ *wxWHITE };
	wxPen linePen{ *wxBLACK, 3 };
	wxColour highlightColor{ 0, 158, 224 };
	double highlightAlpha{ 0.5 };	
	wxBrush highlightedBrush
	{
	alpha2rgb(highlightAlpha, backgroundBrush.GetColour(), highlightColor)
	};
	wxPen highlightedLinePen
	{
		alpha2rgb(highlightAlpha, linePen.GetColour(), highlightColor),
		3
	};

private:
	/// @brief Event handler for events emitted when setting a selection start.
	void OnSetSelectionStart(wxCommandEvent& event);

	/// @brief Event handler for events emitted when setting a selection end.
	void OnSetSelectionEnd(wxCommandEvent& event);

	/// @brief Event handler for events emitted when clearing a selection.
	void OnSelectionClear(wxCommandEvent& event);

	/// @brief Event handler for events emitted when pressing the left mouse button.
	void OnLeftDown(wxMouseEvent& event);

	/// @brief Event handler for events emitted when dragging with pressed mouse buttons.
	void OnDrag(wxMouseEvent& event);

	/// @brief Event handler which checks if cursor is still dragged. 
	void OnDragTimer(wxTimerEvent& event);

	/// @brief Event handler for events emitted when stop pressing left mouse button.
	void OnLeftUp(wxMouseEvent& event);

	/// @brief Event handler for events emitted when cursor enters or leaves the area
	/// of the graph picture object.
	void OnMouseLeaveOrEnter(wxMouseEvent& event);

	/// @brief Posts a scroll request to itself (the same graph picture object).
	///
	/// The ScrollRequestEvent will propagate up as long it's not handled
	/// on the graph picture object's level until it's handled properly.
	/// @param newReference new reference of the graph.
	void postScrollRequestEvent(double* newReference);

	/// @brief Posts a selection changed event to itself (the same graph picture object).
	///
	/// The SelectionChangedEvent will propagate up as long it's not handled
	/// on the graph picture object's level until it's handled properly.
	/// @param newSelection new selection of the graph.
	void postSelectionChangedEvent(Selection* newSelection);

	bool abscissaIsPainted_{ true };
	wxPen graphPen{ linePen.GetColour(), 1 };
	double xMin_{ 0.0 }, xMax_{ 10.0 }, yMin_{ 0.0 }, yMax_{ 1.0 };
	bool isNormalized_;
	bool isPartOfStack_;
	int pictureID_;
	inline static int IDT_DRAG_UPDATE{ wxNewId() };
	wxTimer* dragTimer_{ new wxTimer(this, IDT_DRAG_UPDATE) };
	int dragTimerPeriod_{ 20 };  // In milliseconds
	Selection selection_;

	DECLARE_EVENT_TABLE()
};

inline double GraphPicture::zoomToAll()
{
	// Does nothing here, may be used in derived classes to fit graph scaling to content
	return graph.abscissa.reference;
}

inline std::pair<double, double> GraphPicture::zoomToSelection()
{
	if (this->hasValidSelection())
	{
		graph.abscissa.reference = this->getSelectionStart();
		graph.abscissa.positiveLimit = this->getSelectionEnd() - graph.abscissa.reference;
	}

	return { graph.abscissa.reference, graph.abscissa.positiveLimit };
}

inline double GraphPicture::zoomIn()
{
	// When zooming in, the focus of attention should be in the center of the graph area.

	const double currentRange = graph.abscissa.positiveLimit;
	const double newRange = currentRange / 2;

	double center{ 0.0 };
	if (this->hasValidSelection())
	{
		// Try to center the selection after the zooming, but limit reference to 0
		center = selection_.getCenter();
	}
	else
	{
		// Try to center the current center frame
		center = graph.abscissa.reference + graph.abscissa.positiveLimit / 2;
	}

	graph.abscissa.reference = std::max( center - newRange / 2, 0.0);
	graph.abscissa.positiveLimit /= 2;
	return graph.abscissa.positiveLimit;
}

inline double GraphPicture::zoomOut()
{
	// When zooming out, the focus of attention should be in the center of the graph area.

	const double currentRange = graph.abscissa.positiveLimit;
	const double newRange = currentRange * 2;

	double center{ 0.0 };
	if (this->hasValidSelection())
	{
		// Try to center the selection after the zooming, but limit reference to 0
		center = selection_.getCenter();
	}
	else
	{
		// Try to center the current center frame
		center = graph.abscissa.reference + graph.abscissa.positiveLimit / 2;
	}

	graph.abscissa.reference = std::max(center - newRange / 2, 0.0);
	graph.abscissa.positiveLimit *= 2;
	return graph.abscissa.positiveLimit;
}

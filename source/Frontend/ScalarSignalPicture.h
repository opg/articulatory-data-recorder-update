#pragma once
#include "GraphPicture.h"
#include "../Backend/ScalarSignal.h"

/// @brief This class holds functionality to draw a scalar signal.
class ScalarSignalPicture : public GraphPicture
{
public:
	/// @brief Constructs a scalar signal picture object containing a scalar picture
	/// with given or default visualization parameters.
	/// @param parent parent of the constructed scalar signal picture object
	/// @param signal signal containing the scalar data to be drawn
	/// @param backgroundColor_ background color
	/// @param lineColor_ line color
	/// @param linewidth_px_ line width
	/// @param isPartOfStack is part of stack (check documentation of GraphPicture class)
	ScalarSignalPicture(wxWindow* parent, const ScalarSignal& signal,
		wxColour backgroundColor_ = *wxWHITE, wxColour lineColor_ = *wxBLACK,
		int linewidth_px_ = 3, bool isPartOfStack = false);

public:
	const ScalarSignal& signal_;
	double zoomToAll() override;

private:
	/// @brief Draws to the given device context (including graph and signal).
	///
	/// Overrides the base function "draw()" from GraphPicture to manage drawing
	/// for the contact signal picture object. Calls draw() from base class and drawSignal() internally.
	/// @param dc device context for painting (see wxWidgets documentation)
	void draw(wxDC& dc) override;

	/// @brief Draws the signal to the given device context.
	///
	/// Calls labelSignal internally.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawSignal(wxDC& dc);

	/// @brief Labels the drawn signal.
	/// @param dc device context for painting (see wxWidgets documentation)
	void labelSignal(wxDC& dc) const;

	/// @brief Returns the corresponding sample index for a given time instance.
	/// @param time_s time instance
	/// @return sample index
	[[nodiscard]] unsigned getSampleIndex(double time_s) const;
};

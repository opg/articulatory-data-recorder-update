#pragma once
#include <wx/wx.h>

class VCenteredTextCtrl : public wxTextCtrl
{
public:
	VCenteredTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value = wxEmptyString,
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0,
		const wxValidator& validator = wxDefaultValidator,
		const wxString& name = wxTextCtrlNameStr);

	[[nodiscard]] int DoGetBestClientHeight(int width) const override;
	void Update() override;
};
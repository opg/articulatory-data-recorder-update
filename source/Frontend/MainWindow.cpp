#include "MainWindow.h"

#include <chrono>
#include <wx/artprov.h>
#include <wx/aboutdlg.h>
#include <wx/wupdlock.h>

#include "IconsXpm.h"
#include "../Backend/EosBackend.h"
#include "../Backend/FileManager.h"
#include "../Backend/OsloBackend.h"
#include "../Backend/SecondVoiceBackend.h"
#include "../Backend/TongueMouseBackend.h"
#include "../Backend/OpgBackend.h"
#include "../Backend/AudioFile.h"
#include "../Backend/PalateFile.h"
#include "../Backend/AdrFile.h"
#include "../Backend/ExperimentFile.h"

#include "ExperimentDialog.h"



// Menu
static const int IDM_LOAD_DATASET = 1200;
static const int IDM_SAVE_WAV = 1201;
static const int IDM_COM_CONFIG = 1202;
static const int IDM_PALATE_CONFIG = 1203;
static const int IDM_SELECT_HARDWARE_BACKEND = 1204;
static const int IDM_TRACK_SELECTION = 1205;
static const int IDM_NEW_EXPERIMENT = 1206;
static const int IDM_OPEN_EXPERIMENT = 1207;
static const int IDM_SAVE_EXPERIMENT = 1208;
static const int IDM_SAVE_ADR = 1209;
static const int IDM_SAVE_SESSION = 1210;
static const int IDM_LOAD_PALATE = 1211;
static const int IDM_EDIT_EXPERIMENT = 1212;
static const int IDM_SPEC_SETTINGS = 1213;
static const int IDM_SOUND_SETTINGS = 1214;


// Toolbar
static const int ID_TOOLBAR = 1300;
static const int IDB_TOGGLE_LAYOUT = 1301;
static const int IDB_TOGGLE_NOTEBOOK_SPLIT = 1302;
static const int IDB_PLAY_AUDIO = 1303;
static const int IDB_RECORD_DATA = 1304;
static const int IDB_RESET_BUFFERS = 1305;
static const int IDB_REPEAT_AUDIO = 1306;
static const int IDB_STOP_AUDIO = 1307;
static const int IDB_SYNC_AUDIO = 1308;
static const int IDB_PREVIOUS_PROMPT = 1309;
static const int IDB_NEXT_PROMPT = 1310;
static const int IDB_USE_CALIBRATOR = 1311;
static const int IDB_ZOOM_IN = 1312;
static const int IDB_ZOOM_OUT = 1313;
static const int IDB_ZOOM_TO_ALL = 1314;
static const int IDB_ZOOM_TO_SELECTION = 1315;

// Notebook
static const int ID_NOTEBOOK_1 = 1500;

// Pages
static const int IDP_DATAPAGE = 1600;
static const int IDP_PROMPTSPAGE = 1601;
static const int IDP_PROMPTSPAGE_2 = 1602;

// Splitter
static const int IDS_SPLIT_MAIN = 1700;

// Timer
static const int IDT_PLAYBACK_CHECK = 1800;


// ****************************************************************************
// Event Table
// ****************************************************************************
BEGIN_EVENT_TABLE(MainWindow, wxFrame)
EVT_MENU(IDM_LOAD_DATASET, OnLoadDataset)
EVT_MENU(IDM_PALATE_CONFIG, OnPalateConfig)
EVT_MENU(IDM_LOAD_PALATE, OnLoadPalate)
EVT_MENU(IDM_SAVE_ADR, OnSaveAdr)
EVT_MENU(IDM_SAVE_WAV, OnSaveWav)
EVT_MENU(IDM_SAVE_SESSION, OnSaveSession)
EVT_MENU(IDM_COM_CONFIG, OnComConfig)
EVT_MENU(IDM_SELECT_HARDWARE_BACKEND, OnSelectHardwareBackend)
EVT_MENU(IDM_NEW_EXPERIMENT, OnNewExperiment)
EVT_MENU(IDM_OPEN_EXPERIMENT, OnLoadExperiment)
EVT_MENU(IDM_EDIT_EXPERIMENT, OnEditExperiment)
EVT_MENU(IDM_SAVE_EXPERIMENT, OnSaveExperiment)
EVT_MENU(IDM_SOUND_SETTINGS, OnSoundSettings)
EVT_COMMAND(1, INTRO_REACHED, OnIntroReached)
EVT_COMMAND(2, OUTRO_REACHED, OnOutroReached)
EVT_COMMAND(wxID_ANY, PAGE_UPDATE, OnPromptsPageUpdate)
EVT_MENU(IDM_TRACK_SELECTION, OnViewTrackProperties)
EVT_COMMAND(wxID_ANY, SignalPicPropChangedEvent, OnSignalPropChanged)
EVT_MENU(IDM_SPEC_SETTINGS, OnViewSpectrogramSettings)
EVT_COMMAND(wxID_ANY, SpecSettingsChangedEvent, OnSpecSettingsChanged)
EVT_COMMAND(wxID_ANY, AudioSettingsChangedEvent, OnAudioSettingsChanged)
EVT_MENU(wxID_ABOUT, OnAbout)
EVT_MENU(wxID_EXIT, OnClose)
EVT_CLOSE(OnCloseWindow)
EVT_MENU(IDB_TOGGLE_LAYOUT, OnToggleLayout)
EVT_MENU(IDB_TOGGLE_NOTEBOOK_SPLIT, OnNotebookSplit)
EVT_MENU(IDB_RECORD_DATA, OnRecord)
EVT_MENU(IDB_PLAY_AUDIO, OnPlayAudio)
EVT_MENU(IDB_STOP_AUDIO, OnStopAudio)
EVT_MENU(IDB_REPEAT_AUDIO, OnRepeatAudio)
EVT_MENU(IDB_RESET_BUFFERS, OnResetBuffers)
EVT_MENU(IDB_NEXT_PROMPT, OnNextPrompt)
EVT_MENU(IDB_PREVIOUS_PROMPT, OnPreviousPrompt)
EVT_MENU(IDB_USE_CALIBRATOR, OnUseCalibrator)
EVT_MENU(IDB_ZOOM_IN, OnZoom)
EVT_MENU(IDB_ZOOM_OUT, OnZoom)
EVT_MENU(IDB_ZOOM_TO_ALL, OnZoom)
EVT_MENU(IDB_ZOOM_TO_SELECTION, OnZoom)
EVT_MOUSEWHEEL(OnMousewheel)
EVT_SIZE(MainWindow::OnResize)
EVT_TIMER(IDT_PLAYBACK_CHECK, OnCheckPlaybackStatus)
//EVT_MAXIMIZE(OnMaximize)
EVT_COMMAND(wxID_ANY, DataCollectedEvent, OnDataCollected)
EVT_COMMAND(wxID_ANY, SelectionChangedEvent, OnDataSelectionChanged)
END_EVENT_TABLE()

// ****************************************************************************
// ****************************************************************************

MainWindow::MainWindow(wxWindow* parent, wxWindowID id, const wxString& title)
{
	wxFrame::Create(parent, id, title, wxDefaultPosition,
		wxDefaultSize, wxCLOSE_BOX | wxMINIMIZE_BOX |
		wxMAXIMIZE_BOX | wxSYSTEM_MENU | wxCAPTION |
		wxRAISED_BORDER | wxRESIZE_BORDER);

	SetIcon(wxIcon(recorder_xpm));

	// Make sure to init the widgets first and then change backend!
	initWidgets();

	changeBackend(HardwareBackend::Type::EOS_4);

	// Do not set to double buffered or you will get tearing of the selection highlight!
	//this->SetDoubleBuffered(true);

	Maximize();
}

// ****************************************************************************
// Init all widgets
// ****************************************************************************

void MainWindow::initWidgets()
{

	// ****************************************************************
	// Set properties of this window.
	// ****************************************************************

	this->SetSize(20, 20, 1280, 720);

	// ****************************************************************
	// Create the menu.
	// ****************************************************************

	wxMenu* menu = NULL;

	wxMenuBar* menuBar = new wxMenuBar();

	menu = new wxMenu();

	menu->Append(IDM_SAVE_WAV, "Save only WAV file...");

	menu->Append(IDM_SAVE_ADR, "Save only ADR file...");

	menu->Append(IDM_SAVE_SESSION, "Save session (WAV+ADR)...");

	menu->AppendSeparator();
	menu->Append(wxID_EXIT, "Quit");

	menuBar->Append(menu, "File");

	// ****************************************************************


	menu = new wxMenu();
	menu->Append(IDM_NEW_EXPERIMENT, "New experiment...");
	menu->Append(IDM_OPEN_EXPERIMENT, "Load experiment...");
	menu->Append(IDM_EDIT_EXPERIMENT, "Edit experiment...");
	menu->Append(IDM_SAVE_EXPERIMENT, "Save experiment...");
	menuBar->Append(menu, "Experiment");

	// ****************************************************************

	menu = new wxMenu();
	menu->Append(IDM_SELECT_HARDWARE_BACKEND, "Select Hardware Backend...");
	menu->Append(IDM_COM_CONFIG, "COM-Port Settings...");
	//menu->Append(IDM_PALATE_CONFIG, "Palate Settings");
	menu->Append(IDM_LOAD_PALATE, "Load Palate File...");
	menu->Append(IDM_SOUND_SETTINGS, "Sound Settings...");
	menuBar->Append(menu, "Edit");

	// ****************************************************************

	menu = new wxMenu();
	menu->Append(IDM_TRACK_SELECTION, "Signal Properties");
	menu->Append(IDM_SPEC_SETTINGS, "Spectrogram Settings");
	menuBar->Append(menu, "View");

	// ****************************************************************

	menu = new wxMenu();
	menu->Append(wxID_ABOUT, "About");

	menuBar->Append(menu, "Help");

	// ****************************************************************

	this->SetMenuBar(menuBar);


	// ****************************************************************************
	// Generate the toolbar.
	// ****************************************************************************
	toolbar = CreateToolBar(wxTB_DEFAULT_STYLE, ID_TOOLBAR);

	toolbar->AddTool(IDB_RECORD_DATA, "Record data", record_xpm,
		L"Record data", wxITEM_CHECK);
	toolbar->AddTool(IDB_RESET_BUFFERS, "Reset buffers", trashcan_xpm, "Reset buffers");
	toolbar->FindById(IDB_RESET_BUFFERS)->Enable(false);
	toolbar->AddTool(IDB_USE_CALIBRATOR, "Use calibrated values", calibrate_xpm,
		"Use calibrated values (if available)", wxITEM_CHECK);
	toolbar->AddTool(IDB_SYNC_AUDIO, "Sync audio", sync_xpm, "Synchronize audio", wxITEM_CHECK);
	toolbar->FindById(IDB_SYNC_AUDIO)->Enable(false);
	toolbar->AddSeparator();
	/*****************************************************************************/
	toolbar->AddTool(IDB_PLAY_AUDIO, "Play audio", playArrow_xpm, "Play audio");
	toolbar->AddTool(IDB_STOP_AUDIO, "Stop audio playback", stop_xpm, "Stop audio playback");
	toolbar->FindById(IDB_STOP_AUDIO)->Enable(false);
	toolbar->AddTool(IDB_REPEAT_AUDIO, "Repeat audio playback", repeat_xpm, "Repeat audio playback", wxITEM_CHECK);
	toolbar->FindById(IDB_REPEAT_AUDIO)->Toggle(SoundInterface::getInstance().isRepeating());
	toolbar->AddSeparator();
	/*****************************************************************************/
	toolbar->AddTool(IDB_ZOOM_IN, "Zoom in", zoom_in_xpm, "Zoom in");
	toolbar->AddTool(IDB_ZOOM_OUT, "Zoom out", zoom_out_xpm, "Zoom out");
	toolbar->AddTool(IDB_ZOOM_TO_ALL, "Zoom to fit all", compress_horz_xpm, "Zoom to fit entire recording");
	toolbar->AddTool(IDB_ZOOM_TO_SELECTION, "Zoom to selection", expand_horz_xpm, "Zoom to fit selected range");
	toolbar->AddSeparator();
	/*****************************************************************************/
	toolbar->AddTool(IDB_PREVIOUS_PROMPT, "Show previous prompt", previousPrompt_xpm, "Show previous prompt (if not at beginning of experiment)");
	toolbar->AddTool(IDB_NEXT_PROMPT, "Show next prompt", nextPrompt_xpm, "Show next prompt (if not at end of experiment)");
	toolbar->AddStretchableSpace();
	/*****************************************************************************/
	/*****************************************************************************/
	toolbar->AddTool(IDB_TOGGLE_LAYOUT, L"Toggle display mode", dualWindow_xpm,
		L"Toggle display mode", wxITEM_CHECK);
	toolbar->AddTool(IDB_TOGGLE_NOTEBOOK_SPLIT, L"Toggle notebook split", mainWindowSplit_xpm,
		L"Toggle notebook split", wxITEM_CHECK);

	toolbar->Realize();

	// ****************************************************************************
	// Generate the notebook containing the Data and Prompts pages.
	// ****************************************************************************
	notebook = new wxAuiNotebook(this, ID_NOTEBOOK_1, wxDefaultPosition,
		wxDefaultSize, wxAUI_NB_TOP);
	dataPage = createDataPage(notebook, DataPage::Mode::ANALYSIS);
	promptsPage = new StringPromptsPage(notebook, &experimentManager, false, IDP_PROMPTSPAGE);
	notebook->AddPage(static_cast<wxPanel*>(dataPage), L"Data", true);
	notebook->AddPage(static_cast<wxPanel*>(promptsPage), L"Prompts", false);

	// ****************************************************************************
	// Create the prompts window but don't show yet
	// ****************************************************************************
	promptsWindow = new wxFrame(this, wxID_ANY, L"Articulatory Data Recorder",
		wxDefaultPosition, promptsWindow->FromDIP(wxSize(1280, 720)), wxCAPTION | wxRAISED_BORDER | wxMAXIMIZE_BOX | wxRESIZE_BORDER);
	promptsWindow->SetDoubleBuffered(true);
	promptsPageSecondary = new StringPromptsPage(promptsWindow, &experimentManager, true, IDP_PROMPTSPAGE_2);
	promptsWindow->Show(false);

	// ****************************************************************************
	// Accelerator Table
	// ****************************************************************************
	wxAcceleratorEntry entries[6];
	entries[0].Set(wxACCEL_NORMAL, WXK_LEFT, IDB_PREVIOUS_PROMPT);
	entries[1].Set(wxACCEL_NORMAL, WXK_RIGHT, IDB_NEXT_PROMPT);
	entries[2].Set(wxACCEL_CTRL, (int) 'A', IDB_ZOOM_TO_ALL);
	entries[3].Set(wxACCEL_CTRL, (int) 'I', IDB_ZOOM_IN);
	entries[4].Set(wxACCEL_CTRL, (int) 'O', IDB_ZOOM_OUT);
	entries[5].Set(wxACCEL_CTRL, (int) 'N', IDB_ZOOM_TO_SELECTION);
	const wxAcceleratorTable hotKeys(6, entries);
	this->wxWindowBase::SetAcceleratorTable(hotKeys);


	checkPlaybackTimer_ = new wxTimer(this, IDT_PLAYBACK_CHECK);

	updateWidgets();

}

void MainWindow::updateWidgets()
{
	if (promptsPageSecondary != nullptr && promptsWindow->IsShown())
	{
		promptsPageSecondary->update();
		promptsPageSecondary->showPrompt->HideNativeCaret();
	}
	if (promptsPage != nullptr)
	{
		promptsPage->update();
		promptsPage->showPrompt->HideNativeCaret();
	}

	this->Refresh();
	this->Layout();
	dataPage->updateWidgets();
	this->Refresh();
	this->Layout();
}

void MainWindow::changeBackend(HardwareBackend::Type newBackendSelection)
{
	// Reset to null pointer to make sure old backend is properly destroyed before the new one is created
	hardwareBackend_.reset(nullptr);
	// Reset the buffers
	this->resetBuffers();
	// Make the switch
	switch (newBackendSelection)
	{
	case HardwareBackend::Type::EOS:
		hardwareBackend_.reset(new EosBackend(this, Data::getInstance().frameBuffer));
		Data::getInstance().currentBackend = HardwareBackend::Type::EOS;
		break;
	case HardwareBackend::Type::EOS_4:
		hardwareBackend_.reset(new Eos4Backend(this, Data::getInstance().frameBuffer));
		Data::getInstance().currentBackend = HardwareBackend::Type::EOS_4;
		break;
	case HardwareBackend::Type::SECOND_VOICE:
		hardwareBackend_.reset(new SecondVoiceBackend(this, Data::getInstance().frameBuffer));
		Data::getInstance().currentBackend = HardwareBackend::Type::SECOND_VOICE;
		break;
	case HardwareBackend::Type::OSLO:
		hardwareBackend_.reset(new OsloBackend(this, Data::getInstance().frameBuffer));
		Data::getInstance().currentBackend = HardwareBackend::Type::OSLO;
		break;
	case HardwareBackend::Type::TONGUE_MOUSE:
		hardwareBackend_.reset(new TongueMouseBackend(this, Data::getInstance().frameBuffer));
		Data::getInstance().currentBackend = HardwareBackend::Type::TONGUE_MOUSE;
		break;	
	case HardwareBackend::Type::OPG:
		hardwareBackend_.reset(new TongueMouseBackend(this, Data::getInstance().frameBuffer));
		Data::getInstance().currentBackend = HardwareBackend::Type::OPG;
		break;
	default:
		break;
	}

	Data::getInstance().audioBuffer.clear();
	toolbar->EnableTool(IDB_SYNC_AUDIO, hardwareBackend_->syncMethod_ != DataCollector::SyncMethod::NO_SYNC);
	resetSignalPage(mainWindowIsSplit);
}

bool MainWindow::pauseAudio()
{
	auto retVal = SoundInterface::getInstance().pausePlayback();
	toolbar->SetToolNormalBitmap(IDB_PLAY_AUDIO, resume_xpm);
	toolbar->SetToolShortHelp(IDB_PLAY_AUDIO, "Resume audio playback");
	checkPlaybackTimer_->Stop();
	return retVal;
}

bool MainWindow::playAudio()
{
	try
	{
		SoundInterface::Status retVal{ SoundInterface::Status::STOPPED };
		if (dataPage->hasSelection())
		{
			retVal = SoundInterface::getInstance().playAsynchronously(dataPage->getSelection().getStart(), dataPage->getSelection().getEnd());
		}
		else
		{
			retVal = SoundInterface::getInstance().playAsynchronously();
		}
		toolbar->SetToolNormalBitmap(IDB_PLAY_AUDIO, pause_xpm);
		toolbar->SetToolShortHelp(IDB_PLAY_AUDIO, "Pause audio playback");
		toolbar->EnableTool(IDB_STOP_AUDIO, true);

		checkPlaybackTimer_->Start(100);

		return retVal == SoundInterface::Status::ACTIVE;
	}
	catch (const std::runtime_error& e)
	{
		wxMessageBox(e.what(), "Error", wxICON_ERROR);
		return false;
	}	
}

bool MainWindow::queryRangeOnly()
{
	if (dataPage->signalPictures_.front()->hasSelection())
	{
		const int answer = wxMessageBox("Save only the selected range?", "Confirm",
			wxYES_NO, this);

		return answer == wxYES;
	}

	return false;
}

bool MainWindow::saveAdrFile(wxString filename, double start, double end)
{
	try
	{
		AdrFile adrFile(start, end);
		FileManager::writeFile(adrFile, filename.ToStdString());
	}
	catch (std::exception e)
	{
		wxMessageBox(e.what(), "Error while saving ADR file", wxOK | wxICON_ERROR);
		return false;
	}
	return true;	
}

bool MainWindow::saveWavFile(wxString filename, double start, double end)
{
	try
	{
		auto samplingRate = Data::getInstance().audioBuffer.getSamplingRate_Hz();
		int firstSample = static_cast<int>(start * samplingRate);
		int lastSample = static_cast<int>(end * samplingRate);

		AudioFile<double> audioFile;
		audioFile.setBitDepth(16);
		audioFile.setNumChannels(1);
		audioFile.setSampleRate(Data::getInstance().audioBuffer.getSamplingRate_Hz());
		AudioFile<double>::AudioBuffer buffer;
		buffer.resize(1);
		buffer[0] = { Data::getInstance().audioBuffer.begin() + firstSample, Data::getInstance().audioBuffer.begin() + lastSample };
		audioFile.setAudioBuffer(buffer);
		audioFile.save(filename.ToStdString(), AudioFileFormat::Wave);
	}
	catch (std::exception e)
	{
		wxMessageBox(e.what(), "Error while saving WAV file", wxOK | wxICON_ERROR);
		return false;
	}

	return true;	
}

bool MainWindow::stopAudio()
{
	bool retVal{ false };
	retVal = SoundInterface::getInstance().stopPlayback();
	toolbar->SetToolNormalBitmap(IDB_PLAY_AUDIO, playArrow_xpm);
	toolbar->SetToolShortHelp(IDB_PLAY_AUDIO, "Play audio");
	toolbar->EnableTool(IDB_STOP_AUDIO, false);

	return retVal;
}

void MainWindow::OnLoadDataset(wxCommandEvent& event)
{
	// TODO
}

void MainWindow::OnLoadPalate(wxCommandEvent& event)
{
	wxFileDialog
		openPalateFileDialog(this, _("Load palate file"), "", "",
			"palate files (*.palate)|*.palate", wxFD_OPEN | wxFD_FILE_MUST_EXIST);

	if (openPalateFileDialog.ShowModal() == wxID_CANCEL) { return; }

	PalateFile palateFile;
	FileManager::readFile(openPalateFileDialog.GetPath().ToStdString(), palateFile);

	try
	{
		Data::getInstance().setPalate(palateFile.getPalate());
	}
	catch (std::exception& e)
	{
		wxMessageBox(e.what());
	}

}

void MainWindow::OnSaveAdr(wxCommandEvent& event)
{
	bool saveRangeOnly = queryRangeOnly();

	wxFileDialog
		saveAdrFileDialog(this, _("Save ADR file"), "", "",
			"ADR files (*.adr)|*.adr", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

	if (saveAdrFileDialog.ShowModal() == wxID_CANCEL) { return; }
	
	auto numArticulatorySamples = Data::getInstance().frameBuffer.size();
	if (numArticulatorySamples > 0)
	{
		double start = 0.0;
		double end = static_cast<double>(Data::getInstance().frameBuffer.size()) / Data::getInstance().frameBuffer.getSamplingRate();

		if (saveRangeOnly)
		{
			start = dataPage->getSelection().getStart() >= start ? dataPage->getSelection().getStart() : start;
			end = dataPage->getSelection().getEnd() <= end ? dataPage->getSelection().getEnd() : end;
		}

		saveAdrFile(saveAdrFileDialog.GetPath(), start, end);
	}
	else
	{
		wxMessageBox("No articulatory data recorded. Cannot save ADR file!", "No articulatory data", wxOK);
	}
}

void MainWindow::OnSaveWav(wxCommandEvent& event)
{
	bool saveRangeOnly = queryRangeOnly();

	wxFileDialog
		saveWavFileDialog(this, _("Save WAV file"), "", "",
			"WAV files (*.wav)|*.wav", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveWavFileDialog.ShowModal() == wxID_CANCEL) { return; }

	auto numAudioSamples = Data::getInstance().audioBuffer.size();
	if (numAudioSamples > 0)
	{
		const auto samplingRate = Data::getInstance().audioBuffer.getSamplingRate_Hz();
		double start = 0.0;
		double end = static_cast<double>(Data::getInstance().audioBuffer.size()) / samplingRate;

		if (saveRangeOnly)
		{
			start = dataPage->getSelection().getStart() >= start ? dataPage->getSelection().getStart() : start;
			end = dataPage->getSelection().getEnd() <= end ? dataPage->getSelection().getEnd() : end;
		}

		saveWavFile(saveWavFileDialog.GetPath(), start, end);
	}
	else
	{
		wxMessageBox("No audio recorded. Cannot save WAV file!", "No audio", wxOK);
	}
}

void MainWindow::OnSaveSession(wxCommandEvent& event)
{
	bool saveRangeOnly = queryRangeOnly();

	wxFileDialog
		saveSessionFileDialog(this, _("Save session (WAV+ADR)"), "", "",
			"", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveSessionFileDialog.ShowModal() == wxID_CANCEL) { return; }

	wxString baseFileName = saveSessionFileDialog.GetPath();

	wxString wavFileName = baseFileName + ".wav";
	wxString adrFileName = baseFileName + ".adr";
	if (wxFileExists(wavFileName))
	{
		if (wxMessageBox("File " + wavFileName + " already exists and will be overwritten. Proceed?",
			"Confirm overwrite", wxOK | wxCANCEL | wxICON_QUESTION) != wxOK)
		{
			wxMessageBox("Saving session canceled. No files were written.", "Save canceled", wxOK | wxICON_EXCLAMATION);
			return;
		}
	}

	if (wxFileExists(adrFileName))
	{
		if (wxMessageBox("File " + adrFileName + " already exists and will be overwritten. Proceed?",
			"Confirm overwrite", wxOK | wxCANCEL | wxICON_QUESTION) != wxOK)
		{
			wxMessageBox("Saving session canceled. No files were written.", "Save canceled", wxOK | wxICON_EXCLAMATION);
			return;
		}
	}


	// Save WAV file
	auto numAudioSamples = Data::getInstance().audioBuffer.size();
	if (numAudioSamples > 0)
	{
		auto samplingRate = Data::getInstance().audioBuffer.getSamplingRate_Hz();
		double start = 0.0;
		double end = static_cast<double>(numAudioSamples) / samplingRate;

		if (saveRangeOnly)
		{
			double selectionStart = dataPage->getSelection().getStart();
			double selectionEnd = dataPage->getSelection().getEnd();

			if (selectionStart >= start && selectionStart <= end)
			{
				start = selectionStart;
			}

			if (selectionEnd <= end && selectionEnd >= start)
			{
				end = selectionEnd;
			}
		}
		saveWavFile(wavFileName, start, end);
	}
	else
	{
		wxMessageBox("No audio recorded. Cannot save WAV file!", "No audio", wxOK);
	}
	

	// Save ADR file
	auto numArticulatorySamples = Data::getInstance().frameBuffer.size();
	if (numArticulatorySamples > 0)
	{
		double start = 0.0;
		double end = static_cast<double>(numArticulatorySamples) / Data::getInstance().frameBuffer.getSamplingRate();
		if (saveRangeOnly)
		{
			double selectionStart = dataPage->getSelection().getStart();
			double selectionEnd = dataPage->getSelection().getEnd();

			if (selectionStart >= start && selectionStart <= end)
			{
				start = selectionStart;
			}

			if (selectionEnd <= end && selectionEnd >= start)
			{
				end = selectionEnd;
			}
		}
		saveAdrFile(adrFileName, start, end);
	}
	else
	{
		wxMessageBox("No articulatory data recorded. Cannot save ADR file!", "No articulatory data", wxOK);
	}
	
}

void MainWindow::OnPalateConfig(wxCommandEvent& event)
{
	// TODO
}

void MainWindow::OnRecord(wxCommandEvent& event)
{
	if (event.IsChecked())
	{
		if (this->hardwareBackend_->hasAudio)
		{
			try
			{
				// Start recording in detached mode
				SoundInterface::getInstance().detachRecordingBuffer();
				SoundInterface::getInstance().recordAsynchronously();
				// Give the recording device some setup time
				wxMilliSleep(100);
				// Attach the buffer to store the recorded samples
				SoundInterface::getInstance().attachRecordingBuffer();
				// Finally start the hardware data collection
				hardwareBackend_->run(true);
			}
			catch (const std::runtime_error& e)
			{
				hardwareBackend_->run(false);
				resetBuffers();
				experimentManager.reset();
				toolbar->ToggleTool(event.GetId(), false);
				wxMessageBox(e.what(), "Error", wxICON_ERROR);					
				return;
			}				
		}

		
		if (hardwareBackend_->run(true))
		{
			dataPage->mode = DataPage::Mode::RECORDING;
			toolbar->EnableTool(IDB_RESET_BUFFERS, true);
		}
		else
		{
			SoundInterface::getInstance().stopRecording();
			wxLogError("Failed to start recording! Check serial port settings.");
			toolbar->ToggleTool(event.GetId(), false);
		}
	}
	else
	{
		hardwareBackend_->run(false);
		wxMilliSleep(100);
		SoundInterface::getInstance().stopRecording();
		dataPage->mode = DataPage::Mode::ANALYSIS;
		if (hardwareBackend_->syncMethod_ != DataCollector::SyncMethod::NO_SYNC && toolbar->GetToolState(IDB_SYNC_AUDIO))
		{
			synchronizeAudio();
		}		
	}
	updateWidgets();

}

void MainWindow::OnPlayAudio(wxCommandEvent& event)
{
	if (hardwareBackend_->IsRunning() || SoundInterface::getInstance().getRecordingStatus() == SoundInterface::Status::ACTIVE)
	{
		return;
	}

	if (SoundInterface::getInstance().getPlaybackStatus() == SoundInterface::Status::ACTIVE)
	{
		pauseAudio();
	}
	else
	{
		playAudio();
	}
}

void MainWindow::OnStopAudio(wxCommandEvent& event)
{
	stopAudio();
}

void MainWindow::OnRepeatAudio(wxCommandEvent& event)
{
	SoundInterface::getInstance().isRepeating(!SoundInterface::getInstance().isRepeating());
}

void MainWindow::OnResetBuffers(wxCommandEvent& event)
{
	const int answer = wxMessageBox("Do you really want to reset the recording buffers? All recorded data will be lost!",
		"Confirm", wxYES_NO | wxNO_DEFAULT | wxICON_EXCLAMATION, this);
	if (answer == wxYES)
	{
		resetBuffers();
		experimentManager.reset();
		updateWidgets();
	}
}

void MainWindow::OnSelectHardwareBackend(wxCommandEvent& event)
{
	wxArrayString choices;

	int currentSelectionIndex{ 0 }, i{ 0 };
	for (const auto& [type, label] : HardwareBackend::type2string)
	{
		choices.Add(label);
		if (Data::getInstance().currentBackend == type)
		{
			currentSelectionIndex = i;
		}
		++i;
	}

	const int selected = wxGetSingleChoiceIndex(wxT("Select the connected hardware version"),
		wxT("Backend selection"), choices, this,
		wxDefaultCoord, wxDefaultCoord, true, 200, 150, currentSelectionIndex);

	if (selected != -1)
	{
		changeBackend(HardwareBackend::string2type[choices[selected].ToStdString()]);
	}
}

void MainWindow::OnSoundSettings(wxCommandEvent& event)
{
	auto* dlg = new SoundSettingsDialog(this, wxID_ANY, "Change Sound Settings", &SoundInterface::getInstance());
	dlg->Show();
}

void MainWindow::OnUseCalibrator(wxCommandEvent& event)
{
	for (auto& signal : Data::getInstance().frameBuffer.getScalarSignals())
	{
		// Try to (de)activate calibrator, will have no effect if signal cannot be calibrated
		signal.useCalibrator(!signal.isCalibrated());
	}
	resetSignalPage(mainWindowIsSplit);
}

void MainWindow::OnViewTrackProperties(wxCommandEvent& event)
{
	if (signalPicturePropertiesDialog != nullptr)
	{
		signalPicturePropertiesDialog->Destroy();
		signalPicturePropertiesDialog = nullptr;
	}
	signalPicturePropertiesDialog = new SignalPicturePropertiesDialog(dataPage, dataPage->signalProperties_);
	signalPicturePropertiesDialog->Show();
}

void MainWindow::OnSignalPropChanged(wxCommandEvent& event)
{
	updateWidgets();
}

void MainWindow::OnViewSpectrogramSettings(wxCommandEvent& event)
{
	if (spectrogramPictureSettingsDialog != nullptr)
	{
		spectrogramPictureSettingsDialog->Close();
		spectrogramPictureSettingsDialog = nullptr;
	}
	spectrogramPictureSettingsDialog = new SpectrogramPictureSettingsDialog(this, wxID_ANY, "Spectrogram settings", *dataPage->specPicture);
	spectrogramPictureSettingsDialog->CenterOnParent();
	spectrogramPictureSettingsDialog->Show();
}

void MainWindow::OnSpecSettingsChanged(wxCommandEvent& event)
{
	updateWidgets();
}

void MainWindow::OnAudioSettingsChanged(wxCommandEvent& event)
{
	if (spectrogramPictureSettingsDialog != nullptr)
	{
		spectrogramPictureSettingsDialog->updateWidgets();
	}	
}

void MainWindow::OnComConfig(wxCommandEvent& event)
{
	serialPortDialog = new SerialPortDialog(this, wxID_ANY, "Edit COM-Port configuration.", hardwareBackend_->getSerialPortSettings());
	serialPortDialog->CenterOnParent();

	if (serialPortDialog->ShowModal() == wxID_OK) {
		if (hardwareBackend_->setSerialPortSettings(serialPortDialog->getSerialPortSettings())) {

		}
		else {
			wxLogError("Failed to change COM port settings!");
		}
	}
}

void MainWindow::OnNewExperiment(wxCommandEvent& event)
{
	auto* experimentDialog = new ExperimentDialog(this);
	experimentDialog->CenterOnParent();

	if (experimentDialog->ShowModal() == wxID_OK) {
		if (!experimentDialog->getExperiment().inventory.empty())
		{
			prepareExperiment(experimentDialog->getExperiment());
		}
		else
		{
			wxMessageBox(L"Inventory of experiment is empty!", L"Error", wxICON_ERROR);
		}
	}
}

void MainWindow::OnLoadExperiment(wxCommandEvent& event)
{
	wxFileDialog openFileDialog(this, _("Open Experiment File"), "", "", "Experiment files (*.exp)|*.exp", wxFD_OPEN | wxFD_FILE_MUST_EXIST);

	if (openFileDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}
	ExperimentFile file;
	FileManager::readFile(openFileDialog.GetPath().ToStdString(), file);

	auto* experimentDialog = new ExperimentDialog(this, file.getExperiment());

	experimentDialog->CenterOnParent();

	if (experimentDialog->ShowModal() == wxID_OK) {
		if (!experimentDialog->getExperiment().inventory.empty())
		{
			prepareExperiment(experimentDialog->getExperiment());
		}
		else
		{
			wxMessageBox(L"Inventory of experiment is empty!", L"Error", wxICON_ERROR);
		}
	}
}

void MainWindow::OnEditExperiment(wxCommandEvent& event)
{
	auto* experimentDialog = new ExperimentDialog(this, experimentManager.getExperiment());

	experimentDialog->CenterOnParent();

	if (experimentDialog->ShowModal() == wxID_OK)
	{
		if (!experimentDialog->getExperiment().inventory.empty())
		{
			prepareExperiment(experimentDialog->getExperiment());
		}
		else
		{
			wxMessageBox(L"Inventory of experiment is empty!", L"Error", wxICON_ERROR);
		}
	}
}

void MainWindow::OnSaveExperiment(wxCommandEvent& event)
{
	wxFileDialog saveFileDialog(this, _("Save Experiment File"), "", "", "Experiment files (*.exp)|*.exp", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

	if (saveFileDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}
	if (!experimentManager.getExperiment().inventory.empty())
	{
		auto file = ExperimentFile(experimentManager.getExperiment());
		FileManager::writeFile(file, saveFileDialog.GetPath().ToStdString());
	}
	else
	{
		wxMessageBox(L"Inventory of experiment is empty!", L"Error", wxICON_ERROR);
	}
}


// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************
// ****************************************************************************


// ****************************************************************************
// Handle the change of the data page when hardware backend is changed.
// ****************************************************************************

void MainWindow::resetSignalPage(bool mainWindowIsSplit)
{
	bool signalStatusDialogWasShown{ false };
	if (signalPicturePropertiesDialog != nullptr)
	{
		if (signalPicturePropertiesDialog->IsShown())
		{
			signalStatusDialogWasShown = true;
		}
		else
		{
			signalStatusDialogWasShown = false;
		}
	}

	if (mainWindowIsSplit) {
		const auto panelHeight = dataPage->GetSize().GetHeight();
		promptsPage->Destroy();
		promptsPage = nullptr;
		dataPage->Destroy();
		dataPage = nullptr;
		splitter->Destroy();
		splitter = nullptr;
		splitter = new wxSplitterWindow(this, IDS_SPLIT_MAIN, wxDefaultPosition, wxDefaultSize, wxSP_LIVE_UPDATE | wxSP_HORIZONTAL);
		splitter->SetDoubleBuffered(true);
		splitter->Hide();
		if (SoundInterface::getInstance().getRecordingStatus()==SoundInterface::Status::ACTIVE) {
			dataPage = createDataPage(splitter, DataPage::Mode::RECORDING);
		}
		else {
			dataPage = createDataPage(splitter, DataPage::Mode::ANALYSIS);
		}
		promptsPage = new StringPromptsPage(splitter, &experimentManager, false, IDP_PROMPTSPAGE);
		splitter->SplitHorizontally(dataPage, promptsPage);
		promptsPage->SetMinSize(wxSize(-1, minimalPaneSize));
		splitter->SetMinimumPaneSize(promptsPage->GetMinHeight());
		splitter->SetSashPosition(panelHeight);
		splitter->Show();
		this->Layout();
	}
	else {
		notebook->Hide();
		notebook->RemovePage(0);
		dataPage->Destroy();
		dataPage = nullptr;
		if (SoundInterface::getInstance().getRecordingStatus() == SoundInterface::Status::ACTIVE) {
			dataPage = createDataPage(notebook, DataPage::Mode::RECORDING);
		}
		else {
			dataPage = createDataPage(notebook, DataPage::Mode::ANALYSIS);
		}
		notebook->InsertPage(0, static_cast<wxPanel*>(dataPage), L"Data", true);
		notebook->Show();
	}
	if (!this->hardwareBackend_->hasAudio)
	{
		dataPage->signalProperties_.at("Audio").isShown = false;
		dataPage->signalProperties_.at("Spectrogram").isShown = false;
		if (spectrogramPictureSettingsDialog != nullptr)
		{
			spectrogramPictureSettingsDialog->Hide();
		}
	}
	// signalPicturePropertiesDialog will be destroyed at this point (as child of dataPage). Explicitly set it to nullptr here!
	signalPicturePropertiesDialog = nullptr;
	if (signalStatusDialogWasShown)
	{
		signalPicturePropertiesDialog = new SignalPicturePropertiesDialog(dataPage, dataPage->signalProperties_);
		signalPicturePropertiesDialog->Show();
	}

	updateWidgets();
}

void MainWindow::prepareExperiment(Experiment<std::string> experiment)
{
	experimentManager.setExperiment(experiment);
	updateWidgets();
}

// ****************************************************************************
// Handle the creation of a new data page.
// ****************************************************************************

DataPage* MainWindow::createDataPage(wxWindow* parent, DataPage::Mode workingMode)
{
	return new DataPage(Data::getInstance().frameBuffer, Data::getInstance().audioBuffer, parent, workingMode, IDP_DATAPAGE);
}

void MainWindow::resetBuffers()
{
	this->stopAudio();
	SoundInterface::getInstance().stopRecording();
	SoundInterface::getInstance().resetRecording();
	if (hardwareBackend_ != nullptr)
	{
		hardwareBackend_->reset();
	}
	this->toolbar->ToggleTool(IDB_RECORD_DATA, false);
	this->toolbar->EnableTool(IDB_RESET_BUFFERS, false);

	dataPage->reset();
}

void MainWindow::setNextPrompt()
{
	if (!experimentManager.getQueue().empty())
	{
		experimentManager.next();
		updateWidgets();
	}
}

void MainWindow::setPreviousPrompt()
{
	if (!experimentManager.getQueue().empty())
	{
		experimentManager.previous();
		updateWidgets();
	}
}

void MainWindow::synchronizeAudio()
{
	switch(hardwareBackend_->syncMethod_)
	{
	case DataCollector::SyncMethod::NO_SYNC:
		wxMessageBox("Hardware backend does not support synchronization!", "No sync support");
		break;
	case DataCollector::SyncMethod::BEEP_SYNC:
		{
			auto beepSync = BeepSynchronizer(Data::getInstance().audioBuffer, hardwareBackend_->syncParams_);
			auto syncSample = beepSync.getSyncSample();
			auto beepStart_s = static_cast<double>(syncSample) / Data::getInstance().audioBuffer.getSamplingRate_Hz();
			auto beepEnd_s = beepStart_s + beepSync.getBeepLength_s();
			dataPage->setSelection({ beepStart_s, beepEnd_s });
			updateWidgets();
			const auto oldView = dataPage->getView();
			dataPage->zoomToSelection();
			dataPage->zoomOut();
			const int answer = wxMessageBox("Found the beep in the highlighted segment. Do you want to sync audio to the beginning of the beep?", 
				"Confirm sync", wxYES_NO, this);
			if (answer == wxYES)
			{
				beepSync.sync();
			}
			dataPage->clearSelection();
			dataPage->setView(oldView);
			break;
		}
	default: 
		throw std::runtime_error("[MainWindow::synchronizeAudio()] Unknown synchronization method!");
	}
	toolbar->EnableTool(IDB_SYNC_AUDIO, true);
	toolbar->ToggleTool(IDB_SYNC_AUDIO, true);
	updateWidgets();
}


// ****************************************************************************
// Handle the information on the software.
// ****************************************************************************

void MainWindow::OnAbout(wxCommandEvent& event)
{
	wxAboutDialogInfo info;
	info.SetName(wxT("Articulatory Data Recorder"));
	info.SetVersion(wxT("0.1"));
	info.SetDescription(wxT("Articulatory Data Recorder is a free and open-source PC software to record articulatory data with pseudo-palates manufactured at the IAS."));
	info.SetCopyright(wxT("(C) 2020 TU Dresden, IAS"));
	info.SetLicence(wxT("wxWidgets: https://www.wxwidgets.org/about/licence/ \n Icons: https://www.apache.org/licenses/LICENSE-2.0.html"));
	info.AddDeveloper(wxT("Simon Stone"));
	info.AddDeveloper(wxT("Alexander Wilbrandt"));
	wxAboutBox(info);
}

void MainWindow::OnMousewheel(wxMouseEvent& event)
{
	// Control + mouse wheel zooms in or out
	if (wxGetKeyState(WXK_CONTROL))
	{
		if(event.GetWheelRotation() > 0)
		{
			dataPage->zoomIn();
		}
		else if (event. GetWheelRotation() < 0)
		{
			dataPage->zoomOut();
		}
	}
	updateWidgets();
	event.Skip();
}

void MainWindow::OnZoom(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case IDB_ZOOM_IN:
			dataPage->zoomIn();
			break;
		case IDB_ZOOM_OUT:
			dataPage->zoomOut();
			break;
		case IDB_ZOOM_TO_ALL:
			dataPage->zoomToAll();
			break;
		case IDB_ZOOM_TO_SELECTION:
			dataPage->zoomToSelection();
			break;
		default:
			wxMessageBox("We fell through! AAAAAAAaaaah!");
	}
	updateWidgets();
}

void MainWindow::OnNextPrompt(wxCommandEvent& event)
{
	setNextPrompt();

}

void MainWindow::OnPreviousPrompt(wxCommandEvent& event)
{
	setPreviousPrompt();
}

void MainWindow::OnPromptsPageUpdate(wxCommandEvent& event)
{
	updateWidgets();
}

// ****************************************************************************
// Handle the program exiting.
// ****************************************************************************

void MainWindow::OnClose(wxCommandEvent& event)
{
	Close(true);
}

// ****************************************************************************
// ****************************************************************************

void MainWindow::OnCloseWindow(wxCloseEvent&)
{
	if (wxMessageBox("Are you sure you want to quit?", "Unsaved changes will be discarded!", wxYES_NO, this) == wxYES)
	{
		hardwareBackend_->run(false);
		SoundInterface::getInstance().stopRecording();
		promptsPageSecondary->fireEvents = false;
		this->Destroy();
		exit(0);
	}
}

// ****************************************************************************
// Manage window resizing.
// ****************************************************************************

void MainWindow::OnResize(wxSizeEvent& event)
{
	if (mainWindowIsSplit)
	{
		const auto panelHeight = this->GetSize().GetHeight();
		splitter->SetSashPosition(panelHeight - minimalPaneSize);
		this->Layout();
	}
	else
	{
		this->Layout();
	}
	event.Skip();
}

void MainWindow::OnIntroReached(wxCommandEvent& event)
{
	if (!mainWindowIsSplit && notebook->GetSelection() == 0)
	{
		wxMessageBox("You are at the beginning of the experiment!", "Start!", wxCENTRE, this);
	}
}

void MainWindow::OnOutroReached(wxCommandEvent& event)
{
	if (!mainWindowIsSplit && notebook->GetSelection() == 0)
	{
		wxMessageBox("You are at the end of the experiment!", "End!", wxCENTRE, this);
	}
	else {
		this->Layout();
	}
}

void MainWindow::OnCheckPlaybackStatus(wxTimerEvent& event)
{
	if (SoundInterface::getInstance().getPlaybackStatus() == SoundInterface::Status::STOPPED)
	{
		checkPlaybackTimer_->Stop();
		stopAudio();		
	}
}

void MainWindow::OnDataCollected(wxCommandEvent& event)
{
	updateWidgets();
}

void MainWindow::OnDataSelectionChanged(wxCommandEvent& event)
{
	stopAudio();
}


// ****************************************************************************
// Manage monitor mode toggeling.
// ****************************************************************************

void MainWindow::OnToggleLayout(wxCommandEvent& event)
{
	promptsWindow->Show(!promptsWindow->IsShown());
	updateWidgets();
}

// ****************************************************************************
// Manage notebook splitting.
// ****************************************************************************

void MainWindow::OnNotebookSplit(wxCommandEvent& event)
{
	std::map<std::string, GraphPicture::Properties> properties;
	for (const auto& [signalName, signalProperties] : dataPage->signalProperties_)
	{
		properties[signalName] = signalProperties;
	}
	bool signalStatusDialogWasShown{ false };
	if (signalPicturePropertiesDialog != nullptr)
	{
		if (signalPicturePropertiesDialog->IsShown())
		{
			signalStatusDialogWasShown = true;
		}
		else
		{
			signalStatusDialogWasShown = false;
		}
	}

	if (this->toolbar->GetToolState(IDB_TOGGLE_NOTEBOOK_SPLIT)) {
		const auto panelHeight = dataPage->GetSize().GetHeight();
		this->notebook->Destroy();
		notebook = nullptr;
		splitter = new wxSplitterWindow(this, IDS_SPLIT_MAIN, wxDefaultPosition, wxDefaultSize, wxSP_LIVE_UPDATE | wxSP_HORIZONTAL);
		splitter->SetDoubleBuffered(true);
		splitter->Hide();
		if (SoundInterface::getInstance().getRecordingStatus() == SoundInterface::Status::ACTIVE) {
			dataPage = createDataPage(splitter, DataPage::Mode::RECORDING);
		}
		else {
			dataPage = createDataPage(splitter, DataPage::Mode::ANALYSIS);
		}
		promptsPage = new StringPromptsPage(splitter, &experimentManager, false, IDP_PROMPTSPAGE);
		splitter->SplitHorizontally(dataPage, promptsPage);
		promptsPage->SetMinSize(wxSize(-1, minimalPaneSize));
		splitter->SetMinimumPaneSize(promptsPage->GetMinHeight());
		splitter->SetSashPosition(panelHeight);
		splitter->Show();
		mainWindowIsSplit = true;
	}
	else
	{
		this->splitter->Destroy();
		splitter = nullptr;
		notebook = new wxAuiNotebook(this, ID_NOTEBOOK_1, wxDefaultPosition,
			wxDefaultSize, wxAUI_NB_TOP);
		notebook->Hide();
		if (SoundInterface::getInstance().getRecordingStatus() == SoundInterface::Status::ACTIVE) {
			dataPage = createDataPage(notebook, DataPage::Mode::RECORDING);
		}
		else {
			dataPage = createDataPage(notebook, DataPage::Mode::ANALYSIS);
		}
		promptsPage = new StringPromptsPage(notebook, &experimentManager, false, IDP_PROMPTSPAGE);
		notebook->AddPage(static_cast<wxPanel*>(dataPage), L"Data", true);
		notebook->AddPage(static_cast<wxPanel*>(promptsPage), L"Prompts", false);
		notebook->Show();
		mainWindowIsSplit = false;
	}

	for (const auto& [signalName, signalProperties] : properties)
	{
		dataPage->signalProperties_[signalName] = signalProperties;
	}
	signalPicturePropertiesDialog = new SignalPicturePropertiesDialog(dataPage, dataPage->signalProperties_);

	if (signalStatusDialogWasShown)
	{
		signalPicturePropertiesDialog->Show();
	}
	updateWidgets();
}

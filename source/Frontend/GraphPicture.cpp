#include "GraphPicture.h"

static const int IDM_SELECTION_START = 16001;
static const int IDM_SELECTION_END = 16002;
static const int IDM_SELECTION_CLEAR = 16003;

// Custom events
wxDEFINE_EVENT(SelectionChangedEvent, wxCommandEvent);
wxDEFINE_EVENT(ScrollRequestEvent, wxCommandEvent);

// ****************************************************************************
// The event table.
// ****************************************************************************
BEGIN_EVENT_TABLE(GraphPicture, BasicPicture)
EVT_MENU(IDM_SELECTION_START, GraphPicture::OnSetSelectionStart)
EVT_MENU(IDM_SELECTION_END, GraphPicture::OnSetSelectionEnd)
EVT_MENU(IDM_SELECTION_CLEAR, GraphPicture::OnSelectionClear)
EVT_LEFT_DOWN(GraphPicture::OnLeftDown)
EVT_LEFT_UP(GraphPicture::OnLeftUp)
//EVT_MOTION(GraphPicture::OnDrag)
EVT_LEAVE_WINDOW(GraphPicture::OnMouseLeaveOrEnter)
EVT_ENTER_WINDOW(GraphPicture::OnMouseLeaveOrEnter)
EVT_TIMER(IDT_DRAG_UPDATE, GraphPicture::OnDragTimer)
END_EVENT_TABLE()

GraphPicture::GraphPicture(wxWindow* parent, Properties properties, bool isPartOfStack) :
	BasicPicture(parent), graph(this), isPartOfStack_(isPartOfStack)
{
	this->setProperties(properties);
	
	graph.setMargins({ 0, 20, 0, 60 });

	rightClickMenu.Append(IDM_SELECTION_START, "Set selection start");
	rightClickMenu.Append(IDM_SELECTION_END, "Set selection end");
	rightClickMenu.Append(IDM_SELECTION_CLEAR, "Clear selection");
}

GraphPicture::GraphPicture(wxWindow* parent, bool isPartOfStack)
	: GraphPicture(parent, Properties(), isPartOfStack)
{
}

GraphPicture::GraphPicture(wxWindow* parent, wxColour backgroundColor, wxColour lineColor, int linewidth_px, bool isPartOfStack) :
	GraphPicture(parent, {-1, backgroundColor, lineColor, linewidth_px}, isPartOfStack)
{	
}

GraphPicture::GraphPicture(wxWindow* parent, Graph::LinearDomain abscissaProperties, Graph::LinearDomain ordinateProperties,
	wxColour backgroundColor, wxColour lineColor, int linewidth_px, bool isPartOfStack) :
	GraphPicture(parent, backgroundColor, lineColor, linewidth_px, isPartOfStack)
{
	graph.initAbscissa(abscissaProperties);

	graph.initLinearOrdinate(ordinateProperties);
}


void GraphPicture::paintAbscissa(bool abscissaIsVisible)
{
	abscissaIsPainted_ = abscissaIsVisible;
	const auto newBottomMargin = abscissaIsPainted_ ? 40 : 0;
	graph.setMargin(newBottomMargin, Graph::MarginType::BOTTOM);
}

bool GraphPicture::isAbscissaPainted()
{
	return abscissaIsPainted_;
}

int GraphPicture::getPictureID() const
{
	return pictureID_;
}

wxColour GraphPicture::getBackgroundColor() const
{
	return backgroundBrush.GetColour();
}

wxColour GraphPicture::getLineColor() const
{
	return linePen.GetColour();
}

int GraphPicture::getLineWidth() const
{
	return linePen.GetWidth();
}

bool GraphPicture::isNormalized() const
{
	return isNormalized_;
}

GraphPicture::Properties GraphPicture::getProperties() const
{
	return { pictureID_, backgroundBrush.GetColour(), linePen.GetColour(), linePen.GetWidth(), isNormalized_, this->IsShown()};
}

bool GraphPicture::hasSelection() const
{
	return selection_.hasStart() || selection_.hasEnd();
}

bool GraphPicture::hasValidSelection() const
{
	return selection_.isValid();
}

double GraphPicture::getSelectionStart() const
{
	return selection_.getStart();
}

double GraphPicture::getSelectionEnd() const
{
	return selection_.getEnd();
}

wxColour GraphPicture::getHighlightColor() const
{
	return highlightColor;
}

double GraphPicture::getHighlightAlpha() const
{
	return highlightAlpha;
}

void GraphPicture::isNormalized(bool doNormalize)
{
	isNormalized_ = doNormalize;
	//TODO: Change y-axis of graph to range -1 / +1 if signal is normalized or back to the original range if signal is not normalized (anymore)
}

void GraphPicture::setPictureID(const int newPictureID)
{
	pictureID_ = newPictureID;
}

void GraphPicture::setBackgroundColor(const wxColour& backgroundColor)
{
	backgroundBrush.SetColour(backgroundColor);
	highlightedBrush.SetColour(alpha2rgb(highlightAlpha, backgroundBrush.GetColour(), highlightColor));
}

void GraphPicture::setLineColor(const wxColour& lineColor)
{
	linePen.SetColour(lineColor);
	highlightedLinePen.SetColour(alpha2rgb(highlightAlpha, linePen.GetColour(), highlightColor));
}

void GraphPicture::setLineWidth(const int lineWidth_px)
{
	linePen.SetWidth(lineWidth_px);
	highlightedLinePen.SetWidth(lineWidth_px);
}

void GraphPicture::setIsShown(const bool isShown)
{
	this->Show(isShown);
}

void GraphPicture::setProperties(const Properties& newProperties)
{
	setBackgroundColor(newProperties.backgroundColor);
	setLineColor(newProperties.lineColor);
	setLineWidth(newProperties.linewidth_px);
	isNormalized(newProperties.doNormalize);
	setPictureID(newProperties.pictureID);
	setIsShown(newProperties.isShown);
}

void GraphPicture::setSelection(const Selection& newSelection)
{
	selection_ = newSelection;
}

void GraphPicture::setSelectionStart(int i)
{
	selection_.setStart(graph.getAbsXValue(i));
}

void GraphPicture::setSelectionEnd(int i)
{
	selection_.setEnd(graph.getAbsXValue(i));
}

void GraphPicture::dragSelection(int i)
{
	selection_.dragActiveMarker(graph.getAbsXValue(i));
}

void GraphPicture::clearSelection()
{
	selection_.clear();
}

void GraphPicture::setHighlightColor(wxColour newColor)
{
	highlightColor = newColor;
}

void GraphPicture::setHighlightAlpha(double newAlpha)
{
	highlightAlpha = newAlpha;
}

void GraphPicture::draw(wxDC& dc)
{
	drawGraphBackground(dc);
	if (this->hasValidSelection())
	{
		drawSelectionHighlight(dc);
	}
	drawGraphAxes(dc);
	if (this->hasSelection())
	{
		drawSelectionMarks(dc);
	}
}

void GraphPicture::drawGraph(wxDC& dc)
{
	drawGraphBackground(dc);
	drawGraphAxes(dc);
}

void GraphPicture::drawGraphBackground(wxDC& dc)
{
	int graphX, graphY, graphW, graphH;

	graph.getDimensions(graphX, graphY, graphW, graphH);

	dc.SetBackground(*wxWHITE_BRUSH);
	dc.Clear();

	dc.SetPen(*wxTRANSPARENT_PEN);
	dc.SetBrush(backgroundBrush);

	dc.DrawRectangle(graphX, graphY, graphW, graphH);
}

void GraphPicture::drawGraphAxes(wxDC& dc)
{
	dc.SetPen(linePen);

	if (abscissaIsPainted_)
	{
		graph.paintAbscissa(dc);
	}
	else
	{
		graph.paintAbscissa(dc, false);
	}
	graph.paintOrdinate(dc);

	dc.SetPen(graphPen);
	dc.SetBrush(*wxTRANSPARENT_BRUSH);

	int graphX, graphY, graphW, graphH;
	graph.getDimensions(graphX, graphY, graphW, graphH);
	dc.DrawRectangle(graphX, graphY, graphW, graphH);
}

void GraphPicture::drawSelection(wxDC& dc)
{
	// Draw selection highlight first so marks show up on top of the highlight instead of behind it
	if (selection_.isValid())
	{
		drawSelectionHighlight(dc);
	}

	if (selection_.hasStart())
	{
		drawSelectionMark(dc, graph.getXPos(selection_.getStart()), Selection::Marker::Start);
	}

	if (selection_.hasEnd())
	{
		drawSelectionMark(dc, graph.getXPos(selection_.getEnd()), Selection::Marker::End);
	}
}

void GraphPicture::drawSelectionMark(wxDC& dc, int xPos, GraphPicture::Selection::Marker markerType)
{
	if (xPos < graph.getXPos(graph.abscissa.reference + graph.abscissa.negativeLimit)) { return; }
	if (xPos > graph.getXPos(graph.abscissa.reference + graph.abscissa.positiveLimit)) { return; }

	int x, y, w, h;
	graph.getDimensions(x, y, w, h);

	wxPoint start, end;
	start.x = xPos;
	start.y = y;

	end.x = start.x;
	end.y = y + h;

	dc.SetPen(wxPen(wxColour(0, 48, 94), 2, wxPENSTYLE_LONG_DASH));
	dc.DrawLine(start, end);

	// Draw triangular markers at the top and bottom of the mark
	const auto markerSize = h / 10;
	dc.SetPen(wxPen(wxColour(0, 48, 94), 2, wxPENSTYLE_SOLID));
	dc.SetBrush(wxBrush(wxColour(0, 48, 94)));

	int direction = 1;
	if (markerType == Selection::Marker::End) { direction = -1; }

	// Top
	wxPoint points[] = { start, start + wxPoint(direction * markerSize, 0), start + wxPoint(0, markerSize) };
	dc.DrawPolygon(3, points);

	// Bottom
	points[0] = end;
	points[1] = end + wxPoint(direction * markerSize, 0);
	points[2] = end + wxPoint(0, -markerSize);
	dc.DrawPolygon(3, points);
}

void GraphPicture::drawSelectionMarks(wxDC& dc)
{
	if (selection_.hasStart())
	{
		drawSelectionMark(dc, graph.getXPos(selection_.getStart()), Selection::Marker::Start);
	}

	if (selection_.hasEnd())
	{
		drawSelectionMark(dc, graph.getXPos(selection_.getEnd()), Selection::Marker::End);
	}
}

void GraphPicture::drawSelectionHighlight(wxDC& dc)
{
	auto xPos_start = graph.getXPos(selection_.getStart());
	auto xPos_end = graph.getXPos(selection_.getEnd());
	if (xPos_start < graph.getXPos(graph.abscissa.reference + graph.abscissa.negativeLimit))
	{
		if (xPos_end < graph.getXPos(graph.abscissa.reference + graph.abscissa.negativeLimit)) { return; }
		xPos_start = graph.getXPos(graph.abscissa.reference + graph.abscissa.negativeLimit);
	}


	if (xPos_end > graph.getXPos(graph.abscissa.reference + graph.abscissa.positiveLimit))
	{
		if (xPos_start > graph.getXPos(graph.abscissa.reference + graph.abscissa.positiveLimit)) { return; }
		xPos_end = graph.getXPos(graph.abscissa.reference + graph.abscissa.positiveLimit);
	}

	int x, y, w, h;
	graph.getDimensions(x, y, w, h);

	const wxPoint start(xPos_start, y);
	const wxSize size(xPos_end - xPos_start, h);
	
	dc.SetBrush(highlightedBrush);
	dc.SetPen(*wxTRANSPARENT_PEN);
	dc.DrawRectangle(start, size);
}

wxColour GraphPicture::alpha2rgb(double alpha, wxColour background, wxColour foreground)
{
	return wxColour(
		(1 - alpha) * background.Red() + alpha * foreground.Red(),
		(1 - alpha) * background.Green() + alpha * foreground.Green(),
		(1 - alpha) * background.Blue() + alpha * foreground.Blue()
	);
}

void GraphPicture::OnSetSelectionStart(wxCommandEvent& WXUNUSED(event))
{
	if (isPartOfStack_)
	{
		Selection* newSelection = new Selection(selection_);
		newSelection->setStart(graph.getAbsXValue(menuX));
		postSelectionChangedEvent(newSelection);
	}
	else
	{
		setSelectionStart(menuX);
	}


}

void GraphPicture::OnSetSelectionEnd(wxCommandEvent& WXUNUSED(event))
{
	if (isPartOfStack_)
	{
		Selection* newSelection = new Selection(selection_);
		newSelection->setEnd(graph.getAbsXValue(menuX));
		postSelectionChangedEvent(newSelection);
	}
	else
	{
		setSelectionEnd(menuX);
	}
}

void GraphPicture::OnSelectionClear(wxCommandEvent& WXUNUSED(event))
{
	if (isPartOfStack_)
	{
		Selection* newSelection = new Selection(selection_);
		newSelection->clear();
		postSelectionChangedEvent(newSelection);
	}
	else
	{
		clearSelection();
	}
}

void GraphPicture::OnLeftDown(wxMouseEvent& event)
{
	if (isPartOfStack_)
	{
		Selection* newSelection = new Selection(selection_);
		newSelection->setStart(graph.getAbsXValue(event.GetX()));
		postSelectionChangedEvent(newSelection);
	}
	else
	{
		setSelectionStart(event.GetX());
	}
	dragTimer_->Start(dragTimerPeriod_);
}

void GraphPicture::OnDrag(wxMouseEvent& event)
{
	if (event.Dragging())
	{
		if (isPartOfStack_)
		{
			Selection* newSelection = new Selection(selection_);
			newSelection->dragActiveMarker(graph.getAbsXValue(event.GetX()));
			postSelectionChangedEvent(newSelection);
		}
		else
		{
			selection_.dragActiveMarker(graph.getAbsXValue(event.GetX()));
		}
	}
}

void GraphPicture::OnDragTimer(wxTimerEvent& event)
{
	if (!wxGetMouseState().ButtonIsDown(wxMOUSE_BTN_LEFT))  // i.e., we are not longer dragging
	{
		dragTimer_->Stop();
		return;
	}

	const wxPoint pt = wxGetMousePosition();
	const int mouseX = pt.x - this->GetScreenPosition().x;

	if (isPartOfStack_)
	{

		Selection* newSelection = new Selection(selection_);
		newSelection->dragActiveMarker(graph.getAbsXValue(mouseX));
		postSelectionChangedEvent(newSelection);
	}
	else
	{
		selection_.dragActiveMarker(graph.getAbsXValue(mouseX));
	}

		// If we are dragging near an edge of the graph, scroll the graph
	if (abs(graph.getXPos(graph.abscissa.reference + graph.abscissa.positiveLimit) - mouseX) < 20)
	{
		if (isPartOfStack_)
		{
			double* newReference = new double(std::min(graph.abscissa.reference + 0.1, graph.abscissa.positiveLimitMax));
			postScrollRequestEvent(newReference);
		}
		else
		{
			graph.abscissa.reference = std::min(graph.abscissa.reference + 0.1, graph.abscissa.positiveLimitMax);
		}
	}
	if (abs(graph.getXPos(graph.abscissa.reference + graph.abscissa.negativeLimit) - mouseX) < 20)
	{
		if (isPartOfStack_)
		{
			double* newReference = new double(std::max(graph.abscissa.reference - 0.1, graph.abscissa.negativeLimitMax));
			postScrollRequestEvent(newReference);
		}
		else
		{
			graph.abscissa.reference = std::max(graph.abscissa.reference - 0.1, graph.abscissa.negativeLimitMax);
		}
	}
}

void GraphPicture::OnLeftUp(wxMouseEvent& event)
{
	if (isPartOfStack_)
	{
		Selection* newSelection = new Selection(selection_);
		newSelection->dragActiveMarker(graph.getAbsXValue(event.GetX()));
		// If start and end are the same (no mouse drag between mouse button down and up),
		// then the assumed intent is to clear the current selection
		if (newSelection->getStart() == newSelection->getEnd())
		{
			newSelection->clear();
		}
		postSelectionChangedEvent(newSelection);
	}
	else
	{
		selection_.dragActiveMarker(graph.getAbsXValue(event.GetX()));
	}
	dragTimer_->Stop();
}

void GraphPicture::OnMouseLeaveOrEnter(wxMouseEvent& event)
{
	if (event.Dragging())
	{
		if (isPartOfStack_)
		{

			Selection* newSelection = new Selection(selection_);
			newSelection->dragActiveMarker(graph.getAbsXValue(event.GetX()));
			postSelectionChangedEvent(newSelection);
		}
		else
		{
			selection_.dragActiveMarker(graph.getAbsXValue(event.GetX()));
		}

		if (event.Leaving())
		{
			dragTimer_->Stop();
		}
		if (event.Entering())
		{
			dragTimer_->Start();
		}
	}
}

void GraphPicture::postScrollRequestEvent(double* newReference)
{
	wxCommandEvent event(ScrollRequestEvent);
	event.SetClientData(newReference);
	wxPostEvent(this, event);
}

void GraphPicture::postSelectionChangedEvent(Selection* newSelection)
{
	wxCommandEvent event(SelectionChangedEvent);
	event.SetClientData(newSelection);
	wxPostEvent(this, event);
}

double GraphPicture::Selection::getStart() const
{
	return start_;
}

double GraphPicture::Selection::getCenter() const
{
	if (!isValid())
	{
		throw std::runtime_error("Center of an invalid selection requested!");
	}
	return start_ + (end_ - start_) / 2;
}

double GraphPicture::Selection::getEnd() const
{
	return end_;
}

double GraphPicture::Selection::getLength() const
{
	if (!isValid())
	{
		throw std::runtime_error("Length of invalid selection requested!");
	}
	return (end_ - start_);
}

GraphPicture::Selection::Marker GraphPicture::Selection::getActive() const
{
	return active_;
}

bool GraphPicture::Selection::hasStart() const
{
	return hasStart_;
}

bool GraphPicture::Selection::hasEnd() const
{
	return hasEnd_;
}

void GraphPicture::Selection::setStart(const double newStartPos, bool startDragging)
{
	start_ = std::max(newStartPos, 0.0);
	hasStart_ = true;
	if (startDragging)
	{
		setActive(Selection::Marker::End);
	}
}

void GraphPicture::Selection::dragActiveMarker(double newMarkerPos)
{
	switch (active_)
	{
	case Marker::Start:
		if (newMarkerPos > end_)
		{
			setStart(getEnd(), false);
			setActive(Marker::End);
			setEnd(newMarkerPos);
		}
		else
		{
			setStart(newMarkerPos, false);
		}
		break;
	case Marker::End:
		if (newMarkerPos < start_)
		{
			setEnd(getStart());
			setActive(Marker::Start);
			setStart(newMarkerPos, false);
		}
		else
		{
			setEnd(newMarkerPos);
		}
		break;
	case Marker::None:
		break;
	default:;
	}
}

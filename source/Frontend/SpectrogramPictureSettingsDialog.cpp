#include "SpectrogramPictureSettingsDialog.h"
#include <wx/collpane.h>
#include <wx/gbsizer.h>

static const int IDS_FRAMELENGTH = 30000;
static const int IDS_OVERLAP = 30001;
static const int IDS_CUTOFF = 30002;
static const int IDS_WINDOW_TYPE = 30003;
static const int IDS_NFFT = 30004;
static const int IDS_DYNAMIC_RANGE = 30005;
static const int IDS_COLORMAP_TYPE = 30006;
static const int IDS_LOWER_BOUND_FREQ = 30007;
static const int IDS_UPPER_BOUND_FREQ = 30008;
static const int IDS_PREEMPHASIS_COEFF = 30009;


wxDEFINE_EVENT(SpecSettingsChangedEvent, wxCommandEvent);


BEGIN_EVENT_TABLE(SpectrogramPictureSettingsDialog, wxDialog)
EVT_SPINCTRLDOUBLE(IDS_DYNAMIC_RANGE, OnChangeDynamicRange)
EVT_SPINCTRLDOUBLE(IDS_FRAMELENGTH, OnChangeFrameLength)
EVT_SPINCTRLDOUBLE(IDS_OVERLAP, OnChangeOverlap)
EVT_SPINCTRLDOUBLE(IDS_LOWER_BOUND_FREQ, OnChangeLowerFreq)
EVT_SPINCTRLDOUBLE(IDS_UPPER_BOUND_FREQ, OnChangeUpperFreq)
EVT_SPINCTRLDOUBLE(IDS_CUTOFF, OnChangeRelativeCutoff)
EVT_SPINCTRLDOUBLE(IDS_PREEMPHASIS_COEFF, OnChangePreEmphasisCoeff)
EVT_COMBOBOX(IDS_WINDOW_TYPE, OnChangeWindowType)
EVT_SPINCTRLDOUBLE(IDS_NFFT, OnChangeFftLength)
EVT_COMBOBOX(IDS_COLORMAP_TYPE, OnChangeColorMap)
EVT_TEXT_ENTER(wxID_ANY, OnEnter)
END_EVENT_TABLE()

SpectrogramPictureSettingsDialog::SpectrogramPictureSettingsDialog(wxWindow* parent, wxWindowID id, const wxString& title, SpectrogramPicture& specPicture) : wxDialog(
	parent, id, title, wxDefaultPosition, wxDefaultSize), ptrSpecPic_(&specPicture)
{
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	initWidgets();
}


void SpectrogramPictureSettingsDialog::initWidgets()
{
	auto* topLevelSizer = new wxGridBagSizer(0, 0);

	auto* dynamicRangeText = new wxStaticText(this, wxID_ANY, "Dynamic Range [dB]:", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	auto* toolTip = new wxToolTip(L"Sets the dynamic range for the currently visible part of the spectrogram.");
	toolTip->SetAutoPop(10000);
	dynamicRangeText->SetToolTip(toolTip);
	dynamicRangeText->SetMinSize(FromDIP(wxSize(minimumWidgetSize, -1)));
	selectDynamicRange = new wxSpinCtrlDouble(this, IDS_DYNAMIC_RANGE, L"", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 1, 180, ptrSpecPic_->settings.dynamicRange_dB_, 5.0);
	topLevelSizer->Add(dynamicRangeText, wxGBPosition(0,0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	topLevelSizer->Add(selectDynamicRange, wxGBPosition(0, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);
	
	auto* frameLengthText = new wxStaticText(this, wxID_ANY, "Framelength [ms]:", wxDefaultPosition, wxDefaultSize);
	frameLengthText->SetToolTip(L"Sets the length of the processed frames. Very low values in combination with a high FFT length may harm the performance of the software.");
	selectFrameLength_ = new wxSpinCtrlDouble(this, IDS_FRAMELENGTH, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 1, 200, static_cast<double>(ptrSpecPic_->spectrogram.getFrameLength(true)) / ptrSpecPic_->spectrogram.getSamplingRate_Hz() * 1000, 1.0);
	topLevelSizer->Add(frameLengthText, wxGBPosition(1, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	topLevelSizer->Add(selectFrameLength_, wxGBPosition(1, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);

	auto* frequencyRangeText = new wxStaticText(this, wxID_ANY, "Display Range [Hz]:", wxDefaultPosition, wxDefaultSize);
	frequencyRangeText->SetToolTip(L"Sets a lower and upper bound only for visualization. This does not influence the calculation of the spectrogram itself.");
	selectLowerBoundFreqRange = new wxSpinCtrlDouble(this, IDS_LOWER_BOUND_FREQ, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 0, static_cast<double>(ptrSpecPic_->spectrogram.getSamplingRate_Hz() * ptrSpecPic_->spectrogram.getCutoff() - 10), ptrSpecPic_->settings.frequencyRange.first, 10.0);
	auto* frequencyRangeSeparator = new wxStaticText(this, wxID_ANY, "-", wxDefaultPosition, wxDefaultSize);
	selectUpperBoundFreqRange = new wxSpinCtrlDouble(this, IDS_UPPER_BOUND_FREQ, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 0, static_cast<double>(ptrSpecPic_->spectrogram.getSamplingRate_Hz() * ptrSpecPic_->spectrogram.getCutoff()), ptrSpecPic_->settings.frequencyRange.second, 10.0);
	topLevelSizer->Add(frequencyRangeText, wxGBPosition(2, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	topLevelSizer->Add(selectLowerBoundFreqRange, wxGBPosition(2, 1), wxDefaultSpan, wxALL, 5);
	topLevelSizer->Add(frequencyRangeSeparator, wxGBPosition(2, 2), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	topLevelSizer->Add(selectUpperBoundFreqRange, wxGBPosition(2, 3), wxDefaultSpan, wxALL, 5);

	auto* advancedSettingsPanel = new wxCollapsiblePane(this, wxID_ANY, "Advanced Settings:");
	advancedSettingsPanel->GetControlWidget()->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	wxWindow* win = advancedSettingsPanel->GetPane();

	auto* advancedSettingsSizer = new wxGridBagSizer(0, 0);

	auto* overlapText = new wxStaticText(win, wxID_ANY, "Overlap:", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	overlapText->SetToolTip(L"Sets the overlap of concurrent frames. High values may harm the performance of the software.");
	overlapText->SetMinSize(FromDIP(wxSize(minimumWidgetSize, -1)));
	selectOverlap = new wxSpinCtrlDouble(win, IDS_OVERLAP, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 0.0, 0.95, static_cast<double>(ptrSpecPic_->spectrogram.getOverlap(true)) / ptrSpecPic_->spectrogram.getFrameLength(true), 0.05);
	advancedSettingsSizer->Add(overlapText, wxGBPosition(0, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	advancedSettingsSizer->Add(selectOverlap, wxGBPosition(0, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);

	
	auto* relativeCutoffText = new wxStaticText(win, wxID_ANY, "Relative Cutoff:", wxDefaultPosition, wxDefaultSize);
	relativeCutoffText->SetToolTip(L"Value times sampling rate of the audio signal marks the upper frequency bound. Values higher than 0.5 will lead to mirroring of the caculated spectrogram.");
	selectRelativeCutoff = new wxSpinCtrlDouble(win, IDS_CUTOFF, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 0.1, 1.0, ptrSpecPic_->spectrogram.getCutoff(), 0.1);
	advancedSettingsSizer->Add(relativeCutoffText, wxGBPosition(1, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	advancedSettingsSizer->Add(selectRelativeCutoff, wxGBPosition(1, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);

	auto* preEmphasisCoeffText = new wxStaticText(win, wxID_ANY, "Preemphasis Coefficient:", wxDefaultPosition, wxDefaultSize);
	preEmphasisCoeffText->SetToolTip(L"Coefficient of single tap FIR preemphasis filter (y[n] = y[n] - value * y[n-1].");
	selectPreEmphasisCoeff = new wxSpinCtrlDouble(win, IDS_PREEMPHASIS_COEFF, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 0.0, 1.0, ptrSpecPic_->spectrogram.getPreEmphasisCoeff(), 0.01);
	advancedSettingsSizer->Add(preEmphasisCoeffText, wxGBPosition(2, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	advancedSettingsSizer->Add(selectPreEmphasisCoeff, wxGBPosition(2, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);

	auto* windowTypeText = new wxStaticText(win, wxID_ANY, "Window Type:", wxDefaultPosition, wxDefaultSize);
	windowTypeText->SetToolTip(L"Sets the type of the window which the frames are multiplied with as part of preprocessing for the calculation of the spectrogram.");
	wxArrayString windowChoices;
	int i = 0;
	for (const auto& type : Spectrogram::getSupportedWindowTypes())
	{
		windowChoices.Add(dsp::window::type2string(type));
		i++;
	}
	windowComboBox = new wxComboBox(win, IDS_WINDOW_TYPE, L"", wxDefaultPosition, wxDefaultSize, windowChoices, wxCB_READONLY);
	windowComboBox->SetValue(dsp::window::type2string(ptrSpecPic_->spectrogram.getWindowType()));
	advancedSettingsSizer->Add(windowTypeText, wxGBPosition(3, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	advancedSettingsSizer->Add(windowComboBox, wxGBPosition(3, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);

	auto* fftLength = new wxStaticText(win, wxID_ANY, "Length of FFT:", wxDefaultPosition, wxDefaultSize);
	fftLength->SetToolTip(L"Sets the length of the FFT. Very high values in combination with a short frame length may harm the performance of the software.");
	selectFftLength = new wxSpinCtrlDouble(win, IDS_NFFT, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 1, 8192, ptrSpecPic_->spectrogram.getNfft(), 1);
	advancedSettingsSizer->Add(fftLength, wxGBPosition(4, 0), wxDefaultSpan, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT | wxALL, 5);
	advancedSettingsSizer->Add(selectFftLength, wxGBPosition(4, 1), wxGBSpan(1, 3), wxEXPAND | wxALL, 5);

	win->SetSizer(advancedSettingsSizer);
	topLevelSizer->Add(advancedSettingsPanel, wxGBPosition(3, 0), wxGBSpan(1, 4), wxEXPAND);


	// Create and add the standard button sizer with separator line
	topLevelSizer->Add(CreateSeparatedButtonSizer(wxCLOSE), wxGBPosition(4, 0), wxGBSpan(1, 4), wxEXPAND | wxALL, 5);

	topLevelSizer->AddGrowableCol(1);
	advancedSettingsSizer->AddGrowableCol(1);

	this->SetSizerAndFit(topLevelSizer);

}

void SpectrogramPictureSettingsDialog::updateWidgets()
{
	selectUpperBoundFreqRange->SetRange(0, static_cast<double>(ptrSpecPic_->spectrogram.getSamplingRate_Hz() * ptrSpecPic_->spectrogram.getCutoff()));
	selectUpperBoundFreqRange->SetValue(ptrSpecPic_->settings.frequencyRange.second);
	this->Refresh();
}

void SpectrogramPictureSettingsDialog::OnChangeDynamicRange(wxSpinDoubleEvent& event)
{
	ptrSpecPic_->settings.setDynamicRange_dB(selectDynamicRange->GetValue());
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeFrameLength(wxSpinDoubleEvent& event)
{
	const int newFrameLength = static_cast<double>(selectFrameLength_->GetValue()) / 1000.0 * static_cast<double>(ptrSpecPic_->spectrogram.getSamplingRate_Hz());
	ptrSpecPic_->spectrogram.setFrameLength(newFrameLength);
	if (static_cast<int>(selectFftLength->GetValue()) != ptrSpecPic_->spectrogram.getNfft())
	{
		selectFftLength->SetValue(ptrSpecPic_->spectrogram.getNfft());
	}
	ptrSpecPic_->spectrogram.setOverlap(selectOverlap->GetValue() * ptrSpecPic_->spectrogram.getFrameLength(true));
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeOverlap(wxSpinDoubleEvent& event)
{
	ptrSpecPic_->spectrogram.setOverlap(selectOverlap->GetValue() * ptrSpecPic_->spectrogram.getFrameLength(true));
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeLowerFreq(wxSpinDoubleEvent& event)
{
	if (selectLowerBoundFreqRange->GetValue() > selectUpperBoundFreqRange->GetValue() - 10)
	{	
		selectLowerBoundFreqRange->SetValue(selectUpperBoundFreqRange->GetValue() - 10);
	}
	ptrSpecPic_->graph.linearOrdinate.positiveLimitMin = selectLowerBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.negativeLimit = selectLowerBoundFreqRange->GetValue();
	ptrSpecPic_->settings.frequencyRange.first = selectLowerBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.scaleDivision = ptrSpecPic_->graph.linearOrdinate.positiveLimitMax - static_cast<int>((ptrSpecPic_->graph.linearOrdinate.positiveLimitMax - ptrSpecPic_->graph.linearOrdinate.positiveLimitMin) / 2);
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeUpperFreq(wxSpinDoubleEvent& event)
{
	if ((selectRelativeCutoff->GetValue() * static_cast<double>(ptrSpecPic_->spectrogram.getSamplingRate_Hz())) > selectUpperBoundFreqRange->GetValue())
	{
		if (selectUpperBoundFreqRange->GetValue() < selectLowerBoundFreqRange->GetValue() + 10)
		{
			selectUpperBoundFreqRange->SetValue(selectLowerBoundFreqRange->GetValue() + 10);
		}
	}
	else
	{
		selectUpperBoundFreqRange->SetValue(selectRelativeCutoff->GetValue() * static_cast<double>(ptrSpecPic_->spectrogram.getSamplingRate_Hz()));
	}
	ptrSpecPic_->graph.linearOrdinate.positiveLimit = selectUpperBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.positiveLimitMax = selectUpperBoundFreqRange->GetValue();
	ptrSpecPic_->settings.frequencyRange.second = selectUpperBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.scaleDivision = ptrSpecPic_->graph.linearOrdinate.positiveLimitMin + static_cast<int>((ptrSpecPic_->graph.linearOrdinate.positiveLimitMax - ptrSpecPic_->graph.linearOrdinate.positiveLimitMin) / 2);
	updateWidgets();
	postSpecSettingsChangedEvent();	
}

void SpectrogramPictureSettingsDialog::OnChangeRelativeCutoff(wxSpinDoubleEvent& event)
{
	ptrSpecPic_->spectrogram.setCutoff(selectRelativeCutoff->GetValue());
	if (selectRelativeCutoff->GetValue() * ptrSpecPic_->spectrogram.getSamplingRate_Hz() < selectUpperBoundFreqRange->GetValue())
	{
		selectUpperBoundFreqRange->SetValue(selectRelativeCutoff->GetValue() * ptrSpecPic_->spectrogram.getSamplingRate_Hz());
		if (selectLowerBoundFreqRange->GetValue() > selectUpperBoundFreqRange->GetValue() - 10)
		{
			selectLowerBoundFreqRange->SetValue(0);
		}
	}
	ptrSpecPic_->graph.linearOrdinate.positiveLimit = selectUpperBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.positiveLimitMax = selectUpperBoundFreqRange->GetValue();
	ptrSpecPic_->settings.frequencyRange.second = selectUpperBoundFreqRange->GetValue();

	ptrSpecPic_->graph.linearOrdinate.positiveLimitMin = selectLowerBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.negativeLimit = selectLowerBoundFreqRange->GetValue();
	ptrSpecPic_->settings.frequencyRange.first = selectLowerBoundFreqRange->GetValue();
	ptrSpecPic_->graph.linearOrdinate.scaleDivision = ptrSpecPic_->graph.linearOrdinate.positiveLimitMax - static_cast<int>((ptrSpecPic_->graph.linearOrdinate.positiveLimitMax - ptrSpecPic_->graph.linearOrdinate.positiveLimitMin) / 2);
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangePreEmphasisCoeff(wxSpinDoubleEvent& event)
{
	ptrSpecPic_->spectrogram.setPreEmphasisCoeff(selectPreEmphasisCoeff->GetValue());
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeWindowType(wxCommandEvent& event)
{
	ptrSpecPic_->spectrogram.setWindowType(dsp::window::string2type(windowComboBox->GetValue().ToStdString()));
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeFftLength(wxSpinDoubleEvent& event)
{
	if (selectFftLength->GetValue() < static_cast<int>(pow(2, dsp::nextpow2(ptrSpecPic_->spectrogram.getFrameLength(true)))))
	{
		selectFftLength->SetValue(static_cast<int>(pow(2, dsp::nextpow2(ptrSpecPic_->spectrogram.getFrameLength(true)))));
		wxMessageBox(L"Length of FFT must be a power of 2 and higher than the selected framelength [samples]!", L"Unable to set nFFT!");
	}
	else if (selectFftLength->GetValue() > ptrSpecPic_->spectrogram.getNfft())
	{
		selectFftLength->SetValue(static_cast<int>(pow(2, dsp::nextpow2(ptrSpecPic_->spectrogram.getNfft()) + 1)));
		ptrSpecPic_->spectrogram.setNfft(selectFftLength->GetValue());
	}
	else
	{
		selectFftLength->SetValue(static_cast<int>(pow(2, dsp::nextpow2(event.GetValue()) - 1)));
		ptrSpecPic_->spectrogram.setNfft(selectFftLength->GetValue());
	}
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnChangeColorMap(wxCommandEvent& event)
{
	ptrSpecPic_->setColorMap(ColorMap::string2colorMapType[colorMapComboBox->GetValue().ToStdString()]);
	updateWidgets();
	postSpecSettingsChangedEvent();
}

void SpectrogramPictureSettingsDialog::OnEnter(wxCommandEvent& event)
{
	this->GetParent()->SetFocus();
	this->SetFocus();
}


void SpectrogramPictureSettingsDialog::postSpecSettingsChangedEvent()
{
	const wxCommandEvent event(SpecSettingsChangedEvent);
	wxPostEvent(this->GetParent(), event);
}

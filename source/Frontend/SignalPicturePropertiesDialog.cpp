#include <wx/spinctrl.h>
#include "SignalPicturePropertiesDialog.h"

#include "SerialPortDialog.h"


wxDEFINE_EVENT(SignalPicPropChangedEvent, wxCommandEvent);


static const int wxID_NORMALIZE = 10000;
static const int wxID_SHOW = 10001;

// ****************************************************************************
// Event Table
// ****************************************************************************
BEGIN_EVENT_TABLE(SignalPicturePropertiesDialog, wxDialog)
EVT_CHECKBOX(wxID_ANY, SignalPicturePropertiesDialog::OnCheckboxEvent)
EVT_SPINCTRLDOUBLE(wxID_ANY, SignalPicturePropertiesDialog::OnChangeTrackLinewidth)
EVT_COLOURPICKER_CHANGED(wxID_ANY, OnChangeColor)
EVT_TEXT_ENTER(wxID_ANY, OnEnter)
END_EVENT_TABLE()

SignalPicturePropertiesDialog::SignalPicturePropertiesDialog(wxWindow* parent, std::map<std::string, GraphPicture::Properties>& properties) :
wxDialog(parent, wxID_ANY, "Track properties", wxDefaultPosition, wxDefaultSize), ptrSignalProperties_(&properties)
{
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	initWidgets();	
}

void SignalPicturePropertiesDialog::initWidgets()
{
	auto* topLevelSizer = new wxBoxSizer(wxVERTICAL);
	auto* scrolledDialog = new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxVSCROLL);
	auto* scrolledSizer = new wxBoxSizer(wxVERTICAL);
			
	int trackNumber = 0;
	int position = 0;

	while (trackNumber++ <= static_cast<int>(ptrSignalProperties_->size()))
	{
		for (const auto& [signalName, signalProperties] : *ptrSignalProperties_)
		{
			if (signalProperties.pictureID == position)
			{
				auto* trackProperties = new wxStaticBoxSizer(wxHORIZONTAL, scrolledDialog, signalName);
				auto* propertyCategory = new wxBoxSizer(wxHORIZONTAL);
				propertyCategory->Add(new wxStaticText(scrolledDialog, wxID_ANY, L"Show Track:"), wxSizerFlags().Border(wxALL).CenterVertical());
				auto* showTrack = new wxCheckBox(scrolledDialog, signalProperties.pictureID, L"", wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
				showTrack->SetValue(signalProperties.isShown);
				showTrack->SetClientData(const_cast<int*>(&wxID_SHOW));
				
				propertyCategory->Add(showTrack, wxSizerFlags().Border(wxALL).CenterVertical());
				if (signalName != "Spectrogram" && signalName != "Contacts")
				{
					propertyCategory->Add(new wxStaticText(scrolledDialog, wxID_ANY, L"Normalize:"), wxSizerFlags().Border(wxALL).CenterVertical());
					auto* doNormalize = new wxCheckBox(scrolledDialog, signalProperties.pictureID, L"", wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);
					doNormalize->SetClientData(const_cast<int*>(&wxID_NORMALIZE));
					doNormalize->SetValue(signalProperties.doNormalize);
					
					propertyCategory->Add(doNormalize, wxSizerFlags().Border(wxALL).CenterVertical());
					propertyCategory->Add(new wxStaticText(scrolledDialog, wxID_ANY, L"Linewidth:"), wxSizerFlags().Border(wxALL).CenterVertical());
					auto* selectLineWidth = new wxSpinCtrlDouble(scrolledDialog, signalProperties.pictureID, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL | wxTE_PROCESS_ENTER, 1.0, 5.0, signalProperties.linewidth_px);
					// The following should not be necessary but is the only known 
					// way to enforce a proper width of the spin control under wxWidgets 3.1.0
 					wxSize size = selectLineWidth->GetSizeFromTextSize(selectLineWidth->GetTextExtent("1"));
					selectLineWidth->SetMinSize(size);
					selectLineWidth->SetSize(size);
					propertyCategory->Add(selectLineWidth, wxSizerFlags().Border(wxALL).CenterVertical());
					propertyCategory->Add(new wxStaticText(scrolledDialog, wxID_ANY, L"Linecolor:"), wxSizerFlags().Border(wxALL).CenterVertical());
					propertyCategory->Add(new wxColourPickerCtrl(scrolledDialog, signalProperties.pictureID, signalProperties.lineColor, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER), wxSizerFlags().Border(wxALL).CenterVertical());
					propertyCategory->Add(new wxStaticText(scrolledDialog, wxID_ANY, L"Backgroundcolor:"), wxSizerFlags().Border(wxALL).CenterVertical());
					propertyCategory->Add(new wxColourPickerCtrl(scrolledDialog, signalProperties.pictureID + 1000, signalProperties.backgroundColor, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER), wxSizerFlags().Border(wxALL).CenterVertical());
				}

				trackProperties->Add(propertyCategory, wxSizerFlags(1).Expand());
				scrolledSizer->Add(trackProperties, wxSizerFlags(1).Border(wxALL).Expand());
			
				position++;
			}
		}
	}
	scrolledDialog->ShowScrollbars(wxSHOW_SB_NEVER, wxSHOW_SB_ALWAYS);
	scrolledDialog->SetScrollbars(0, scrolledDialog->GetChildren().GetFirst()->GetData()->GetSize().GetHeight(), 0, ptrSignalProperties_->size());
	scrolledDialog->SetSizerAndFit(scrolledSizer);

	topLevelSizer->Add(scrolledDialog, wxSizerFlags(1).Expand());
	// Create and add the standard button sizer with separator line
	topLevelSizer->Add(SignalPicturePropertiesDialog::CreateSeparatedButtonSizer(wxCLOSE), wxSizerFlags().Expand().Border(wxALL));
	
	this->SetSizerAndFit(topLevelSizer);
	this->SetSize(FromDIP(wxSize(-1, 400)));
}

void SignalPicturePropertiesDialog::OnCheckboxEvent(wxCommandEvent& event)
{
	const auto eventID = event.GetId();
	auto* trigger = dynamic_cast<wxCheckBox*>(event.GetEventObject());
	auto* id = static_cast<int*>(trigger->GetClientData());
	for (const auto& [signalName, signalProperties] : *ptrSignalProperties_)
	{
		if (signalProperties.pictureID == eventID)
		{
			switch (*id)
			{
			case wxID_SHOW:
				ptrSignalProperties_->at(signalName).isShown = event.GetSelection() == 1 ? true : false;
				break;
			case wxID_NORMALIZE:
				ptrSignalProperties_->at(signalName).doNormalize = event.GetSelection() == 1 ? true : false;
				break;
			default:
				throw std::runtime_error("Unknown checkbox ID!");
			}
		}
	}
	postSignalPropChangedEvent();
}

void SignalPicturePropertiesDialog::OnChangeTrackLinewidth(wxSpinDoubleEvent& event)
{
	const auto eventID = event.GetId();
	for (const auto& [signalName, signalProperties] : *ptrSignalProperties_)
	{
		if (signalProperties.pictureID == eventID)
		{
			ptrSignalProperties_->at(signalName).linewidth_px = event.GetValue();
		}
	}
	postSignalPropChangedEvent();
}

void SignalPicturePropertiesDialog::OnChangeColor(wxColourPickerEvent& event)
{
	const auto eventID = event.GetId();
	for (const auto& [signalName, signalProperties] : *ptrSignalProperties_)
	{
		if (signalProperties.pictureID == eventID)
		{
			ptrSignalProperties_->at(signalName).lineColor = event.GetColour();
		}
		if (signalProperties.pictureID == eventID - 1000)
		{
			ptrSignalProperties_->at(signalName).backgroundColor = event.GetColour();
		}
	}
	postSignalPropChangedEvent();
}

void SignalPicturePropertiesDialog::OnEnter(wxCommandEvent& event)
{
	this->GetParent()->SetFocus();
	this->SetFocus();
}


void SignalPicturePropertiesDialog::postSignalPropChangedEvent()
{
	const wxCommandEvent event(SignalPicPropChangedEvent);
	wxPostEvent(this->GetParent(), event);
}

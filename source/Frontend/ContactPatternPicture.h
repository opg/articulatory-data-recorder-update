#pragma once
#include "BasicPicture.h"
#include <vector>

/// @brief This class holds functionality to draw a single contact pattern.
///
/// Open contacts will be drawn white and closed contacts will be drawn black
/// by default. Visualization can be changed.
class ContactPatternPicture :
    virtual public BasicPicture
{
public:
	/// @brief Constructs the contact pattern picture object with given or default
	/// colors and empty contact matrix.
	/// @param parent parent of the constructed pattern picture object
	/// @param open color in which open contacts are drawn
	/// @param closed color in which closed contacts are drawn
	/// @param outline color in which the outlines are drawn
	ContactPatternPicture(wxWindow* parent, 
		wxColour open = *wxWHITE, wxColour closed = *wxBLACK, wxColour outline = *wxBLACK);

	/// @brief Constructs the contact pattern picture object with given or default
	/// colors and given contact matrix.
	/// @param parent parent of the constructed pattern picture object
	/// @param contactMatrix matrix containing contact information
	/// @param open color in which open contacts are drawn
	/// @param closed color in which closed contacts are drawn
	/// @param outline color in which the outlines are drawn
	ContactPatternPicture(wxWindow* parent ,const std::vector<std::vector<double>>& contactMatrix, 
		wxColour open = *wxWHITE, wxColour closed = *wxBLACK, wxColour outline = *wxBLACK);

	/// @brief Draws to the given device context.
	///
	/// Overrides the base function "draw()" from BasicPicture to manage drawing
	/// for the contact pattern picture object.
	/// @param dc device context for painting (see wxWidgets documentation)
	void draw(wxDC& dc) override;
	
	/// @brief Draws the contact pattern.
	///
	/// Passing the contact matrix is strictly speaking not necessary here. It is helpful for the classes owning a contact pattern picture object though!
	/// @param dc drawing context for painting (see wxWidgets documentation)
	/// @param contactMatrix matrix containing contact information
	/// @param offset_x offset in horizontal direction
	/// @param offset_y offset in vertical direction
	/// @param width available width
	/// @param height available height
	void drawContactPattern(wxDC& dc, const std::vector<std::vector<double>>& contactMatrix, int offset_x, int offset_y, int width, int height) const;
	
	/// @brief Returns the current outline color of the contact pattern picture object.
	/// @return outline color
	[[nodiscard]] wxColour getOutlineColor() const;

	/// @brief Returns the current color in which open contacts are drawn.
	/// @return "open" color
	[[nodiscard]] wxColour getOpenContactColor() const;

	/// @brief Returns the current color in which closed contacts are drawn.
	/// @return "closed" color
	[[nodiscard]] wxColour getClosedContactColor() const;
	
	/// @brief Sets the outline color of the contact pattern picture object.
	/// @param newOutlineColor new outline color
	void setOutlineColor(const wxColour& newOutlineColor);

	/// @brief Sets the color in which open contacts will be drawn.
	/// @param newOpenContactColor new "open" color
	void setOpenContactColor(const wxColour& newOpenContactColor);

	/// @brief Sets the color in which closed contacts will be drawn.
	/// @param newClosedContactColor new "closed" color
	void setClosedContactColor(const wxColour& newClosedContactColor);

	/// @brief Sets new margins to the contact pattern picture object.
	/// @param marginTop_px margin on top in pixels
	/// @param marginRight_px margin at the right in pixels
	/// @param marginBottom_px margin at bottom in pixels
	/// @param marginLeft_px margin at the left in pixels
	void setMargins(int marginTop_px, int marginRight_px, int marginBottom_px, int marginLeft_px);
	int marginTop_px{ 0 };
	int marginRight_px{ 0 };
	int marginBottom_px{ 0 };
	int marginLeft_px{ 0 };
	int spacingX_px{ 1 };
	int spacingY_px{ 1 };

private:
	std::vector<std::vector<double>> contactMatrix_;

	wxBrush openContactBrush_;
	wxBrush closedContactBrush_;
	wxPen outlinePen_;
};


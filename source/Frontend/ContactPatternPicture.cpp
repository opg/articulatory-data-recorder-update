#include "ContactPatternPicture.h"

ContactPatternPicture::ContactPatternPicture(wxWindow* parent, 
	const std::vector<std::vector<double>>& contactMatrix,
	wxColour open, wxColour closed, wxColour outline)
	: BasicPicture(parent),
	contactMatrix_(contactMatrix),
	openContactBrush_(open), closedContactBrush_(closed), outlinePen_(outline)
{
}

ContactPatternPicture::ContactPatternPicture(wxWindow* parent, 
	wxColour open, wxColour closed, wxColour outline)
	: ContactPatternPicture(parent, {}, open, closed, outline)
{
}


void ContactPatternPicture::draw(wxDC& dc)
{
	if (contactMatrix_.empty()) { return; }

	dc.SetBackground(*wxWHITE);
	dc.SetPen(outlinePen_);
	dc.Clear();

	int patternHeight = this->GetSize().y;
	if (patternHeight > 0)
	{
		int patternWidth = patternHeight * 2 / 3;
		int x = marginLeft_px;
		int y = marginTop_px;
		patternHeight -= (marginTop_px + marginBottom_px);
		patternWidth -= (marginLeft_px + marginRight_px);
		drawContactPattern(dc, contactMatrix_, x, y, patternWidth, patternHeight);
	}
}

void ContactPatternPicture::drawContactPattern(wxDC& dc, const std::vector<std::vector<double>>& contactMatrix, int offset_x, int offset_y, int width, int height) const
{
	const int numRows = static_cast<const int>(contactMatrix.size());
	const int numColumns = static_cast<const int>(contactMatrix.front().size());

	// X and Y positions of the contact pattern cells
	std::vector<int> cellX;
	std::vector<int> cellY;

	dc.SetPen(outlinePen_);
	for (int rowIdx = 0; rowIdx < numRows; ++rowIdx)
	{
		cellY.push_back(offset_y + (height - spacingY_px) * rowIdx / numRows);

		for (int colIdx = 0; colIdx < numColumns; ++colIdx)
		{
			if (rowIdx == 0)
			{
				cellX.push_back(offset_x + (width - spacingX_px) * colIdx / numColumns);
			}
			if (contactMatrix[rowIdx][colIdx] > 0)
			{
				dc.SetBrush(closedContactBrush_);
			}
			else if (contactMatrix[rowIdx][colIdx] == -1)
			{
				continue;
			}
			else
			{
				dc.SetBrush(openContactBrush_);
			}
			dc.DrawRectangle(cellX[colIdx], cellY[rowIdx], (width - spacingX_px) / (numColumns)-2 * spacingX_px, (height - spacingY_px) / (numRows)-2 * spacingY_px);
		}
	}
}

wxColour ContactPatternPicture::getOutlineColor() const
{
	return outlinePen_.GetColour();
}

wxColour ContactPatternPicture::getOpenContactColor() const
{
	return openContactBrush_.GetColour();
}

wxColour ContactPatternPicture::getClosedContactColor() const
{
	return closedContactBrush_.GetColour();
}

void ContactPatternPicture::setOutlineColor(const wxColour& newOutlineColor)
{
	this->outlinePen_ = wxPen(newOutlineColor);
}

void ContactPatternPicture::setOpenContactColor(const wxColour& newOpenContactColor)
{
	this->openContactBrush_ = wxBrush(newOpenContactColor);
}

void ContactPatternPicture::setClosedContactColor(const wxColour& newClosedContactColor)
{
	this->closedContactBrush_ = wxBrush(newClosedContactColor);
}

void ContactPatternPicture::setMargins(int marginTop_px, int marginRight_px, int marginBottom_px, int marginLeft_px)
{
	this->marginTop_px = marginTop_px;
	this->marginRight_px = marginRight_px;
	this->marginBottom_px = marginBottom_px;
	this->marginLeft_px = marginLeft_px;
}

#include "TextCtrlSelfExpanding.h"

BEGIN_EVENT_TABLE(TextCtrlSelfExpanding, wxTextCtrl)
EVT_SET_FOCUS(OnFocus)
END_EVENT_TABLE()

TextCtrlSelfExpanding::TextCtrlSelfExpanding(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos,
	const wxSize& size, long style, const wxValidator& validator, const wxString& name) : wxTextCtrl(parent, id, value, pos, size, style, validator, name)
{
}

int TextCtrlSelfExpanding::DoGetBestClientHeight(int width) const
{
	const auto charWidth = GetCharWidth();
	const auto charHeight = static_cast<double>(GetCharHeight());
	const auto charsPerLine = (width / 1.5) / charWidth + 1;
	const auto charsToDisplay = this->GetLabel().size();
	const auto linesNeeded = static_cast<double>(charsToDisplay) / charsPerLine + 1;
	const auto heightNeeded = linesNeeded * charHeight;

	std::cout << heightNeeded << std::endl;
	
	return static_cast<int>(heightNeeded);
}

void TextCtrlSelfExpanding::Update()
{
	const auto bestHeight = DoGetBestClientHeight(this->GetSize().GetWidth());
	this->SetSize(-1, bestHeight);

	const auto panelHeight = this->GetParent()->GetSize().GetHeight();
	this->SetPosition(wxPoint(this->GetPosition().x, static_cast<int>(panelHeight / 2 - bestHeight / 1.3 / 2)));
}

void TextCtrlSelfExpanding::OnFocus(wxFocusEvent& event)
{
	this->HideNativeCaret();
}

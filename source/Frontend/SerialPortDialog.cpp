#include "SerialPortDialog.h"

#include <limits>
#include <string>


// *********************************************************************
/// \brief Constructor
///
/// Creates a new COM port dialog with dropdown menus to select the
/// COM port (COM1 to COM9) and the baud rate (currently only 115200)
/// \param parent parent window
/// \param id window id
/// \param title string to be displayed in the dialog title bar
//**********************************************************************
SerialPortDialog::SerialPortDialog(wxWindow* parent, wxWindowID id, const wxString& title) : wxDialog(
	parent, id, title, wxDefaultPosition, wxDefaultSize)
{
	this->wxWindowBase::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	const SerialPort::PortSettings portSettings;
	initWidgets(portSettings);
}

SerialPortDialog::SerialPortDialog(wxWindow* parent, wxWindowID id, const wxString& title,
                                   SerialPort::PortSettings portSettings) : wxDialog(
	parent, id, title, wxDefaultPosition, wxDefaultSize)
{
	this->wxWindowBase::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));

	initWidgets(portSettings);
}

// *********************************************************************
/// \brief Destructor
//**********************************************************************

SerialPortDialog::~SerialPortDialog()
= default;

SerialPort::PortSettings SerialPortDialog::getSerialPortSettings() const
{	
	SerialPort::PortSettings dialogPortSettings;

	const auto serialPortNumber = std::to_string(nameSelection->GetValue());
	auto serialPortName = "COM" + serialPortNumber;

	int i = 0;
	for (const auto& character : serialPortName) {
		dialogPortSettings.portName[i++] = character;
	}	

	dialogPortSettings.baudRate = std::stoi(static_cast<std::string>(baudRate->GetValue()));

	wxString parityOptions[] = { "NO_PARITY", "ODD_PARITY", "EVEN_PARITY" };
	if (parity->GetValue() == parityOptions[0]) {
		dialogPortSettings.parity = SerialPort::NO_PARITY;
	}
	else if (parity->GetValue() == parityOptions[1]) {
		dialogPortSettings.parity = SerialPort::ODD_PARITY;
	}
	else if (parity->GetValue() == parityOptions[2]) {
		dialogPortSettings.parity = SerialPort::EVEN_PARITY;
	}

	wxString stopBitsOptions[] = { "ONE_STOP_BIT", "TWO_STOP_BIT" };
	if (stopBits->GetValue() == stopBitsOptions[0]) {
		dialogPortSettings.stopBits = SerialPort::ONE_STOP_BIT;
	}
	else if (stopBits->GetValue() == stopBitsOptions[1]) {
		dialogPortSettings.stopBits = SerialPort::TWO_STOP_BITS;
	}

	dialogPortSettings.hardwareFlowControl = hardwareFlowControl->GetValue();
	dialogPortSettings.timeout_s = timeout_s->GetValue();
	dialogPortSettings.timeout_ms = timeout_ms->GetValue();

	return dialogPortSettings;
}


void SerialPortDialog::initWidgets(SerialPort::PortSettings portSettings)
{
	std::string name = static_cast<std::string>(portSettings.portName);
	const int serialPortNumber = std::stoi(name.erase(0, 3));

	nameSelection = new wxSpinCtrl(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxSP_WRAP, 1, 255, serialPortNumber);


	wxString baudOptions[] = { "75", "300", "1200", "2400", "4800", "9600", "14400", "19200", "28800","38400", "57600", "115200" };
	const int numBaudRates = 12;
	baudRate = new wxComboBox(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, numBaudRates, baudOptions, wxCB_READONLY);

	for (const auto& baudOption : baudOptions) {
		if (std::stoi(static_cast<std::string>(baudOption.mb_str())) == portSettings.baudRate) {
			baudRate->SetValue(baudOption);
		}
	}

	wxString parityOptions[] = { "NO_PARITY", "ODD_PARITY", "EVEN_PARITY" };
	const int numParityOptions = 3;
	parity = new wxComboBox(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, numParityOptions, parityOptions, wxCB_READONLY);

	switch (portSettings.parity)
	{
	case SerialPort::NO_PARITY:
		parity->SetValue(parityOptions[0]);
		break;
	case SerialPort::ODD_PARITY:
		parity->SetValue(parityOptions[1]);
		break;
	case SerialPort::EVEN_PARITY:
		parity->SetValue(parityOptions[2]);
		break;
	default:
		break;
	}

	wxString stopBitsOptions[] = { "ONE_STOP_BIT", "TWO_STOP_BIT" };
	const int numStopBitsOptions = 2;
	stopBits = new wxComboBox(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, numStopBitsOptions, stopBitsOptions, wxCB_READONLY);

	switch (portSettings.stopBits)
	{
	case SerialPort::ONE_STOP_BIT:
		stopBits->SetValue(stopBitsOptions[0]);
		break;
	case SerialPort::TWO_STOP_BITS:
		stopBits->SetValue(stopBitsOptions[1]);
		break;
	default:
		break;
	}

	hardwareFlowControl = new wxCheckBox(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
	hardwareFlowControl->SetValue(portSettings.hardwareFlowControl);

	timeout_s = new wxSpinCtrl(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxSP_WRAP, 0,
	                           std::numeric_limits<int>::max(), portSettings.timeout_s);
	timeout_ms = new wxSpinCtrl(this, wxID_ANY, L"", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxSP_WRAP, 0,
	                            std::numeric_limits<int>::max(), portSettings.timeout_s);


	wxBoxSizer* topLevelSizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer* horizontalSizer = new wxBoxSizer(wxHORIZONTAL);

	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "COM-Port:"), wxSizerFlags().CenterVertical().Border(wxALL));
	nameSelection->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(nameSelection, wxSizerFlags().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "Baudrate:"), wxSizerFlags().CenterVertical().Border(wxALL));
	baudRate->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(baudRate, wxSizerFlags().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "Parity:"), wxSizerFlags().CenterVertical().Border(wxALL));
	parity->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(parity, wxSizerFlags().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "Stop bits:"), wxSizerFlags().CenterVertical().Border(wxALL));
	stopBits->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(stopBits, wxSizerFlags().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "Flow control:"), wxSizerFlags().CenterVertical().Border(wxALL));
	hardwareFlowControl->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(hardwareFlowControl, wxSizerFlags().CenterVertical().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "Timeout [s]:"), wxSizerFlags().CenterVertical().Border(wxALL));
	timeout_s->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(timeout_s, wxSizerFlags().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	horizontalSizer = new wxBoxSizer(wxHORIZONTAL);
	horizontalSizer->Add(new wxStaticText(this, wxID_ANY, "Timeout [ms]:"), wxSizerFlags().CenterVertical().Border(wxALL));
	timeout_ms->SetMinSize(wxSize(minimumWidgetSize, -1));
	horizontalSizer->Add(timeout_ms, wxSizerFlags().Border(wxALL));
	topLevelSizer->Add(horizontalSizer, wxSizerFlags(1).Align(wxALIGN_RIGHT));

	// Create and add the standard button sizer with separator line
	topLevelSizer->Add(SerialPortDialog::CreateSeparatedButtonSizer(wxOK | wxCANCEL), wxSizerFlags().Expand().Border(wxALL));

	this->SetSizerAndFit(topLevelSizer);
}

// *********************************************************************
/// \brief Event handler for cancel event
/// 
/// Is called when Cancel is pressed by user (nothing happens).
/// \param event cancel button event
//**********************************************************************

void OnCancel(wxCommandEvent& event);

// *********************************************************************
/// \brief Event handler for OK event
///
/// Is called when the OK button is pressed (nothing happens).
/// \param event OK button event
//**********************************************************************
void OnOk(wxCommandEvent& event);

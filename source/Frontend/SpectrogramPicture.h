#pragma once
#include "../Backend/Colormap.h"
#include "../Backend/Spectrogram.h"
#include "../Backend/ScalarSignal.h"
#include "GraphPicture.h"

// This event is fired every time audio settings have been changed. Parent of the dialog window can catch.
wxDECLARE_EVENT(AudioSettingsChangedEvent, wxCommandEvent);

/// @brief This class holds functionality to draw a spectrogram.
///
/// The spectrogram of the given scalar signal will be drawn with
/// defined settings (which are manipulatable though). This class is
/// designed to work in real time.
class SpectrogramPicture : public GraphPicture
{
public:
	/// @brief Constructs the spectrogram picture object with given
	/// settings. Visualization will be set to default. Background and line color
	/// do not need to be manipulated. Colormap of the spectrogram can be changed in
	/// the settings.
	/// @param parent parent of the spectrogram picture object
	/// @param audioSignal audio signal of which the spectrogram is calculated
	/// @param spectrogramSettings settings containing the configuration the
	/// calculation is based on
	/// @param backgroundColor_ background color of the picture 
	/// @param lineColor_ line color of the graph
	/// @param linewidth_px_ line width of the graph
	/// @param isPartOfStack is part of stack (check documentation of GraphPicture class)
	SpectrogramPicture(wxWindow* parent, const ScalarSignal& audioSignal, wxColour backgroundColor_ = *wxWHITE,
	                   wxColour lineColor_ = *wxBLACK, int linewidth_px_ = 3, bool isPartOfStack = false);
	~SpectrogramPicture();

	[[nodiscard]] double getLongestAnalysis() const;
	int getSamplingRate();
	/// @brief Sets a new colormap to the spectrogram picture object.
	/// @param newColorMapType type of the new colormap (can be SMOOTH_COOL_WARM, GRAY, BLACK_BODY)
	void setColorMap(ColorMap::ColorMapType newColorMapType);
	
	/// @brief Sets a new colormap to the spectrogram picture object.
	/// @param newColorMap new colormap. Has to be constructed with the constructor of the
	/// desired colormap
	void setColorMap(ColorMap* newColorMap);

	void setLongestAnalysis(double t_s);

	double zoomToAll() override;

public:
	/// @brief Struct containing all relevant settings of the spectrogram picture object
	/// which are not held by the spectrogram itself.
	struct Settings
	{
		int dynamicRange_dB_{ 150 };
		std::pair<int, int> frequencyRange;
		ColorMap* colorMap_{ new SmoothCoolWarmColorMap(true) };
		Settings() = default;
		
		/// @brief Sets a new dynamic range [db].
		/// @param newDynamicRange new dynamic range [dB]
		void setDynamicRange_dB(int newDynamicRange);

		/// @brief Returns the current dynamic range [dB].
		/// @return dynamic range [dB]
		int getDynamicRange_dB();
	}settings;

	Spectrogram spectrogram;
	Graph::LinearDomain ordinateProperties;

private:
	const ScalarSignal& audioSignal_;
	double longestAnalysis_{ 15.0 };  // in seconds

private:
	/// @brief Returns the color value which corresponds to the given
	/// magnitude value.
	/// @param magnitude magnitude value
	/// @param maxMagnitude maximum magnitude value
	/// @returns color value
	[[nodiscard]] wxColor getColorValue(double magnitude, double maxMagnitude) const;

	/// @brief Draws to the given device context.
	///
	/// Overrides the base function "draw()" from GraphPicture to manage drawing
	/// for the spectrogram picture object. Calls draw-functions from base class and drawSpectrogram()
	/// and labelSpectrogram() internally.
	/// @param dc device context for painting (see wxWidgets documentation)
	void draw(wxDC& dc) override;

	static void drawOverflowMessage(wxDC& dc);

	/// @brief Draws the currently visible part of the spectrogram.
	/// @param dc device context for painting (see wxWidgets documentation)
	void drawSpectrogram(wxDC& dc);

	/// @brief Labels the spectrogram.
	/// @param dc device context for painting (see wxWidgets documentation)
	void labelSpectrogram(wxDC& dc) const;

	void postAudioSettingsChangedEvent();
};


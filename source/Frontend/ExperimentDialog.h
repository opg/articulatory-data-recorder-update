#pragma once
#include <wx/wx.h>
#include <wx/dialog.h>
#include <map>

#include "MainWindow.h"
#include "../Backend/ExperimentManager.h"


class ExperimentDialog : public wxDialog
{
public:
	ExperimentDialog(wxWindow* parent);
	ExperimentDialog(wxWindow* parent, const Experiment<std::string>& experiment);

	void OnRandomizationStrategySelection(wxCommandEvent& event);

	void OnOk(wxCommandEvent& event);

	Experiment<std::string>& getExperiment();


private:
	static std::vector<std::string> splitDelimitedString(std::string delimitedString, std::string delimiter);
	
	int minimumWidgetSize{ 300 };
	int minimumWidgetHeight{ 300 };
	int inventoryLinesToDisplay{ 10 }; // This value is not accurate. The higher this value is set to be, the lower is the precision

	wxTextCtrl* introMessageInputCtrl_{};
	wxTextCtrl* inventoryInput{};
	wxSpinCtrl* repetitionsCtrl_{};
	wxComboBox* randomizationStratBox_{};
	wxTextCtrl* randomizationStratDescriptionText_{};
	wxTextCtrl* outroMessageInputCtrl_{};
	std::map<std::string, RandomizationStrategy::Type> randDisplayMap_;

	Experiment<std::string> experiment_;

	DECLARE_EVENT_TABLE()
};


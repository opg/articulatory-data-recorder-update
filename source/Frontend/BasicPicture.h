#ifndef __BASIC_PICTURE_H__
#define __BASIC_PICTURE_H__

#include <wx/wx.h>
#include <wx/dcbuffer.h>

/// @brief This class is a base class for bitmap based pictures.
///
/// This class is used as a base class for all other pictures. It provides
/// the basic functionality of handling a paint event that must be extended by
/// any derived classes for the specific use case.
class BasicPicture : public wxPanel
{
	// **************************************************************************
	// Public functions.
	// **************************************************************************
public:	
	/// @brief Constructs a basic picture object.
	/// @param parent parent of the constructed object
	BasicPicture(wxWindow* parent);

	/// @brief Draws to the given device context.
	///
	/// Re-implement this virtual function in a derived class to do all the painting.
	/// This function is automatically called in the paint event handler.
	/// As long as this function is not reimplemented, it will draw a test image.
	/// @param dc device context for painting (see wxWidgets documentation)
	virtual void draw(wxDC& dc);

	/// @brief Returns the bitmap where you can paint on. See the function drawTestImage()
	/// for an example.
	/// @return bitmap to paint on
	wxBitmap* getBitmap();

protected:
	wxMenu rightClickMenu;
	wxCoord menuX;
	wxCoord menuY;

	// **************************************************************************
	// Private data.
	// **************************************************************************

private:
	/// Background bitmap of the picture for double-buffered painting
	wxBitmap* bitmap;

	// **************************************************************************
	// Private functions.
	// **************************************************************************

private:

	/// @brief Event handler for paint event.
	///
	/// Is called whenever the picture needs to be repainted (screen refresh,
	/// resizing). The handler calls the respective \ref draw() function.
	/// @param event paint event
	void OnPaint(wxPaintEvent& event);

	/// @brief Event handler for erase event.
	///
	/// Intercept this event to avoid flickering. The call to this function is 
	/// necessary due to a bug in wxWidgets 2.8 and this function should be 
	/// ignored.
	/// @param event erase event
	void OnEraseBackground(wxEraseEvent& event);

	/// @brief Event handler for right click with mouse events.
	/// @param event mouse event
	void OnRightClick(wxMouseEvent& event);

	// ****************************************************************************
	// Declare the event table right at the end
	// ****************************************************************************
	DECLARE_EVENT_TABLE()
};

#endif

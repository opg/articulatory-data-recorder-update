#pragma once
#include <map>
#include <string>

#include <wx/wx.h>

#include "../Backend/SoundInterface.h"

class SoundSettingsDialog : public wxDialog
{

public:
	template <class objectType>
	class ClientObject : public wxClientData
	{
	public:
		ClientObject(objectType object) : wxClientData(), object_(object) {}

		void SetObject(objectType data) { object_ = data; }
		objectType GetObject() { return object_; }

	private:
		objectType object_;
	};

public:
	SoundSettingsDialog(wxWindow* parent, wxWindowID id, 
		const wxString& title, SoundInterface* soundInterface);

	SoundInterface::ApiInfo getSelectedApi() const;
	SoundInterface::DeviceInfo getSelectedInputDevice() const;
	SoundInterface::DeviceInfo getSelectedOutputDevice() const;
	int getSelectedSamplingRate() const;


private:
	void applyChoices();
	void refreshAll(bool doInterfaceRefresh = false);
	void refreshApiSelection(bool doInterfaceRefresh = false);
	void refreshInputDeviceSelection(bool doInterfaceRefresh = false);
	void refreshOutputDeviceSelection(bool doInterfaceRefresh = false);
	void refreshSamplingRates(bool doInterfaceRefresh = false);
	void setSoundInterface(SoundInterface* newSoundInterface);
	bool enableControls(bool doEnable = true);
	bool disableControls(bool doDisable = true) { return !enableControls(!doDisable); }
	void OnApiSelectionChanged(wxCommandEvent& event);
	void OnDeviceSelectionChanged(wxCommandEvent& event);
	void OnApply(wxCommandEvent& event);
	void OnRefreshButton(wxCommandEvent& event);
	void OnUpdateUI(wxUpdateUIEvent& event);


private:
	SoundInterface* soundInterface_{ nullptr };
	wxChoice* apiSelection{ nullptr };
	wxChoice* inputDeviceSelection{ nullptr };
	wxChoice* outputDeviceSelection{ nullptr };
	wxRadioBox* samplingRateSelection{ nullptr };
	std::vector<int> samplingRates_Hz{ 8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000, 64000, 96000, 192000 };

	// ****************************************************************************
	// Declare the event table right at the end
	// ****************************************************************************
	DECLARE_EVENT_TABLE()
};


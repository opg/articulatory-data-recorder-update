#pragma once

#include <wx/wx.h>
#include "MainWindow.h"

class Application : public wxApp
{
public:
    Application();
    virtual bool OnInit() override;

	// ****************************************************************************
	// Declare the event table right at the end
	// ****************************************************************************
	DECLARE_EVENT_TABLE()
	
private:
	MainWindow mainWindow_{ nullptr, wxID_ANY, L"Articulatory Data Recorder" };
};

DECLARE_APP(Application);
IMPLEMENT_APP(Application);

// ****************************************************************************
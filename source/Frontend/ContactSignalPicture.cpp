#include "ContactSignalPicture.h"

ContactSignalPicture::ContactSignalPicture(wxWindow* parent, const MatrixSignal& contactMatrixSignal,
	wxColour backgroundColor, wxColour lineColor,
	bool isPartOfStack) :
	GraphPicture(parent, backgroundColor, lineColor, 3, isPartOfStack), contactPatternPicture_(this), contactMatrixSignal_(contactMatrixSignal)
{
	Graph::LinearDomain abscissaProperties;
	Graph::LinearDomain ordinateProperties;

	abscissaProperties.label = contactMatrixSignal.unit_x;
	abscissaProperties.positiveLimit = 10.0;
	abscissaProperties.positiveLimitMax = 30.0;

	ordinateProperties.label = contactMatrixSignal.unit_y;
	ordinateProperties.scaleDivision = contactMatrixSignal.range.second / 2;
	ordinateProperties.negativeLimitMin = contactMatrixSignal.range.first * 1.2;
	ordinateProperties.negativeLimitMax = 10 * contactMatrixSignal.range.first * 1.2;
	ordinateProperties.negativeLimit = contactMatrixSignal.range.first * 1.2;
	ordinateProperties.positiveLimitMax = 10 * contactMatrixSignal.range.second * 1.2;
	ordinateProperties.positiveLimit = contactMatrixSignal.range.second * 1.2;
	ordinateProperties.postDecimalPositions = 1;

	graph.initAbscissa(abscissaProperties);

	graph.initLinearOrdinate(ordinateProperties);


	contactPatternPicture_.marginLeft_px = 8;
	contactPatternPicture_.marginRight_px = 8;
	graph.linearOrdinate.showGrayLines = false;


}

void ContactSignalPicture::draw(wxDC& dc)
{
	GraphPicture::draw(dc);

	if (contactMatrixSignal_.empty())
	{
		return;
	}

	int x, y, graphWidth, graphHeight;
	graph.getDimensions(x, y, graphWidth, graphHeight);
	int patternHeight = graphHeight;
	int patternWidth = patternHeight * 2 / 3;

	const int firstPatternIdx = std::max(0.0, graph.abscissa.reference * contactMatrixSignal_.getSamplingRate_Hz());
	const int maxNumPatterns = static_cast<int>(graph.abscissa.positiveLimit * contactMatrixSignal_.getSamplingRate_Hz());

	const int lastPatternPosX = graph.getXPos(graph.abscissa.reference + graph.abscissa.positiveLimit) - patternWidth;
	const int lastPatternIdx = std::min(static_cast<int>(contactMatrixSignal_.size()) - 1,
		static_cast<int>(graph.getAbsXValue(lastPatternPosX) * contactMatrixSignal_.getSamplingRate_Hz()));
	const int maxVisiblePatterns = std::max(1, graphWidth / patternWidth);
	const int framesPerPalate = std::max(1, maxNumPatterns / maxVisiblePatterns);

	patternHeight -= (contactPatternPicture_.marginTop_px + contactPatternPicture_.marginBottom_px);
	patternWidth -= (contactPatternPicture_.marginLeft_px + contactPatternPicture_.marginRight_px);


	for (int i = firstPatternIdx; i <= lastPatternIdx; i += framesPerPalate)
	{
		auto t_s = static_cast<double>(i) / contactMatrixSignal_.getSamplingRate_Hz();
		x = graph.getXPos(t_s) + contactPatternPicture_.marginLeft_px;
		auto contactMatrix = reshapeMatrix(contactMatrixSignal_[i], contactMatrixSignal_.dimensions[0], contactMatrixSignal_.dimensions[1]);

		// If the pattern is within the highlighted selection, tint the colors accordingly
		if (this->hasValidSelection())
		{
			if (t_s < getSelectionStart() || t_s > getSelectionEnd())
			{
				contactPatternPicture_.setOutlineColor(linePen.GetColour());
				contactPatternPicture_.setClosedContactColor(linePen.GetColour());
				contactPatternPicture_.setOpenContactColor(backgroundBrush.GetColour());
			}
			else
			{
				contactPatternPicture_.setOutlineColor(highlightedLinePen.GetColour());
				contactPatternPicture_.setClosedContactColor(highlightedLinePen.GetColour());
				contactPatternPicture_.setOpenContactColor(highlightedBrush.GetColour());
			}
		}
		else
		{
			contactPatternPicture_.setOutlineColor(linePen.GetColour());
			contactPatternPicture_.setClosedContactColor(linePen.GetColour());
			contactPatternPicture_.setOpenContactColor(backgroundBrush.GetColour());
		}


		contactPatternPicture_.drawContactPattern(dc, contactMatrix, x, y, patternWidth, patternHeight);
	}
}

std::vector<std::vector<double>> ContactSignalPicture::reshapeMatrix(const std::vector<double>& linearlyIndexedMatrix,
	int rows, int columns)
{
	std::vector<std::vector<double>> matrix2d(rows, std::vector<double>(columns, -1));

	for (size_t i = 0; i < rows * columns; ++i)
	{
		const int x = i / columns;
		const int y = i % rows;

		matrix2d[x][y] = linearlyIndexedMatrix[i];
	}

	return matrix2d;
}

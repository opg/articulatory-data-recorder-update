// ****************************************************************************
// This file is part of VocalTractLab.
// Copyright (C) 2008, Peter Birkholz, Hamburg, Germany
// www.vocaltractlab.de
// author: Peter Birkholz
// ****************************************************************************
#pragma once
#include <wx/wx.h>

/// @brief This class is used to display graphs.
///
/// This class can draw an axes, display a graph of some kind
/// and offers numerous functions for analysis of the graph.
class Graph
{
	// **************************************************************************
	// Public data.
	// **************************************************************************
public:
	/// Axes properties in the linear domain
	struct LinearDomain
	{
		/// Label (is displayed next to the axis)
		std::string label{ "" };
		/// reference value
		double reference{ 0.0 };
		/// interval of scale division
		double scaleDivision{ 0.1 };

		/// minimum negative limit
		double negativeLimitMin{ 0.0 };
		/// maximum negative limit
		double negativeLimitMax{ 0.0 };
		/// negative limit
		double negativeLimit{ 0.0 };

		/// minimum positive limit
		double positiveLimitMin{ 0.0 };
		/// maximum positive limit
		double positiveLimitMax{ 1.0 };
		/// positive limit
		double positiveLimit{ 1.0 };

		/// number of possible zoom steps
		int    numZoomSteps{ 10 };
		/// number of decimal places to display
		int    postDecimalPositions{ 2 };
		/// scale labels need to be converted to CGS system
		bool   useCgsUnit{ false };
		/// labels are relative to the reference value
		bool   useRelativeInscription{ false };
		/// show gray grid lines
		bool   showGrayLines{ true };
	}
	abscissa, linearOrdinate;

	/// Ordinate properties in the logarithmic domain
	struct LogDomain
	{
		/// Label (is displayed next to the axis)
		std::string label{ "" };
		/// reference value
		double reference{ 1.0 };
		/// interval of scale division
		double scaleDivision{ 1.0 };
		/// minimum lower level
		double lowerLevelMin{ -20.0 };
		/// maximum lower level
		double lowerLevelMax{ -20.0 };
		/// lower level
		double lowerLevel{ -20.0 };

		/// minimum upper level
		double upperLevelMin{ 20.0 };
		/// maximum upper level
		double upperLevelMax{ 20.0 };
		/// upper level
		double upperLevel{ 20.0 };
		/// show gray grid lines
		bool showGrayLines{ true };
		/// number of zoom steps in dB
		double zoomStep{ 10.0 };
	} logOrdinate;

	struct Margins
	{
		int top{ 0 };
		int right{ 0 };
		int bottom{ 0 };
		int left{ 0 };
	};

	enum class MarginType
	{
		TOP,
		RIGHT,
		LEFT,
		BOTTOM
	};

	// Options *****************************************************
	/// ordinate is linearly scaled (otherwise logarithmic)
	bool isLinearOrdinate{ true };
	/// abscissa is at the bottom of the graph (otherwise at the top)
	bool abscissaAtBottom{ true };
	/// ordinate is at the left side of the graph (otherwise at the right side)
	bool ordinateAtLeftSide{ true };

	// **************************************************************************
	// Public functions.
	// **************************************************************************
	Graph() = delete;

	/// @brief Constructs a graph object an initializes it with default values.
	/// @param parent Parent of the constructed object
	Graph(wxWindow* parent);

	/// @brief Constructs a graph object and initializes it with linear abscissa
	/// and ordinate
	/// @param parent Parent of the constructed object
	/// @param abscissa Linear domain object holding the abscissa properties
	/// @param ordinate Linear domain object holding the ordinate properties
	Graph(wxWindow* parent, LinearDomain abscissa, LinearDomain ordinate);

	/// @brief Constructs a graph object and initializes it with linear abscissa
	/// and logarithmic ordinate
	/// @param parent Parent of the constructed object
	/// @param abscissa Linear domain object holding the abscissa properties
	/// @param ordinate Logarithmic domain object holding the ordinate properties
	Graph(wxWindow* parent, LinearDomain abscissa, LogDomain ordinate);

	/// @brief Returns the dimensions of the graph.
	/// @param [out] x x-coordinate of the graph
	/// @param [out] y y-coordinate of the graph 
	/// @param [out] w width of the graph 
	/// @param [out] h height of the graph
	void getDimensions(int& x, int& y, int& w, int& h);

	/// @brief Returns the margins on the sides of the graph.
	/// @return margins object
	[[nodiscard]] const Margins& getMargins() const;

	/// @brief Sets new margins on the sides of the graph.
	/// @param newMargins margins object holding the new margins
	void setMargins(const Margins& newMargins);

	/// @brief Sets a new margin to specific side of the graph.
	/// @param newMargin value of new margin
	/// @param type TOP, RIGHT, LEFT or BOTTOM
	void setMargin(int newMargin, MarginType type);

	/// @brief Initializes a linear abscissa with given values.
	/// @param abscissa Linear domain object holding the abscissa properties
	void initAbscissa(LinearDomain abscissa);

	void initAbscissa(std::string label, double reference, double scaleDivision,
	                  double negativeLimitMin, double negativeLimitMax, double negativeLimit,
	                  double positiveLimitMin, double positiveLimitMax, double positiveLimit,
	                  int numZoomSteps, int postDecimalPositions,
	                  bool useCgsUnit, bool useRelativeInscription, bool showGrayLines);

	/// @brief Initializes a linear ordinate with given values.
	/// @param ordinate Linear domain object holding the ordinate properties
	void initLinearOrdinate(LinearDomain ordinate);
	
	void initLinearOrdinate(std::string label, double reference, double scaleDivision,
	                        double negativeLimitMin, double negativeLimitMax, double negativeLimit,
	                        double positiveLimitMin, double positiveLimitMax, double positiveLimit,
	                        int numZoomSteps, int postDecimalPositions,
	                        bool useCgsUnit, bool useRelativeInscription, bool showGrayLines);

	/// @brief Initializes a logarithmic ordinate with given values.
	/// @param ordinate Logarithmic domain object holding the ordinate properties
	void initLogOrdinate(LogDomain ordinate);
	
	void initLogOrdinate(std::string label, double reference, double scaleDivision,
		double lowerLevelMin, double lowerLevelMax, double lowerLevel,
		double upperLevelMin, double upperLevelMax, double upperLevel,
		bool showGrayLines, double zoomStep);

	/// @brief Draw the abscissa to the specified device context.
	/// @param dc device context for painting (see wxWidgets documentation)
	/// @param drawLabels Labels are shown
	void paintAbscissa(wxDC& dc, bool drawLabels = true);

	/// @brief Draw the ordinate scale to the specified device context.
	/// @param dc device context for painting (see wxWidgets documentation)
	void paintOrdinate(wxDC& dc);

	/// @brief Zooms into the abscissa by changing the current negative and/or positive
	/// limit by a zoom factor.
	/// @param negativeLimit negative limit shall be affected (or not)
	/// @param positiveLimit positive limit shall be affected (or not)
	void zoomInAbscissa(bool negativeLimit, bool positiveLimit);

	/// @brief Zooms out of the abscissa by changing the current negative and/or positive
	/// limit by a zoom factor.
	/// @param negativeLimit negative limit shall be affected (or not)
	/// @param positiveLimit positive limit shall be affected (or not)
	void zoomOutAbscissa(bool negativeLimit, bool positiveLimit);

	/// @brief Zooms into the ordinate by changing the current negative and/or positive
	/// limit by a zoom factor.
	/// @param negativeLimit negative limit shall be affected (or not)
	/// @param positiveLimit positive limit shall be affected (or not)
	void zoomInOrdinate(bool negativeLimit, bool positiveLimit);

	/// @brief Zooms out of the ordinate by changing the current negative and/or positive
	/// limit by a zoom factor.
	/// @param negativeLimit negative limit shall be affected (or not)
	/// @param positiveLimit positive limit shall be affected (or not)
	void zoomOutOrdinate(bool negativeLimit, bool positiveLimit);

	// Transformationen phys. quantity <-> pixel values **********

	/// @brief Returns the x-position in the graph that corresponds with the specified
	/// physical quantity.
	/// @param absXValue value of a physical quantity
	/// @return x-position in graph corresponding to value
	int getXPos(double absXValue);

	/// @brief Returns the y-position in the graph that corresponds with the specified
	/// physical quantity.
	/// @param absYValue value of a physical quantity
	/// @return y-position in graph corresponding to value
	int getYPos(double absYValue);

	/// @brief Returns value of a physical quantity in the graph that corresponds 
	/// to the specified x-coordinate.
	/// @param xPos x-position in graph
	/// @return value of a physical quantity at specified position
	double getAbsXValue(int xPos);

	/// @brief Returns value of a physical quantity in the graph that corresponds 
	/// to the specified y-coordinate.
	/// @param yPos y-position in graph
	/// @return value of a physical quantity at specified position
	double getAbsYValue(int yPos);

	double getEPSILON()
	{
		return EPSILON;
	}

	// **************************************************************************
	// Private data.
	// **************************************************************************
private:
	static const double EPSILON;

	/// Margins of the graph to the edge of the picture
	Margins margins;
	wxWindow* control{nullptr};

	// **************************************************************************
	// Private functions.
	// **************************************************************************
private:
	/// @brief Calculates the factors by which the negative and/or positive limits of the
	/// axes are divided or multiplied when zooming.
	/// @param domain the current domain (can be abscissa or ordinate)
	/// @param [out] positiveZoomFactor zoom factor for positive limit
	/// @param [out] negativeZoomFactor zoom factor for negative limit
	void getZoomFactors(LinearDomain* domain, double& positiveZoomFactor, double& negativeZoomFactor);
};

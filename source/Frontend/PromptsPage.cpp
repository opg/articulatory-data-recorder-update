#include "PromptsPage.h"

BEGIN_EVENT_TABLE_TEMPLATE1(PromptsPage, wxPanel, PromptContentType)
END_EVENT_TABLE()

// ****************************************************************************
/// Constructor.
// ****************************************************************************
template<class PromptContentType>
PromptsPage<PromptContentType>::PromptsPage(wxWindow* parent, ExperimentManager<PromptContentType>* ptrExperimentManager, wxWindowID id) : wxPanel(parent, id, wxDefaultPosition, wxDefaultSize, wxCLIP_CHILDREN), sizer(nullptr), topLevelSizer(nullptr), ptrExperimentManager_(ptrExperimentManager)
{
	this->wxWindowBase::SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	this->SetDoubleBuffered(true);
}

// Necessary to avoid linker error when only including the PromptsPage.h
template class PromptsPage<std::string>;



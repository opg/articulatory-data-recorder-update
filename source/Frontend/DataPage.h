#pragma once

#include <map>
#include <vector>
#include <wx/wx.h>
#include <map>

#include "../Backend/FrameBuffer.h"
#include "GraphPicture.h"
#include "SpectrogramPicture.h"

class DataPage : public wxPanel
{
	// ****************************************************************
	// Public data.
	// ****************************************************************
public:
	enum class Mode { RECORDING, ANALYSIS };
	Mode mode{ Mode::ANALYSIS };
	std::vector<GraphPicture*> signalPictures_;
	std::map<std::string, GraphPicture::Properties> signalProperties_{};
	SpectrogramPicture* specPicture{ nullptr };

	struct View
	{
		double min{ 0.0 };
		double reference{ 0.0 };
		double max{ 0.0 };
	};
	
	// ****************************************************************
	// Public functions.
	// ****************************************************************
public:
	DataPage(FrameBuffer& signals, ScalarSignal& audioSignal, wxWindow* parent, DataPage::Mode workingMode, wxWindowID id = wxID_ANY);
	void setGraphReference(double newReference);
	void setGraphLimit(double posLimit, double negLimit = 0.0);
	void clearSelection();
	GraphPicture::Selection getSelection();
	void setSelection(GraphPicture::Selection newSelection);
	bool hasSelection();
	void reset();
	View getView() const;
	void setView(View newView);
	void zoomIn();
	void zoomOut();
	void zoomToAll();
	void zoomToSelection();

	
	void updateWidgets();

	// ****************************************************************
	// Private functions.
	// ****************************************************************
private:
	void initWidgets();
	void updateSignalProperties();
	void attachAbscissaToLastVisiblePicture();
	void OnTimeScrollBar(wxScrollEvent& event);
	void OnResize(wxSizeEvent& event);
	void OnScrollRequest(wxCommandEvent& event);
	void OnSelectionChanged(wxCommandEvent& event);
	
	// ****************************************************************
	// Private data.
	// ****************************************************************
private:
	FrameBuffer& signals_;
	ScalarSignal& audioSignal;
	wxScrollBar* scrTime;
	wxBoxSizer* sizer;
	const int minTrackHeight{ 65 }; // <-- adjusts the minimum height one single track is supposed to have
	const int maxTrackHeight{ 300 }; // <-- adjusts the maximum height one single track is supposed to have

	GraphPicture::Selection selection_;
	double graphReference_{ 0.0 };
	double graphPositiveLimit_{ 0.0 };
	double graphNegativeLimit_{ 0.0 };
	
	bool lastShownPicFound{ false };
	DECLARE_EVENT_TABLE()
};

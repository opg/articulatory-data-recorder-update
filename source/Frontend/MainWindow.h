#pragma once

#include <wx/wx.h>
#include <wx/aui/auibook.h>
#include <wx/toolbar.h>
#include <wx/splitter.h>

// Backend
#include "../Backend/Data.h"

// Frontend
#include "DataPage.h"
#include "StringPromptsPage.h"
#include "SerialPortDialog.h"
#include "SignalPicturePropertiesDialog.h"
#include "SpectrogramPictureSettingsDialog.h"
#include "SoundSettingsDialog.h"


// ****************************************************************************
// Main window of the application.
// ****************************************************************************

class MainWindow : public wxFrame
{

public:
	// **************************************************************************
	// Public functions.
	// **************************************************************************
	MainWindow(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString);

	// **************************************************************************
	// Public data.
	// **************************************************************************
public:

	


// **************************************************************************
// Private data.
// **************************************************************************
private:
	wxToolBar* toolbar{ nullptr };
	wxAuiNotebook* notebook{ nullptr };
	DataPage* dataPage{ nullptr };
	StringPromptsPage* promptsPage{ nullptr };
	StringPromptsPage* promptsPageSecondary{ nullptr };
	wxFrame* promptsWindow{ nullptr };
	wxSplitterWindow* splitter{ nullptr };
	bool mainWindowIsSplit{ false };
	int minimalPaneSize{ 40 };
	SerialPortDialog* serialPortDialog;
	SerialPort::PortSettings serialPortSettings;
	SignalPicturePropertiesDialog* signalPicturePropertiesDialog{ nullptr };
	SpectrogramPictureSettingsDialog* spectrogramPictureSettingsDialog{ nullptr };
	ExperimentManager<std::string> experimentManager;


	// **************************************************************************
	// Private functions.
	// **************************************************************************
private:
	void initWidgets();
	void updateWidgets();
	void changeBackend(HardwareBackend::Type newBackendSelection);
	bool pauseAudio();
	bool playAudio();
	bool queryRangeOnly();
	bool saveAdrFile(wxString filename, double start, double end);
	bool saveWavFile(wxString filename, double start, double end);
	bool stopAudio();
	void resetSignalPage(bool mainWindowIsSplit);
	void prepareExperiment(Experiment<std::string> experiment);
	DataPage* createDataPage(wxWindow* parent, DataPage::Mode workingMode);
	void setNextPrompt();
	void setPreviousPrompt();
	void synchronizeAudio();
	void resetBuffers();


	
	void OnLoadDataset(wxCommandEvent& event);
	void OnLoadPalate(wxCommandEvent& event);
	void OnSaveAdr(wxCommandEvent& event);
	void OnSaveWav(wxCommandEvent& event);
	void OnSaveSession(wxCommandEvent& event);
	void OnPalateConfig(wxCommandEvent& event);
	void OnRecord(wxCommandEvent& event);
	void OnPlayAudio(wxCommandEvent& event);
	void OnStopAudio(wxCommandEvent& event);
	void OnRepeatAudio(wxCommandEvent& event);
	void OnResetBuffers(wxCommandEvent& event);
	void OnSelectHardwareBackend(wxCommandEvent& event);
	void OnSoundSettings(wxCommandEvent& event);
	void OnUseCalibrator(wxCommandEvent& event);
	void OnViewTrackProperties(wxCommandEvent& event);
	void OnSignalPropChanged(wxCommandEvent& event);
	void OnViewSpectrogramSettings(wxCommandEvent& event);
	void OnSpecSettingsChanged(wxCommandEvent& event);
	void OnAudioSettingsChanged(wxCommandEvent& event);
	void OnComConfig(wxCommandEvent& event);
	void OnNewExperiment(wxCommandEvent& event);
	void OnLoadExperiment(wxCommandEvent& event);
	void OnEditExperiment(wxCommandEvent& event);
	void OnSaveExperiment(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);

	void OnMousewheel(wxMouseEvent& event);
	
	void OnZoom(wxCommandEvent& event);

	void OnNextPrompt(wxCommandEvent& event);
	void OnPreviousPrompt(wxCommandEvent& event);
	void OnPromptsPageUpdate(wxCommandEvent& event);

	void OnClose(wxCommandEvent& event);
	void OnCloseWindow(wxCloseEvent& event);

	void OnToggleLayout(wxCommandEvent& event);
	void OnNotebookSplit(wxCommandEvent& event);

	void OnResize(wxSizeEvent& event);

	void OnIntroReached(wxCommandEvent& event);
	void OnOutroReached(wxCommandEvent& event);

	void OnCheckPlaybackStatus(wxTimerEvent& event);

	void OnDataCollected(wxCommandEvent& event);

	void OnDataSelectionChanged(wxCommandEvent& event);

	std::unique_ptr<DataCollector> hardwareBackend_{nullptr};  // Do not initialize to a backend here, init widgets first!

	wxTimer* checkPlaybackTimer_;

	// ****************************************************************************
	// Declare the event table right at the end
	// ****************************************************************************
	DECLARE_EVENT_TABLE()
};

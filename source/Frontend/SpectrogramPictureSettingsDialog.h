#pragma once
#include <wx/dialog.h>
#include <wx/spinctrl.h>

#include "SpectrogramPicture.h"

// This event is fired every time spectrogram settings have been changed. Parent of the dialog window can catch.
wxDECLARE_EVENT(SpecSettingsChangedEvent, wxCommandEvent);


class SpectrogramPictureSettingsDialog :
    public wxDialog
{
public:
	SpectrogramPictureSettingsDialog(wxWindow* parent, wxWindowID id, const wxString& title, SpectrogramPicture& specPicture);
	virtual ~SpectrogramPictureSettingsDialog() = default;

	void initWidgets();
	void updateWidgets();

private:
	int minimumWidgetSize{ 175 };
	SpectrogramPicture* ptrSpecPic_;

	wxSpinCtrlDouble* selectDynamicRange{ nullptr };
	wxSpinCtrlDouble* selectFrameLength_{ nullptr };
	wxSpinCtrlDouble* selectLowerBoundFreqRange{ nullptr };
	wxSpinCtrlDouble* selectUpperBoundFreqRange{ nullptr };
	wxSpinCtrlDouble* selectFftLength{ nullptr };
	wxSpinCtrlDouble* selectOverlap{ nullptr };
	wxSpinCtrlDouble* selectRelativeCutoff{ nullptr };
	wxSpinCtrlDouble* selectPreEmphasisCoeff{ nullptr };
	wxComboBox* windowComboBox{ nullptr };
	wxComboBox* colorMapComboBox{ nullptr };

private:
	void OnChangeDynamicRange(wxSpinDoubleEvent& event);
	void OnChangeFrameLength(wxSpinDoubleEvent& event);
	void OnChangeOverlap(wxSpinDoubleEvent& event);
	void OnChangeLowerFreq(wxSpinDoubleEvent& event);
	void OnChangeUpperFreq(wxSpinDoubleEvent& event);
	void OnChangeRelativeCutoff(wxSpinDoubleEvent& event);
	void OnChangePreEmphasisCoeff(wxSpinDoubleEvent& event);
	void OnChangeWindowType(wxCommandEvent& event);
	void OnChangeFftLength(wxSpinDoubleEvent& event);
	void OnChangeColorMap(wxCommandEvent& event);
	void OnEnter(wxCommandEvent& event);

	void postSpecSettingsChangedEvent();


	// ****************************************************************************
	// Declare the event table right at the end
	// ****************************************************************************
	DECLARE_EVENT_TABLE()
};


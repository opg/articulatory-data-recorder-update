#include "SoundSettingsDialog.h"

#include <iterator>
#include <stdexcept>

namespace 
{
	static const char* refresh_arrows_xpm[] = {
	"48 48 2 1",
	" 	g None",
	".	g #000000",
	"                                                ",
	"                                                ",
	"                        .                       ",
	"                        ..                      ",
	"                        ...                     ",
	"                        ....                    ",
	"                        .....                   ",
	"                        ......                  ",
	"                    ...........                 ",
	"                 ...............                ",
	"               .................                ",
	"              .................                 ",
	"             ........   ......                  ",
	"            ......      .....                   ",
	"           ......       ....                    ",
	"          ......        ...                     ",
	"          .....         ..          ..          ",
	"         .....          .          ....         ",
	"         ....                      ....         ",
	"         ....                      ....         ",
	"        .....                      .....        ",
	"        ....                        ....        ",
	"        ....                        ....        ",
	"        ....                        ....        ",
	"        ....                        ....        ",
	"        ....                        ....        ",
	"        ....                        ....        ",
	"        .....                      .....        ",
	"         ....                      ....         ",
	"         ....                      ....         ",
	"         ....          .          .....         ",
	"          ..          ..         .....          ",
	"                     ...        ......          ",
	"                    ....       ......           ",
	"                   .....      ......            ",
	"                  ......   ........             ",
	"                 .................              ",
	"                .................               ",
	"                ...............                 ",
	"                 ...........                    ",
	"                  ......                        ",
	"                   .....                        ",
	"                    ....                        ",
	"                     ...                        ",
	"                      ..                        ",
	"                       .                        ",
	"                                                ",
	"                                                " };
}

static const int IDB_REFRESH = 10001;

static const int IDC_API = 10000;
static const int IDC_INPUT_DEVICE = 20000;
static const int IDC_OUTPUT_DEVICE = 30000;

BEGIN_EVENT_TABLE(SoundSettingsDialog, wxDialog)
EVT_BUTTON(IDB_REFRESH, OnRefreshButton)
EVT_BUTTON(wxID_OK, OnApply)
EVT_BUTTON(wxID_APPLY, OnApply)
EVT_CHOICE(IDC_API, OnApiSelectionChanged)
EVT_CHOICE(IDC_INPUT_DEVICE, OnDeviceSelectionChanged)
EVT_CHOICE(IDC_OUTPUT_DEVICE, OnDeviceSelectionChanged)
EVT_UPDATE_UI(wxID_ANY, OnUpdateUI)
END_EVENT_TABLE()


SoundSettingsDialog::SoundSettingsDialog(wxWindow* parent, wxWindowID id, const
                                         wxString& title, SoundInterface* soundInterface)
	: wxDialog(parent, id, title, wxDefaultPosition, wxDefaultSize), soundInterface_(soundInterface)
{
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	
	auto* topLevelSizer = new wxBoxSizer(wxVERTICAL);
	auto* horzSizer = new wxFlexGridSizer(2);
	auto* optionsSizer = new wxFlexGridSizer(2, { 10, 10 });

	const auto labelFlags = wxSizerFlags().CenterVertical().Right().Border(wxALL, 10);
	const auto inputFlags = wxSizerFlags(1).Expand().CenterVertical().Left().Border(wxALL, 10);
	
	// Host APIs
	auto* label = new wxStaticText(this, wxID_ANY, "Host API:");
	optionsSizer->Add(label, labelFlags);

	apiSelection = new wxChoice(this, IDC_API);
	refreshApiSelection();
	optionsSizer->Add(apiSelection, inputFlags);

	// Devices
	const auto defaultOutput = soundInterface->getDefaultOutputDevice();

	// Input
	label = new wxStaticText(this, wxID_ANY, "Input Device:");
	optionsSizer->Add(label, labelFlags);
	
	inputDeviceSelection = new wxChoice(this, IDC_INPUT_DEVICE);
	refreshInputDeviceSelection();
	optionsSizer->Add(inputDeviceSelection, inputFlags);

	// Output
	label = new wxStaticText(this, wxID_ANY, "Output Device:");
	optionsSizer->Add(label, labelFlags);

	outputDeviceSelection = new wxChoice(this, IDC_OUTPUT_DEVICE);
	refreshOutputDeviceSelection();
	optionsSizer->Add(outputDeviceSelection, inputFlags);

	horzSizer->Add(optionsSizer);

	// Sampling rate options
	wxArrayString samplingRates_str;
	for (const auto& sr : samplingRates_Hz)
	{
		samplingRates_str.Add(wxString::Format(wxT("%i"), sr));
	}
	samplingRateSelection = new wxRadioBox(this, wxID_ANY, "Sampling rate [Hz]", 
		wxDefaultPosition, wxDefaultSize, samplingRates_str, 3, wxRA_SPECIFY_ROWS);

	const auto currentSamplingRate = soundInterface->getPlaybackSamplingRate();
	const auto idx = samplingRateSelection->FindString(wxString::Format("%i", static_cast<int>(currentSamplingRate)));
	if(idx == wxNOT_FOUND)
	{
		throw std::runtime_error(wxString::Format("[SoundSettingsDialog()] Encountered unsupported sampling rate: %.2f", currentSamplingRate));
	}
	samplingRateSelection->SetSelection(idx);

	horzSizer->Add(samplingRateSelection, wxSizerFlags(1).Expand().Border(wxRIGHT | wxBOTTOM, 10));

	topLevelSizer->Add(horzSizer);

	// Create and add the standard button sizer with separator line
	const wxImage refreshIcon = wxBitmap(refresh_arrows_xpm).ConvertToImage().Rescale(GetTextExtent("MMM").GetHeight(), GetTextExtent("MMM").GetHeight());
	const auto refreshFlags = wxSizerFlags().CenterVertical().Border(wxRIGHT, 10);
	auto* buttonSizer = CreateButtonSizer(wxOK | wxAPPLY | wxCANCEL);
	auto* refreshButton = new wxBitmapButton(this, IDB_REFRESH, refreshIcon);
	refreshButton->SetToolTip("Click here to refresh the device lists (e.g. after connecting or disconnecting a device)");
	buttonSizer->Insert(0, refreshButton);
	topLevelSizer->Add(CreateSeparatedSizer(buttonSizer), wxSizerFlags(1).Expand().Border(wxALL, 5));
	this->SetSizerAndFit(topLevelSizer);
}

SoundInterface::ApiInfo SoundSettingsDialog::getSelectedApi() const
{
	const auto selection = apiSelection->GetSelection();
	if (selection == wxNOT_FOUND)
	{
		return SoundInterface::noApi;
	}
	const auto selectedApi = dynamic_cast<ClientObject<SoundInterface::ApiInfo>*>(apiSelection->GetClientObject(selection));

	return selectedApi->GetObject();
}

SoundInterface::DeviceInfo SoundSettingsDialog::getSelectedInputDevice() const
{
	const auto selection = inputDeviceSelection->GetSelection();
	if (selection == wxNOT_FOUND)
	{
		return SoundInterface::noDevice;
	}
	const auto clientData = dynamic_cast<ClientObject<SoundInterface::DeviceInfo>*>(inputDeviceSelection->GetClientObject(selection));

	return clientData->GetObject();
}

SoundInterface::DeviceInfo SoundSettingsDialog::getSelectedOutputDevice() const
{
	const auto selection = outputDeviceSelection->GetSelection();
	if (selection == wxNOT_FOUND)
	{
		return SoundInterface::noDevice;
	}
	const auto clientData = dynamic_cast<ClientObject<SoundInterface::DeviceInfo>*>(outputDeviceSelection->GetClientObject(selection));

	return clientData->GetObject();
}

int SoundSettingsDialog::getSelectedSamplingRate() const
{
	return wxAtoi(samplingRateSelection->GetStringSelection());
}

void SoundSettingsDialog::applyChoices()
{
	const auto selectedApi = getSelectedApi();
	const auto selectedInputDevice = getSelectedInputDevice();
	const auto selectedOutputDevice = getSelectedOutputDevice();
	const auto selectedSamplingRate = getSelectedSamplingRate();
	
	soundInterface_->setInputDevice(selectedInputDevice);
	soundInterface_->setRecordingSamplingRate(selectedSamplingRate);
	soundInterface_->setOutputDevice(selectedOutputDevice);
	soundInterface_->setPlaybackSamplingRate(selectedSamplingRate);
}

void SoundSettingsDialog::refreshAll(bool doInterfaceRefresh)
{
	if (doInterfaceRefresh)
	{
		soundInterface_->reset();
	}
	refreshApiSelection(false);
	refreshInputDeviceSelection(false);
	refreshOutputDeviceSelection(false);
}

void SoundSettingsDialog::refreshApiSelection(bool doInterfaceRefresh)
{
	if (doInterfaceRefresh)
	{
		soundInterface_->reset();
	}
	const auto apis = soundInterface_->getHostApis();
	apiSelection->Clear();

	const auto currentApi = soundInterface_->getCurrentApi();

	for (const auto& api : apis)
	{
		int idx = apiSelection->Append(api.name, new ClientObject(api));
		if (api.index == currentApi.index)
		{
			apiSelection->SetSelection(idx);
		}
	}
}

void SoundSettingsDialog::refreshInputDeviceSelection(bool doInterfaceRefresh)
{
	if (doInterfaceRefresh)
	{
		soundInterface_->reset();
	}
	const auto devices = soundInterface_->getDevices();
	inputDeviceSelection->Clear();
	const auto selectedApi = getSelectedApi();
	if (selectedApi == SoundInterface::noApi) return;
	const auto currentInputDevice = soundInterface_->getCurrentInputDevice();
	for (const auto& device : devices)
	{
		if (device.maxInputChannels == 0 || device.hostApiIndex != selectedApi.index) { continue; }

		int idx = inputDeviceSelection->Append(device.name, new ClientObject(device));
		if (device.index == currentInputDevice.index)
		{
			inputDeviceSelection->SetSelection(idx);
		}
	}
}

void SoundSettingsDialog::refreshOutputDeviceSelection(bool doInterfaceRefresh)
{
	if (doInterfaceRefresh)
	{
		soundInterface_->reset();
	}
	const auto devices = soundInterface_->getDevices();
	outputDeviceSelection->Clear();

	const auto selectedApi = getSelectedApi();
	if (selectedApi == SoundInterface::noApi) return;

	const auto currentOutputDevice = soundInterface_->getCurrentOutputDevice();
	for (const auto& device : devices)
	{
		if (device.maxOutputChannels == 0 || device.hostApiIndex != selectedApi.index) { continue; }
		int idx = outputDeviceSelection->Append(device.name, new ClientObject(device));
		if (device.index == currentOutputDevice.index)
		{
			outputDeviceSelection->SetSelection(idx);
		}
	}
}

void SoundSettingsDialog::refreshSamplingRates(bool doInterfaceRefresh)
{
	if (doInterfaceRefresh)
	{
		soundInterface_->reset();
	}
	const auto numOptions = samplingRateSelection->GetCount();
	for (unsigned i = 0; i < numOptions; ++i)
	{
		const auto sr = wxAtoi(samplingRateSelection->GetString(i));
		const bool inputSupported = soundInterface_->isSamplingRateSupported(getSelectedInputDevice(), sr);
		const bool outputSupported = soundInterface_->isSamplingRateSupported(getSelectedOutputDevice(), sr);
		samplingRateSelection->Enable(i, inputSupported && outputSupported);
	}
	// If the currently selected sampling rate is still enabled, we can return here
	const auto currentSelection = samplingRateSelection->GetSelection();
	if (samplingRateSelection->IsItemEnabled(currentSelection)) return;

	// Otherwise we select the next lower sampling rate that *is* currently supported
	auto newSelection = currentSelection - 1;
	while (newSelection >= 0)
	{
		if (samplingRateSelection->IsItemEnabled(newSelection))
		{
			samplingRateSelection->SetSelection(newSelection);
			return;
		}
		newSelection--;
	}
	// If we could not find any, look for the next higher one
	newSelection = currentSelection + 1;
	while (newSelection < static_cast<int>(numOptions))
	{
		if (samplingRateSelection->IsItemEnabled(newSelection))
		{
			samplingRateSelection->SetSelection(newSelection);
			return;
		}
		newSelection++;
	}

	// If we still could not find one, there are no compatible sampling rates!
	throw std::invalid_argument("There are no supported sampling rates available using the selected devices! Please choose a different input and/or output device.");
}

void SoundSettingsDialog::setSoundInterface(SoundInterface* newSoundInterface)
{
	soundInterface_ = newSoundInterface;
	refreshAll(true);
}

bool SoundSettingsDialog::enableControls(bool doEnable)
{
	const auto controls = this->GetChildren();
	for (const auto control : controls)
	{
		control->Enable(doEnable);
	}
	return doEnable;
}

void SoundSettingsDialog::OnApiSelectionChanged(wxCommandEvent& event)
{
	refreshInputDeviceSelection(false);
	refreshOutputDeviceSelection(false);
	refreshSamplingRates(false);
	// Make sure the wxChoice widgets are large enough to fit the names
	this->Fit();
}

void SoundSettingsDialog::OnDeviceSelectionChanged(wxCommandEvent& event)
{
	try
	{
		refreshSamplingRates(false);
	}
	catch (const std::invalid_argument& e)
	{
		wxMessageBox(e.what(), "No supported sampling rate available", wxICON_ERROR);
		dynamic_cast<wxChoice*>(event.GetEventObject())->SetSelection(-1);
	}
	
}

void SoundSettingsDialog::OnApply(wxCommandEvent& event)
{
	applyChoices();
	if (this->GetParent() != nullptr)
	{
		this->GetParent()->Refresh();
	}	
	event.Skip();
}

void SoundSettingsDialog::OnRefreshButton(wxCommandEvent& event)
{
	refreshAll(true);	
	// Make sure the wxChoice widgets are large enough to fit the names
	this->Fit();
}

void SoundSettingsDialog::OnUpdateUI(wxUpdateUIEvent& event)
{
	if(soundInterface_->isRunning())
	{
		disableControls();
	}
	else
	{
		enableControls();
	}
	
	event.Skip();
}
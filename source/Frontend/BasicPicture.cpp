#include "BasicPicture.h"
#include <wx/dcgraph.h>

// ****************************************************************************
// The event table.
// ****************************************************************************
BEGIN_EVENT_TABLE(BasicPicture, wxPanel)
EVT_PAINT(BasicPicture::OnPaint)
EVT_ERASE_BACKGROUND(BasicPicture::OnEraseBackground)
EVT_RIGHT_DOWN(BasicPicture::OnRightClick)
END_EVENT_TABLE()

BasicPicture::BasicPicture(wxWindow* parent) : wxPanel(parent, wxID_ANY,
	wxDefaultPosition, wxDefaultSize,
	wxFULL_REPAINT_ON_RESIZE     // Force a complete redraw always when the window is resized
)
{
	// Leave all background painting to the application
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);

	// Initialize the background image with a small default size.
	bitmap = new wxBitmap(200, 200);

}

void BasicPicture::draw(wxDC& dc)
{
	// Clear the background
	dc.SetBackground(*wxRED_BRUSH);
	dc.Clear();

	int w, h;
	this->GetSize(&w, &h);

	dc.SetPen(*wxBLACK_PEN);
	dc.DrawLine(0, 0, w - 1, h - 1);

	dc.SetPen(*wxWHITE_PEN);
	dc.DrawLine(w - 1, 0, 0, h - 1);
}

wxBitmap* BasicPicture::getBitmap()
{
	// Make sure that we have a background bitmap that is always at least 
	// as big as the client area of this window.

	int w, h;
	this->GetSize(&w, &h);
	if ((bitmap->GetWidth() < w) || (bitmap->GetHeight() < h))
	{
		delete bitmap;
		bitmap = new wxBitmap(w, h);
	}

	// Return the pointer to the bitmap
	return bitmap;
}

void BasicPicture::OnPaint(wxPaintEvent& event)
{
	// ALWAYS create the DC object, whether it is used or not.
	// On destruction of the DC object, the content of bitmap buffer is
	// automatically copied into the client area of this window.

	wxBufferedPaintDC dc(this, *getBitmap());
	draw(dc);	
}

void BasicPicture::OnEraseBackground(wxEraseEvent& event)
{
	// Do nothing here - especially, DO NOT class event.Skip() !!
}

void BasicPicture::OnRightClick(wxMouseEvent& event)
{
	menuX = event.GetX();
	menuY = event.GetY();
	PopupMenu(&rightClickMenu);
}

// ****************************************************************************

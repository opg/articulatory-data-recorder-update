#include "ScalarSignalPicture.h"
#include <algorithm>
#include <execution>


ScalarSignalPicture::ScalarSignalPicture(wxWindow* parent, const ScalarSignal& signal,
	wxColour backgroundColor, wxColour lineColor, int linewidth_px,
	bool isPartOfStack) :
	GraphPicture(parent, backgroundColor, lineColor, linewidth_px, isPartOfStack), signal_(signal)
{
	Graph::LinearDomain abscissaProperties;
	Graph::LinearDomain ordinateProperties;

	abscissaProperties.label = signal_.unit_x;
	abscissaProperties.positiveLimit = 10.0;  // Initial maximum displayed value
	abscissaProperties.positiveLimitMin = 0.2;  // Maximum displayed value when fully zoomed in
	abscissaProperties.positiveLimitMax = 3600.0;  // Maximum displayed value when fully zoomed out

	ordinateProperties.label = signal_.current_unit_y;
	ordinateProperties.scaleDivision = signal_.current_range.second / 2;
	ordinateProperties.negativeLimitMin = signal_.current_range.first * 1.2;
	ordinateProperties.negativeLimitMax = 10 * signal_.current_range.first * 1.2;
	ordinateProperties.negativeLimit = signal_.current_range.first * 1.2;
	ordinateProperties.positiveLimitMax = 10 * signal_.current_range.second * 1.2;
	ordinateProperties.positiveLimit = signal_.current_range.second * 1.2;
	ordinateProperties.postDecimalPositions = 1;

	graph.initAbscissa(abscissaProperties);
	graph.initLinearOrdinate(ordinateProperties);
}

double ScalarSignalPicture::zoomToAll()
{
	if (!signal_.empty())
	{
		graph.abscissa.reference = 0;
		graph.abscissa.positiveLimit = signal_.size() * 1.0 / signal_.getSamplingRate_Hz();
	}

	return graph.abscissa.positiveLimit;
}

void ScalarSignalPicture::draw(wxDC& dc)
{
	GraphPicture::draw(dc);

	drawSignal(dc);
}

void ScalarSignalPicture::drawSignal(wxDC& dc)
{
	if (signal_.empty()) { labelSignal(dc); return; }
	
	const int startSampleIndex = std::max(0, static_cast<int>(graph.abscissa.reference * static_cast<double>(signal_.getSamplingRate_Hz())));
	if (startSampleIndex >= signal_.size()) { return; }
	
	const int maxVisibleSampleIndex = static_cast<int>((graph.abscissa.reference + graph.abscissa.positiveLimit) * static_cast<double>(signal_
		.getSamplingRate_Hz()));
	const int lastSampleIndex = std::min(static_cast<int>(signal_.size()), maxVisibleSampleIndex);

	// All points before the currently selected area (if present)
	wxPointList points;
	points.DeleteContents(true);  // Has to be called so that all the stored objects are destroyed when the list is destroyed!!
	// All points within the currently selected area (if present)
	wxPointList highlightedPoints;
	highlightedPoints.DeleteContents(true);  // Has to be called so that all the stored objects are destroyed when the list is destroyed!!
	// All points after the currently selected area (if present)
	wxPointList postSelectionPoints;
	postSelectionPoints.DeleteContents(true);  // Has to be called so that all the stored objects are destroyed when the list is destroyed!!

	int x, y, w, h;
	graph.getDimensions(x, y, w, h);
	
	double maxValue{ 1.0 };
	if(this->isNormalized())
	{
		auto max_it = std::max_element(signal_.begin() + startSampleIndex, signal_.begin() + lastSampleIndex);
		if(max_it != signal_.begin() + lastSampleIndex)
		{
			maxValue = *max_it;
		}
	}

	unsigned lastSampleIdx = x - 1;
	bool preSelectionComplete{ false };
	bool inSelectionComplete{ false };
	for (int p_x = x; p_x < x+w; ++p_x)
	{
		const auto t_s = graph.getAbsXValue(p_x);
		const auto sampleIdx = getSampleIndex(t_s);
		if (sampleIdx == lastSampleIdx && p_x != x+w-1) { continue; }
		if (sampleIdx >= signal_.size()) { break; }
		
		const int p_y = signal_.isCalibrated() ? graph.getYPos(signal_.getValue(sampleIdx)) : graph.getYPos(signal_[sampleIdx] / maxValue);
		
		if (this->hasValidSelection())
		{
			if (t_s < this->getSelectionStart())
			{
				points.Append(new wxPoint(p_x, p_y));
			}
			else if (t_s >= this->getSelectionStart() && t_s <= this->getSelectionEnd())
			{
				if(!preSelectionComplete)
				{
					// The last line segment from before the selection ends in the selection
					points.Append(new wxPoint(p_x, p_y));
					preSelectionComplete = true;
				}
				highlightedPoints.Append(new wxPoint(p_x, p_y));
			}
			else  // t_s > this->getSelectionEnd()
			{
				if(!inSelectionComplete)
				{
					// The last line segment from in the selection ends outside the selection
					highlightedPoints.Append(new wxPoint(p_x, p_y));
					inSelectionComplete = true;
				}
				postSelectionPoints.Append(new wxPoint(p_x, p_y));
			}
		}
		else
		{
			points.Append(new wxPoint(p_x, p_y));
		}


		lastSampleIdx = sampleIdx;
	}

	if (!points.IsEmpty())
	{
		dc.SetPen(linePen);
		dc.DrawLines(&points);
	}
	if (!highlightedPoints.IsEmpty())
	{
		dc.SetPen(highlightedLinePen);
		dc.DrawLines(&highlightedPoints);
	}
	if (!postSelectionPoints.IsEmpty())
	{
		dc.SetPen(linePen);
		dc.DrawLines(&postSelectionPoints);
	}
	labelSignal(dc);
}

void ScalarSignalPicture::labelSignal(wxDC& dc) const
{
	dc.SetBackgroundMode(wxSOLID);
	dc.SetTextBackground(getBackgroundColor());
	dc.SetTextForeground(getLineColor());

	const int textX{ 70 };
	const int textY{ 5 };
	wxString st;
	if (signal_.size() > 0) {
		st = wxString::Format("%s: %2.1f", signal_.name, signal_[signal_.size() - 1]);
	}
	else {
		st = wxString::Format("%s:", signal_.name);
	}
	dc.DrawText(st, textX, textY);
}

unsigned ScalarSignalPicture::getSampleIndex(double time_s) const
{
	return time_s * signal_.getSamplingRate_Hz();
}


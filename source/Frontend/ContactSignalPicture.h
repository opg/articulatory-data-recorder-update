#pragma once
#include "GraphPicture.h"
#include "ContactPatternPicture.h"
#include "../Backend/Signal.h"

/// @brief This class holds functionality to draw multiple contact pattern
/// pictures in real time.
class ContactSignalPicture : public GraphPicture
{
public:
	/// @brief Constructs the contact signal picture object.
	/// @param parent parent of the constructed contact signal picture
	/// @param contactMatrixSignal linearly indexed matrix signal containing the contact information
	/// @param backgroundColor_ background color
	/// @param lineColor_ line color
	/// @param isPartOfStack is part of stack (check documentation of GraphPicture class)
	ContactSignalPicture(wxWindow* parent, const MatrixSignal& contactMatrixSignal,
		wxColour backgroundColor_ = *wxWHITE, wxColour lineColor_ = *wxBLACK,
		bool isPartOfStack = false);

	/// @brief Draws to the given device context.
	///
	/// Overrides the base function "draw()" from GraphPicture to manage drawing
	/// for the contact signal picture object. Calls drawContactPattern() from ContactPatternPicture class internally.
	/// @param dc device context for painting (see wxWidgets documentation)
	void draw(wxDC& dc) override;

private:
	/// @brief Returns a given linearly indexed matrix reshaped into a given shape.
	/// @param linearlyIndexedMatrix linearly indexed matrix containing contact information
	/// @param rows number of rows
	/// @param columns number of columns
	/// @return reshaped contact matrix
	static std::vector<std::vector<double>> reshapeMatrix(const std::vector<double>& linearlyIndexedMatrix, int rows, int columns);

	ContactPatternPicture contactPatternPicture_;
	const MatrixSignal& contactMatrixSignal_;
};

#pragma once
#include "PromptsPage.h"
#include "TextCtrlSelfExpanding.h"

enum { kExperimentStart = 1, kExperimentEnd = 2 };
wxDECLARE_EVENT(INTRO_REACHED, wxCommandEvent);
wxDECLARE_EVENT(OUTRO_REACHED, wxCommandEvent);
wxDECLARE_EVENT(PAGE_UPDATE, wxCommandEvent);

class StringPromptsPage : public PromptsPage<std::string>
{
public:
	StringPromptsPage(wxWindow* parent, ExperimentManager<std::string>* ptrExperimentManager, bool fireEvents, wxWindowID id = wxID_ANY);
	void update();
	void reset();
	
	
private:
	void OnResize(wxSizeEvent& event);
	void OnPreviousPrompt(wxCommandEvent& event);
	void OnNextPrompt(wxCommandEvent& event);
	void OnUpdate(wxCommandEvent& event);

	void OnIntroReached(wxCommandEvent& event);
	void OnOutroReached(wxCommandEvent& event);
	
	void updateLayout();
	

public:
	TextCtrlSelfExpanding* showPrompt{ nullptr };
	bool fireEvents{ true };
	bool introReachedEventFired{ false };
	bool outroReachedEventFired{ false };

private:
	void initWidgets();
	wxFont myFont;

	DECLARE_EVENT_TABLE()
};

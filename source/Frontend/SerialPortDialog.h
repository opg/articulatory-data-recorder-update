#ifndef __SERIAL_DIALOG_H__
#define __SERIAL_DIALOG_H__

#include "wx\wx.h"
#include <wx/spinctrl.h>
#include "../Backend/SerialPort.h"

/****************************************************************************//**
 * \brief Dialog to select COM port settings.
 *
 * This class represents a dialog for user selection of the COM port settings.
 * It only handles the interface and does not in fact set the selected values.
 ****************************************************************************/
class SerialPortDialog final : public wxDialog
{
public:
	SerialPortDialog(wxWindow* parent, wxWindowID id, const wxString& title);
	SerialPortDialog(wxWindow* parent, wxWindowID id, const wxString& title, SerialPort::PortSettings portSettings);
	virtual ~SerialPortDialog();

	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);

	auto getSerialPortSettings() const -> SerialPort::PortSettings;

private:
	int minimumWidgetSize{ 150 };
	wxSpinCtrl* nameSelection;
	wxComboBox* comPortSelection;
	wxComboBox* baudRate;
	wxComboBox* parity;
	wxComboBox* stopBits;
	wxCheckBox* hardwareFlowControl;
	wxSpinCtrl* timeout_s;
	wxSpinCtrl* timeout_ms;

	SerialPort::PortSettings dialogPortSettings;

private:
	void initWidgets();
	void initWidgets(SerialPort::PortSettings portSettings);
};

#endif

#pragma once
#include <wx/wx.h>

class TextCtrlSelfExpanding : public wxTextCtrl
{
public:
	TextCtrlSelfExpanding(wxWindow* parent, wxWindowID id, const wxString& value = wxEmptyString,
		const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0,
		const wxValidator& validator = wxDefaultValidator,
		const wxString& name = wxString::FromAscii(wxTextCtrlNameStr));

	[[nodiscard]] int DoGetBestClientHeight(int width) const override;
	void Update() override;

private:
	void OnFocus(wxFocusEvent& event);

	DECLARE_EVENT_TABLE()
};
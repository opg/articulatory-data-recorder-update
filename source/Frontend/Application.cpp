#include <wx/image.h>
#include "Application.h"
#include "MainWindow.h"


// ****************************************************************************
// Event Table
// ****************************************************************************
BEGIN_EVENT_TABLE(Application, wxApp)
END_EVENT_TABLE()

Application::Application()
{
}

bool Application::OnInit()
{	
	mainWindow_.Show(true);
	wxInitAllImageHandlers();
	return true;
}

#include "DataPage.h"
#include "../Backend/Data.h"

#include "ContactSignalPicture.h"
#include "ScalarSignalPicture.h"

#include <wx/spinctrl.h>
#include <wx/wupdlock.h>

using namespace std;

// ****************************************************************************
// Ids.
// ****************************************************************************
static const int IDS_TIME = 400;

// ****************************************************************************
// The event table.
// ****************************************************************************
BEGIN_EVENT_TABLE(DataPage, wxPanel)
EVT_COMMAND_SCROLL(IDS_TIME, OnTimeScrollBar)
EVT_SIZE(DataPage::OnResize)
EVT_COMMAND(wxID_ANY, SelectionChangedEvent, OnSelectionChanged)
EVT_COMMAND(wxID_ANY, ScrollRequestEvent, OnScrollRequest)
END_EVENT_TABLE()

// ****************************************************************************
/// Constructor.
// ****************************************************************************
DataPage::DataPage(FrameBuffer& signals, ScalarSignal& audioSignal,
	wxWindow* parent, DataPage::Mode workingMode, wxWindowID id)
	: wxPanel(parent, id, wxDefaultPosition, wxDefaultSize, wxCLIP_CHILDREN), mode(workingMode), signals_(signals), audioSignal(audioSignal)
{
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	initWidgets();

	//this->SetDoubleBuffered(true);
}

void DataPage::initWidgets()
{
	sizer = new wxBoxSizer(wxVERTICAL);
	signalPictures_.clear();
	wxColour backgroundColor[] = { *wxWHITE, wxColour(240, 240, 240) };

	int trackIdx{ 0 };
	signalPictures_.push_back(new ScalarSignalPicture(this, audioSignal,
		backgroundColor[trackIdx++ % 2], *wxBLACK, 3, true));
	signalPictures_.back()->isNormalized(true);
	signalPictures_.back()->paintAbscissa(false);
	signalPictures_.back()->setPictureID(trackIdx - 1);
	signalProperties_[audioSignal.name] = signalPictures_.back()->getProperties();
	sizer->Add(signalPictures_.back(), wxSizerFlags(100).Expand());
	specPicture = new SpectrogramPicture(this, audioSignal, backgroundColor[trackIdx++ % 2], *wxBLACK, 3, true);
	signalPictures_.push_back(specPicture);
	signalPictures_.back()->setPictureID(trackIdx - 1);
	signalProperties_["Spectrogram"] = signalPictures_.back()->getProperties();
	sizer->Add(signalPictures_.back(), wxSizerFlags(100).Expand());

	for (const auto& signal : signals_.getScalarSignals())
	{
		signalPictures_.push_back(new ScalarSignalPicture(this, signal, backgroundColor[trackIdx++ % 2],
			*wxBLACK, 3, true));
		signalPictures_.back()->paintAbscissa(false);
		signalPictures_.back()->setPictureID(trackIdx - 1);
		signalProperties_[signal.name] = signalPictures_.back()->getProperties();
		sizer->Add(signalPictures_.back(), wxSizerFlags(100).Expand());
	}

	for (const auto& signal : signals_.getMatrixSignals())
	{
		signalPictures_.push_back(new ContactSignalPicture(this, signal, backgroundColor[trackIdx++ % 2],
			*wxBLACK, true));
		signalPictures_.back()->paintAbscissa(false);
		signalPictures_.back()->setPictureID(trackIdx - 1);
		signalProperties_[signal.name] = signalPictures_.back()->getProperties();
		sizer->Add(signalPictures_.back(), wxSizerFlags(100).Expand());
	}

	// The final signal picture should have its abscissa painted
	signalPictures_.back()->paintAbscissa(true);

	setGraphReference(signalPictures_.front()->graph.abscissa.reference);
	setGraphLimit(signalPictures_.front()->graph.abscissa.positiveLimit, signalPictures_.front()->graph.abscissa.negativeLimit);

	// Put the horizontal scrollbar below.
	scrTime = new wxScrollBar(this, IDS_TIME, wxDefaultPosition, wxDefaultSize, wxSB_HORIZONTAL);
	scrTime->SetMinSize(wxSize(-1, 20));
	scrTime->SetScrollbar(0, 10, 3600, 10);    // Scroll units are tenth of s.
	sizer->Add(scrTime, wxSizerFlags().Expand());

	this->SetSizerAndFit(sizer);
}

void DataPage::updateSignalProperties()
{
	for (const auto& [_, properties] : signalProperties_)
	{
		signalPictures_[properties.pictureID]->setProperties(properties);
	}
}

void DataPage::attachAbscissaToLastVisiblePicture()
{
	// Only do anything if the data page is shown on screen. 
	// Otherwise this will be called during construction and result in odd proportions
	if (this->IsShownOnScreen())
	{
		// Find the last visible picture
		GraphPicture* lastPic{ nullptr };
		lastShownPicFound = false;
		for (auto it = signalPictures_.rbegin(); it < signalPictures_.rend(); ++it)
		{
			if ((*it)->IsShown() && lastShownPicFound == false)
			{
				(*it)->paintAbscissa(true);
				lastPic = (*it);
				lastShownPicFound = true;
			}
			else
			{
				this->GetSizer()->GetItem(*it)->SetProportion(100);
				(*it)->paintAbscissa(false);
			}
		}
		
		// Lay out the children first so they get their new size
		this->Layout();

		// Height of the first visible picture
		int picHeight{ 0 };
		for (const auto& pic : signalPictures_)
		{
			if (pic->IsShown())
			{
				picHeight = pic->GetSize().GetHeight();
				break;
			}
		}		


		// Give the last picture some extra space to paint the abscissa while still
		// having the same drawing area as the other pictures	
		if (lastPic != nullptr && lastPic->isAbscissaPainted())
		{
			int abscissaHeight = 40;
			auto proportion = static_cast<double>(picHeight + abscissaHeight) / picHeight * 100.0;
			this->GetSizer()->GetItem(lastPic)->SetProportion(static_cast<int>(proportion));
		}
	}
}

void DataPage::updateWidgets()
{
	if (this != nullptr)
	{
		updateSignalProperties();

		if (mode == Mode::RECORDING)
		{
			const int numSamples = Data::getInstance().frameBuffer.size();
			const double mostRecentSample_s = static_cast<double>(numSamples) / Data::getInstance().frameBuffer.getSamplingRate();

			if (graphReference_ > mostRecentSample_s || graphReference_ + 0.5 * graphPositiveLimit_ < mostRecentSample_s)
			{
				setGraphReference(std::max(0.0, mostRecentSample_s - 0.5 * graphPositiveLimit_));
			}
		}
		for (const auto& signalPicture : signalPictures_)
		{
			signalPicture->graph.abscissa.reference = graphReference_;
			signalPicture->graph.abscissa.positiveLimit = graphPositiveLimit_;
			signalPicture->graph.abscissa.negativeLimit = graphNegativeLimit_;
			signalPicture->setSelection(selection_);
		}

		const auto pos_tenth_s = graphReference_ * 10.0;
		const int pos = static_cast<int>(pos_tenth_s + 0.5);
		if (pos != scrTime->GetThumbPosition())
		{
			scrTime->SetThumbPosition(pos);
		}
	}
	attachAbscissaToLastVisiblePicture();
	this->Refresh();
	this->Layout();
}

void DataPage::setGraphReference(double newReference)
{
	graphReference_ = newReference;
}

void DataPage::setGraphLimit(double posLimit, double negLimit)
{
	graphPositiveLimit_ = posLimit;
	graphNegativeLimit_ = negLimit;
}

void DataPage::clearSelection()
{
	selection_.clear();
}

GraphPicture::Selection DataPage::getSelection()
{
	return selection_;
}

void DataPage::setSelection(GraphPicture::Selection newSelection)
{
	selection_ = newSelection;
}

bool DataPage::hasSelection()
{
	return signalPictures_.front()->hasSelection();
}

void DataPage::reset()
{
	setGraphReference(0.0);
	clearSelection();
}

DataPage::View DataPage::getView() const
{
	return { graphNegativeLimit_, graphReference_, graphPositiveLimit_ };
}

void DataPage::setView(View newView)
{
	graphNegativeLimit_ = newView.min;
	graphReference_ = newView.reference;
	graphPositiveLimit_ = newView.max;
}

void DataPage::zoomIn()
{
	double maxLimit{ 0.0 };
	for(auto& pic : signalPictures_)
	{
		maxLimit = std::max(pic->zoomIn(), maxLimit);
	}
	setGraphReference(signalPictures_.front()->graph.abscissa.reference);
	setGraphLimit(maxLimit);
}

void DataPage::zoomOut()
{
	double maxLimit{ 0.0 };
	for (auto& pic : signalPictures_)
	{
		maxLimit = std::max(pic->zoomOut(), maxLimit);
	}
	setGraphReference(signalPictures_.front()->graph.abscissa.reference);
	setGraphLimit(maxLimit);
}

void DataPage::zoomToAll()
{
	double maxLimit{ 0.0 };
	for (auto& pic : signalPictures_)
	{
		maxLimit = std::max(pic->zoomToAll(), maxLimit);
	}
	setGraphReference(signalPictures_.front()->graph.abscissa.reference);
	setGraphLimit(maxLimit);
}

void DataPage::zoomToSelection()
{
	double minRef{ std::numeric_limits<double>::max() }, maxLimit{ 0.0 };
	for (auto& pic : signalPictures_)
	{
		auto [ref, posLim] = pic->zoomToSelection();
		minRef = std::min(ref, minRef);
		maxLimit = std::max(posLim, maxLimit);
	}
	setGraphReference(minRef);
	setGraphLimit(maxLimit);
}

void DataPage::OnTimeScrollBar(wxScrollEvent& event)
{
	const int pos = event.GetPosition();
	graphReference_ = static_cast<double>(pos) / 10;

	updateWidgets();
}

void DataPage::OnResize(wxSizeEvent& event)
{

	// TODO: Enable vertical scrolling if sum of picture heights is higher than space in panel

	updateWidgets();
	event.Skip();
}

void DataPage::OnScrollRequest(wxCommandEvent& event)
{
	graphReference_ = *static_cast<double*>(event.GetClientData());
	updateWidgets();
}

void DataPage::OnSelectionChanged(wxCommandEvent& event)
{
	const auto* requestedSelection = static_cast<GraphPicture::Selection*>(event.GetClientData());
	setSelection(*requestedSelection);
	delete requestedSelection;
	updateWidgets();

	event.Skip();
}
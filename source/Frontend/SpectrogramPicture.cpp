#include "SpectrogramPicture.h"
#include <algorithm>
#include <execution>
#include <utility>

#include <wx/rawbmp.h>

wxDEFINE_EVENT(AudioSettingsChangedEvent, wxCommandEvent);


SpectrogramPicture::SpectrogramPicture(wxWindow* parent, const ScalarSignal& audioSignal, wxColour backgroundColor,
                                       wxColour lineColor, const int linewidth_px, bool isPartOfStack) :
	GraphPicture(parent, backgroundColor, lineColor, linewidth_px, isPartOfStack), audioSignal_(audioSignal)
{
	spectrogram = Spectrogram(Spectrogram::Settings());
	spectrogram.setSamplingRate_Hz(audioSignal_.getSamplingRate_Hz());
	settings.frequencyRange = { 0, spectrogram.getSamplingRate_Hz() * spectrogram.getCutoff() };
	setColorMap(new SmoothCoolWarmColorMap(true));

	Graph::LinearDomain abscissaProperties;

	abscissaProperties.label = "s";
	abscissaProperties.positiveLimit = 10.0;  // Initial maximum displayed value
	abscissaProperties.positiveLimitMin = 0.2;  // Maximum displayed value when fully zoomed in
	abscissaProperties.positiveLimitMax = 3600.0;  // Maximum displayed value when fully zoomed out

	abscissaProperties.showGrayLines = false;

	ordinateProperties.label = "Hz";
	ordinateProperties.positiveLimit = static_cast<double>(audioSignal_.getSamplingRate_Hz()) * spectrogram.getCutoff();
	ordinateProperties.scaleDivision = ordinateProperties.positiveLimit / 2.0;
	ordinateProperties.positiveLimitMax = static_cast<double>(audioSignal_.getSamplingRate_Hz()) * spectrogram.getCutoff();
	ordinateProperties.postDecimalPositions = 0;
	ordinateProperties.showGrayLines = false;

	graph.initAbscissa(abscissaProperties);
	graph.initLinearOrdinate(ordinateProperties);
}

SpectrogramPicture::~SpectrogramPicture()
{
	delete settings.colorMap_;
}

double SpectrogramPicture::getLongestAnalysis() const
{
	return longestAnalysis_;
}

int SpectrogramPicture::getSamplingRate()
{
	return audioSignal_.getSamplingRate_Hz();
}

//ColorMap::ColorMapType SpectrogramPicture::getColorMapType() const
//{
//	//return settings.colorMap_;
//}

void SpectrogramPicture::Settings::setDynamicRange_dB(int newDynamicRange)
{
	dynamicRange_dB_ = newDynamicRange;
}

int SpectrogramPicture::Settings::getDynamicRange_dB()
{
	return dynamicRange_dB_;
}

wxColor SpectrogramPicture::getColorValue(double magnitude, double maxMagnitude) const
{
	const double factor = -1.0 / settings.dynamicRange_dB_;

	const double cutoff = maxMagnitude - settings.dynamicRange_dB_;
	
	if(magnitude > cutoff)
	{
		return settings.colorMap_->getColor(factor * (magnitude - maxMagnitude));
	}
	else
	{
		return settings.colorMap_->getColor(1.0);
	}	
}

void SpectrogramPicture::draw(wxDC& dc)
{
	// Draw image layer by layer
	GraphPicture::drawGraphBackground(dc);
	if (this->hasValidSelection())
	{
		GraphPicture::drawSelectionHighlight(dc);
	}

	drawSpectrogram(dc);
		
	GraphPicture::drawGraphAxes(dc);
	if (this->hasSelection())
	{
		GraphPicture::drawSelectionMarks(dc);
	}
	labelSpectrogram(dc);
}

void SpectrogramPicture::drawOverflowMessage(wxDC& dc)
{
	const auto msg = wxString("Visible signal length to long. Zoom in or change the 'Longest Analysis' setting.");
	// Calculate the start point for the message string so that it shows up centered on the dc
	wxCoord w, h;
	dc.GetSize(&w, &h);
	const auto messageSize = dc.GetTextExtent(msg);
	const auto x = w / 2 - messageSize.x / 2;
	const auto y = h / 2 - messageSize.y / 2;
	dc.DrawText(msg, x, y);	
}

void SpectrogramPicture::drawSpectrogram(wxDC& dc)
{
	if (!audioSignal_.empty())
	{
		// The sampling rate may change depending on the user's sound settings
		if (audioSignal_.getSamplingRate_Hz() != spectrogram.getSamplingRate_Hz()) {
			spectrogram.setSamplingRate_Hz(audioSignal_.getSamplingRate_Hz());
			settings.frequencyRange = { 0, spectrogram.getSamplingRate_Hz() * spectrogram.getCutoff() };
			ordinateProperties.positiveLimit = static_cast<double>(audioSignal_.getSamplingRate_Hz()) * spectrogram.getCutoff();
			ordinateProperties.scaleDivision = ordinateProperties.positiveLimit / 2.0;
			ordinateProperties.positiveLimitMax = static_cast<double>(audioSignal_.getSamplingRate_Hz()) * spectrogram.getCutoff();
			graph.initLinearOrdinate(ordinateProperties);
			postAudioSettingsChangedEvent();
		}


		// Get the visible part of the audio signal
		const auto firstSample = static_cast<int>(graph.abscissa.reference * audioSignal_.getSamplingRate_Hz());
		const auto finalSample = static_cast<int>(audioSignal_.size());
		// If no audio signal is visible: get out
		if(firstSample > finalSample){return;}
		
		const auto maxVisibleSample = static_cast<int>((graph.abscissa.reference + graph.abscissa.positiveLimit) * audioSignal_.getSamplingRate_Hz());
		const auto lastSample = std::min(finalSample, maxVisibleSample);

		// For performance reason, only calculate the spectrogram up to a certain length
		if (lastSample - firstSample > longestAnalysis_ * audioSignal_.getSamplingRate_Hz())
		{
			drawOverflowMessage(dc);
			return;
		}
		
		// Calculate the spectrogram of the visible audio signal
		auto spec = spectrogram.transform({ audioSignal_.begin() + firstSample, audioSignal_.begin() + lastSample }).get();

		// Get the dimensions of the spectrogram drawing area
		int graphX, graphY, graphW, graphH;
		graph.getDimensions(graphX, graphY, graphW, graphH);

		// Map frame index to pixel along width
		const auto maxVisibleSignalLength = maxVisibleSample - firstSample;
		const auto maxNumFrames = dsp::maxNumFrames(maxVisibleSignalLength, spectrogram.getFrameLength(true), spectrogram.getOverlap(true));
		const auto framesPerX = static_cast<double>(maxNumFrames) / graphW;

		// Check for frequency range to be shown
		const auto lowerBinIndex = static_cast<size_t>(spec.front().size() / static_cast<double>(audioSignal_.getSamplingRate_Hz() * spectrogram.getCutoff()) * graph.linearOrdinate.positiveLimitMin);
		const auto upperBinIndex = static_cast<size_t>(spec.front().size() / static_cast<double>(audioSignal_.getSamplingRate_Hz() * spectrogram.getCutoff()) * graph.linearOrdinate.positiveLimitMax);
		// Map frequency bin index to pixel along height
		const auto numBins = upperBinIndex - lowerBinIndex;
		//const auto numBins = spec.front().size();
		const auto binsPerY = static_cast<double>(numBins) / graphH;

		
		if (finalSample < maxVisibleSample)
		{
			// Reduce the drawing area because the signal does not yet cover the entire visible area
			const int finalX = graph.getXPos(static_cast<double>(finalSample) * 1.0 / static_cast<double>(audioSignal_.getSamplingRate_Hz()));
			graphW = finalX - graphX;
		}

		// Paint the image and re-sample the spectrogram to fit the pixel grid of the spectrogram drawing area
		wxBitmap bmp(graphW, graphH, 24);
		wxNativePixelData data(bmp);

		wxNativePixelData::Iterator p(data);
		p.Offset(data, 0, 0);

		// Get the maximum level to draw only a specific dynamic range
		const auto maxLevel = spectrogram.getMaxLevel();
		
		for (int y = 0; y < bmp.GetHeight(); ++y)
		{
			const wxNativePixelData::Iterator rowStart = p;
			const auto binIndex = std::min(binsPerY * y, static_cast<double>(spec.begin()->size() - 1));
			for (int x = 0; x < bmp.GetWidth(); ++x, ++p)
			{
				const auto frameIndex = std::min(framesPerX * x, static_cast<double>(spec.size() - 1));
				auto colorValue = getColorValue(spec[frameIndex][binIndex + lowerBinIndex], maxLevel);

				/* If the current pixel is within the selection area, change the color accordingly.
				 * The alpha channel cannot be used directly because the wxDC class does not support it.
				 */
				if (this->hasSelection())
				{
					if (x >= graph.getXPos(this->getSelectionStart()) 
						-  graph.getXPos(graph.abscissa.reference)
						&& 
						x <= graph.getXPos(this->getSelectionEnd())
						- graph.getXPos(graph.abscissa.reference)
						)
					{
						auto base = this->getHighlightColor();
						auto alpha = this->getHighlightAlpha();
						colorValue = alpha2rgb(alpha, colorValue, base);
					}
				}
				p.Red() = colorValue.Red();
				p.Green() = colorValue.Green();
				p.Blue() = colorValue.Blue();				
			}
			p = rowStart;
			p.OffsetY(data, 1);
		}

		// Image has top left corner as origin but data assumes bottom left corner
		dc.DrawBitmap(bmp.ConvertToImage().Mirror(false), graphX, graphY);
	}
}

void SpectrogramPicture::labelSpectrogram(wxDC& dc) const
{
	dc.SetBackgroundMode(wxSOLID);
	dc.SetTextBackground(getBackgroundColor());
	dc.SetTextForeground(getLineColor());

	const int textX{ 70 };
	const int textY{ 5 };
	const auto st = wxString::Format("Spectrogram:");
	dc.DrawText(st, textX, textY);
}

void SpectrogramPicture::setColorMap(ColorMap* newColorMap)
{
	settings.colorMap_ = newColorMap;
}

void SpectrogramPicture::setLongestAnalysis(double t_s)
{
	longestAnalysis_ = t_s;
}

double SpectrogramPicture::zoomToAll()
{
	if(!audioSignal_.empty())
	{
		graph.abscissa.reference = 0;
		graph.abscissa.positiveLimit = audioSignal_.size() * 1.0 / audioSignal_.getSamplingRate_Hz();
	}

	return graph.abscissa.reference;
}

void SpectrogramPicture::setColorMap(ColorMap::ColorMapType newColorMapType)
{
	switch (newColorMapType)
	{
	case ColorMap::ColorMapType::GRAY:
		settings.colorMap_ = new GrayColorMap(false);
		break;
	case ColorMap::ColorMapType::BLACK_BODY:
		settings.colorMap_ = new BlackBodyColorMap(false);
		break;
	case ColorMap::ColorMapType::SMOOTH_COOL_WARM:
		settings.colorMap_ = new SmoothCoolWarmColorMap(true);
		break;
	default:
		break;
	}
}

void SpectrogramPicture::postAudioSettingsChangedEvent()
{
	const wxCommandEvent event(AudioSettingsChangedEvent);
	wxPostEvent(this->GetParent(), event);
}


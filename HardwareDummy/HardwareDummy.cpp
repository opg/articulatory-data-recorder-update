#define _USE_MATH_DEFINES
#include <cmath>
#include <cstddef>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

#include <chrono>

#include "../source/Backend/SerialPort.h"


void sendTongueMouseFrame(SerialPort& sp, const std::vector<double>& frame)
{
	// Format: "%d\t%d\t%d\t%d\t%d\t%d\t%d\r\n"
	std::ostringstream outString;
	for (const auto& sample : frame)
	{
		if (sample != frame.back())
		{
			outString << static_cast<int>(sample) << '\t';
		}
		else {
			outString << static_cast<int>(sample);
		}
	}

	outString << "\r\n";

	sp.writeData(outString.str().c_str(), outString.str().length());
}

void sendOsloFrame(SerialPort& sp, const std::vector<double>& frame)
{
	// Format: "%4d,%4d,%4d,%4d,%4d,%4d,%4d,%4d,%4d,\n\r"
	std::ostringstream outString;
	for (const auto& sample : frame)
	{
		outString << static_cast<int>(sample) << ',';
	}
	
	outString << "\n\r";

	sp.writeData(outString.str().c_str(), outString.str().length());
}

void sendEosFrame(SerialPort& sp, const std::vector<double>& distanceData, const std::vector<std::byte>& contactData)
{
	// Format (number of bytes): Header (2), mode (1), index (2), distance sensors (3 detectors * 6 sensors * 2 bytes = 36), contact sensors (8), checksum (1)
	static uint16_t frameIndex{ 0 };
	const int kNumBytes = 50;
	char outBuffer[kNumBytes];
	int bytePos{ 0 };
	outBuffer[bytePos++] = static_cast<char>(0xA5);
	outBuffer[bytePos++] = static_cast<char>(0x5A);
	outBuffer[bytePos++] = static_cast<char>(0);
	outBuffer[bytePos++] = static_cast<char>(frameIndex & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((frameIndex >> 8) & 0xFF);
	for (const auto& sample : distanceData)
	{
		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample * 10) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample * 10) >> 8) & 0xFF);

		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample * 5) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample * 5) >> 8) & 0xFF);

		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample * 20) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample * 20) >> 8) & 0xFF);
	}
	for (const auto& byte : contactData)
	{
		outBuffer[bytePos++] = static_cast<char>(byte);
	}

	uint8_t checksum{ 0 };
	for (int i = 2; i < kNumBytes-1; ++i) // Header bytes are not included in checksum and checksum itself is last byte
	{
		checksum += (uint8_t) outBuffer[i];
	}
	outBuffer[bytePos] = checksum;

	sp.writeData(outBuffer, kNumBytes);
	frameIndex++;
};

void sendEos4Frame(SerialPort& sp, const std::vector<double> lipData,
	const std::vector<double>& distanceData,
	const std::vector<std::byte>& contactData)
{
	// Format (number of bytes):
	// Header (2), index (2),
	// lip 1 (2), lip 2 (2),
	// distance sensors (5 sensors * 2 bytes = 10),
	// contact sensors (8),
	// checksum (1)
	static uint16_t frameIndex{ 0 };
	const int kNumBytes = 27;
	char outBuffer[kNumBytes];
	int bytePos{ 0 };
	outBuffer[bytePos++] = static_cast<char>(0x5A);
	outBuffer[bytePos++] = static_cast<char>(0xA5);
	outBuffer[bytePos++] = static_cast<char>(frameIndex & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((frameIndex >> 8) & 0xFF);

	for (const auto& sample : lipData)
	{
		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample) >> 8) & 0xFF);
	}

	for (const auto& sample : distanceData)
	{
		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample) >> 8) & 0xFF);
	}

	for (const auto& byte : contactData)
	{
		outBuffer[bytePos++] = static_cast<char>(byte);
	}

	uint8_t checksum{ 0 };
	for (int i = 2; i < kNumBytes - 1; ++i) // Header bytes are not included in checksum and checksum itself is last byte
	{
		checksum += (uint8_t)outBuffer[i];
	}
	outBuffer[bytePos] = checksum;

	sp.writeData(outBuffer, kNumBytes);
	frameIndex++;
}

void sendSecondVoiceFrame(SerialPort& sp, const std::vector<double> lipData,
	const double& velumData,
	const std::vector<double>& distanceData, 
	const std::vector<std::byte>& contactData,
	const double& f0Data,
	const double& voicingData, 
	const double& breathinessData,
	const double& lungData)
{
	// Format (number of bytes):
	// Header (2), index (2),
	// lip 1 (2), lip 2 (2),
	// velum (2),
	// distance sensors (5 sensors * 2 bytes = 10),
	// contact sensors (8),
	// f0 (2), voicing (1), breathiness (1), lung pressure (2)
	// checksum (1)
	static uint16_t frameIndex{ 0 };
	const int kNumBytes = 36;
	char outBuffer[kNumBytes];
	int bytePos{ 0 };
	outBuffer[bytePos++] = static_cast<char>(0x5A);
	outBuffer[bytePos++] = static_cast<char>(0xA5);
	outBuffer[bytePos++] = static_cast<char>(frameIndex & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((frameIndex >> 8) & 0xFF);

	for (const auto& sample : lipData)
	{
		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample) >> 8) & 0xFF);
	}

	outBuffer[bytePos++] = static_cast<char>(static_cast<int>(velumData) & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((static_cast<int>(velumData) >> 8) & 0xFF);
	
	for (const auto& sample : distanceData)
	{
		outBuffer[bytePos++] = static_cast<char>(static_cast<int>(sample) & 0xFF);
		outBuffer[bytePos++] = static_cast<char>((static_cast<int>(sample) >> 8) & 0xFF);
	}
	
	for (const auto& byte : contactData)
	{
		outBuffer[bytePos++] = static_cast<char>(byte);
	}

	outBuffer[bytePos++] = static_cast<char>(static_cast<int>(f0Data * 10) & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((static_cast<int>(f0Data * 10) >> 8) & 0xFF);

	outBuffer[bytePos++] = static_cast<char>(static_cast<int>(voicingData) & 0xFF);

	outBuffer[bytePos++] = static_cast<char>(static_cast<int>(breathinessData * 1000) & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((static_cast<int>(breathinessData * 1000) >> 8)  & 0xFF);

	outBuffer[bytePos++] = static_cast<char>(static_cast<int>(lungData * 10) & 0xFF);
	outBuffer[bytePos++] = static_cast<char>((static_cast<int>(lungData * 10) >> 8) & 0xFF);
	
	
	
	uint8_t checksum{ 0 };
	for (int i = 2; i < kNumBytes - 1; ++i) // Header bytes are not included in checksum and checksum itself is last byte
	{
		checksum += (uint8_t)outBuffer[i];
	}
	outBuffer[bytePos] = checksum;

	sp.writeData(outBuffer, kNumBytes);
	frameIndex++;
}

int main()
{
	int hardwareVersion{ 0 };

	std::cout << "This is the Hardware Dummy.\n";
	std::cout << "It emulates a measurement hardware device's data stream." << std::endl;


	SerialPort sp("COM1", 115200);
	if (sp.open())
	{
		std::cout << "COM port successfully opened!" << std::endl;
	}
	else
	{
		std::cout << "Opening COM port failed!" << std::endl;
		return 1;
	}

	std::cout << "Choose the hardware you want to emulate:\n"
		<< "(1) OSLO" << std::endl
		<< "(2) EOS" << std::endl
		<< "(3) EOS 4.0" << std::endl
		<< "(4) SecondVoice" << std::endl
		<< "(5) TongueMouse" << std::endl;
	std::cin >> hardwareVersion;

	double t_s{ 0 };
	const int framerateOslo_Hz = 100;
	const int framerateEos_Hz = 100;
	const int framerateEos4_Hz = 100;
	const int framerateSecondVoice_Hz = 100;
	const int framerateTongueMouse_Hz = 500;
	while (true)
	{
		auto t0 = std::chrono::high_resolution_clock::now();
		/* Fake distance data */
		std::vector<double> distanceData{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		int k{ 1 };
		double baseFreq_Hz{ 2 };
		for (auto& signal : distanceData)
		{
			signal = 2000.0 + 2000 * sin(2 * M_PI * k * baseFreq_Hz * t_s);
			++k;
		}

		/* Fake contact data */
		std::vector<std::byte> contactData{ 8, std::byte{1} };
		for (auto& byte : contactData)
		{
			byte <<= 1 * static_cast<int>(t_s / 0.5); // Shift every 0.5 seconds
			if(std::to_integer<int>(byte) == 0)
			{
				byte = std::byte{ 1 };
			}
		}
		
		switch (hardwareVersion)
		{
		case 1:
			{
			sendOsloFrame(sp, distanceData);
			t_s += 1.0 / static_cast<double>(framerateOslo_Hz);
			int T = 1.0 / static_cast<double>(framerateOslo_Hz) * 1000;
			while (std::chrono::high_resolution_clock::now() - t0 < std::chrono::milliseconds(T)) { continue; }
				break;
			}			
		case 2:
			{
			sendEosFrame(sp, std::vector<double>(distanceData.begin(), distanceData.begin()+6), contactData);
			t_s += 1.0 / static_cast<double>(framerateEos_Hz);
			int T = 1.0 / static_cast<double>(framerateEos_Hz) * 1000;
			while (std::chrono::high_resolution_clock::now() - t0 < std::chrono::milliseconds(T)) { continue; }
			break;
			}
		case 3:
		{

			sendEos4Frame(sp,
				std::vector<double>(distanceData.begin(), distanceData.begin() + 2),
				std::vector<double>(distanceData.begin() + 2, distanceData.begin() + 7),
				contactData
			);
			t_s += 1.0 / static_cast<double>(framerateEos4_Hz);
			int T = 1.0 / static_cast<double>(framerateEos4_Hz) * 1000;
			while (std::chrono::high_resolution_clock::now() - t0 < std::chrono::milliseconds(T)) { continue; }
			break;
		}
		case 4:
		{

			sendSecondVoiceFrame(sp,
				std::vector<double>(distanceData.begin(), distanceData.begin() + 2),
				*distanceData.begin(),
				std::vector<double>(distanceData.begin() + 2, distanceData.begin() + 7),
				contactData,
				120.0,
				1.0,
				0.5,
				800.0
			);
			t_s += 1.0 / static_cast<double>(framerateSecondVoice_Hz);
			int T = 1.0 / static_cast<double>(framerateSecondVoice_Hz) * 1000;
			while (std::chrono::high_resolution_clock::now() - t0 < std::chrono::milliseconds(T)) { continue; }
			break;
		}
		case 5:
			{
			sendTongueMouseFrame(sp, 
				std::vector<double>(distanceData.begin(), distanceData.begin() + 7)
				);
			t_s += 1.0 / static_cast<double>(framerateTongueMouse_Hz);
			int T = 1.0 / static_cast<double>(framerateTongueMouse_Hz) * 1000;
			while (std::chrono::high_resolution_clock::now() - t0 < std::chrono::milliseconds(T)) { continue; }
			break;
			}
		}		
	}

	return 0;
}

